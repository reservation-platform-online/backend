<?php

return [
    'class' => 'yii\db\Connection',
    'dsn' => 'mysql:host=localhost;dbname=reservation_platform',
    'username' => 'root',
    'password' => 'root',
    'charset' => 'utf8',
];
