<?php

return [

    '/' => 'site/index',
    '/index' => 'site/index',
    '/redirect' => 'site/redirect',

    '/form' => 'site/form',
    '/contact' => 'site/contact',
    '/404' => 'site/404',
    '/faq' => 'site/faq',
    '/about' => 'site/about',
    '/login' => 'site/login',


];