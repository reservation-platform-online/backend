<?php

    // Client
    return [
        [
            'class' => 'yii\rest\UrlRule',
            'controller' => 'api/v1/company/company',
            'pluralize' => false,
            'extraPatterns' => [
                'OPTIONS,GET' => 'index',
                'OPTIONS,PUT' => 'update',
                'OPTIONS,POST upload-image' => 'upload-image',
                'OPTIONS,DELETE' => 'delete',
            ]
        ],
        [
            'class' => 'yii\rest\UrlRule',
            'controller' => 'api/v1/company/pws',
            'pluralize' => false,
            'tokens' => [
                '{id}'=>'<id:\\w+-\\w+-\\w+-\\w+-\\w+>',
            ],
            'extraPatterns' => [
                'OPTIONS,GET' => 'index',
                'OPTIONS,GET {id}' => 'view',
                'OPTIONS,PUT {id}/toggle-enable' => 'toggle-enable',
                'OPTIONS,GET {id}/comments' => 'comments',
            ]
        ],
        [
            'class' => 'yii\rest\UrlRule',
            'controller' => 'api/v1/company/worker',
            'tokens' => [
                '{id}' => '<id:\\w+-\\w+-\\w+-\\w+-\\w+>',
            ],
            'extraPatterns' => [
                'OPTIONS,GET' => 'index',
                'OPTIONS,GET {id}' => 'view',
                'OPTIONS,POST' => 'create',
                'OPTIONS,PUT {id}' => 'update',
                'OPTIONS,DELETE {id}' => 'delete',
                'OPTIONS,POST upload-image/{id}' => 'upload-image',
                'OPTIONS,POST connect-service/{id}' => 'connect-service',
                'OPTIONS,DELETE disconnect-service/{id}' => 'disconnect-service',
                'OPTIONS,POST connect-zoom/{id}' => 'connect-zoom',
                'OPTIONS,DELETE disconnect-zoom/{id}' => 'disconnect-zoom',
            ]
        ],
        [
            'class' => 'yii\rest\UrlRule',
            'controller' => 'api/v1/company/point',
            'tokens' => [
                '{id}' => '<id:\\w+-\\w+-\\w+-\\w+-\\w+>',
            ],
            'extraPatterns' => [
                'OPTIONS,GET' => 'index',
                'OPTIONS,GET {id}' => 'view',
            ]
        ],
        [
            'class' => 'yii\rest\UrlRule',
            'controller' => 'api/v1/company/service',
            'tokens' => [
                '{id}' => '<id:\\w+-\\w+-\\w+-\\w+-\\w+>',
            ],
            'extraPatterns' => [
                'OPTIONS,GET' => 'index',
                'OPTIONS,GET {id}' => 'view',
                'OPTIONS,POST' => 'create',
                'OPTIONS,PUT {id}' => 'update',
                'OPTIONS,PUT {id}/toggle-enable' => 'toggle-enable',
                'OPTIONS,POST upload-image/{id}' => 'upload-image',
                'OPTIONS,POST connect-worker/{id}' => 'connect-worker',
                'OPTIONS,DELETE disconnect-worker/{id}' => 'disconnect-worker',
                'OPTIONS,DELETE {id}' => 'delete',
            ]
        ],
        [
            'class' => 'yii\rest\UrlRule',
            'controller' => 'api/v1/company/work-time',
            'tokens' => [
                '{id}' => '<id:\\w+-\\w+-\\w+-\\w+-\\w+>',
            ],
            'extraPatterns' => [
                'OPTIONS,GET {id}' => 'view',
                'OPTIONS,POST' => 'create',
                'OPTIONS,PUT {id}' => 'update',
                'OPTIONS,DELETE {id}' => 'delete',
            ]
        ],
        [
            'class' => 'yii\rest\UrlRule',
            'controller' => 'api/v1/company/reservation',
            'tokens' => [
                '{id}' => '<id:\\w+-\\w+-\\w+-\\w+-\\w+>',
            ],
            'extraPatterns' => [
                'OPTIONS,GET' => 'index',
                'OPTIONS,GET {id}' => 'view',
                'OPTIONS,POST' => 'create',
                'OPTIONS,PUT {id}' => 'update',
                'OPTIONS,DELETE {id}' => 'delete',
            ]
        ],
    ];