<?php

    return [
        [
            'class' => 'yii\rest\UrlRule',
            'controller' => 'api/v1/worker/worker',
            'pluralize' => false,
            'extraPatterns' => [
                'OPTIONS,GET' => 'index',
                'OPTIONS,PUT' => 'update',
                'OPTIONS,PUT toggle-enable' => 'toggle-enable',
                'OPTIONS,POST upload-image' => 'upload-image',
                'OPTIONS,DELETE' => 'delete',
            ]
        ],
        [
            'class' => 'yii\rest\UrlRule',
            'controller' => 'api/v1/worker/work-time',
            'extraPatterns' => [
                'OPTIONS,GET' => 'index',
            ]
        ],
        [
            'class' => 'yii\rest\UrlRule',
            'controller' => 'api/v1/worker/reservation',
            'tokens' => [
                '{id}' => '<id:\\w+-\\w+-\\w+-\\w+-\\w+>',
            ],
            'extraPatterns' => [
                'OPTIONS,GET' => 'index',
                'OPTIONS,GET {id}' => 'view',
                'OPTIONS,POST' => 'create',
                'OPTIONS,PUT {id}' => 'update',
                'OPTIONS,DELETE {id}' => 'delete',
                'OPTIONS,POST {id}/create-room' => 'create-room',
            ]
        ],
    ];