<?php

return [
    [
        'class' => 'yii\rest\UrlRule',
        'controller' => 'api/v1/cron/cron',
        'pluralize' => false,
        'extraPatterns' => [
            'OPTIONS,GET per-minute' => 'per-minute',
            'OPTIONS,GET per-day' => 'per-day',
        ]
    ],
];