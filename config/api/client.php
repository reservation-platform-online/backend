<?php

    // Client
    return [
        [
            'class' => 'yii\rest\UrlRule',
            'controller' => 'api/v1/client/report',
            'pluralize' => false,
            'extraPatterns' => [
                'OPTIONS,POST' => 'report',
            ]
        ],
        [
            'class' => 'yii\rest\UrlRule',
            'controller' => 'api/v1/client/external-auth',
            'pluralize' => false,
            'extraPatterns' => [
                'OPTIONS,GET' => 'index',
//                'OPTIONS,POST create-meeting' => 'create-meeting',
                'OPTIONS,POST callback' => 'callback',
                'OPTIONS,POST deauthorization' => 'deauthorization',
//                'OPTIONS,POST create-access-token' => 'create-access-token',
//                'OPTIONS,PUT update-access-token' => 'update-access-token'
            ]
        ],
        [
            'class' => 'yii\rest\UrlRule',
            'controller' => 'api/v1/client/locality',
            'extraPatterns' => [
                'OPTIONS,GET cities' => 'cities',
            ]
        ],
        [
            'class' => 'yii\rest\UrlRule',
            'controller' => 'api/v1/client/auth',
            'tokens' => [
                '{code}' => '<code:\\w+>',
            ],
            'pluralize' => false,
            'extraPatterns' => [
                'OPTIONS,POST deauthorization' => 'deauthorization',
                'OPTIONS,POST login' => 'login',
                'OPTIONS,POST registration' => 'registration',
                'OPTIONS,POST registration-company' => 'registration-company',
                'OPTIONS,POST reset-password' => 'reset-password',
                'OPTIONS,PUT confirm-email' => 'confirm-email',
                'OPTIONS,GET agreements' => 'agreements',
                'OPTIONS,POST external-login' => 'external-login',
//                'OPTIONS,GET platform-list' => 'platform-list',
            ]
        ],
        [
            'class' => 'yii\rest\UrlRule',
            'controller' => 'api/v1/client/company',
            'tokens' => [
                '{id}' => '<id:\\w+-\\w+-\\w+-\\w+-\\w+>',
            ],
            'extraPatterns' => [
                'OPTIONS,GET' => 'index',
                'OPTIONS,POST' => 'create',
                'OPTIONS,PUT' => 'update',
                'OPTIONS,GET {id}' => 'view',
            ]
        ],
        [
            'class' => 'yii\rest\UrlRule',
            'controller' => 'api/v1/client/worker',
            'tokens' => [
                '{id}' => '<id:\\w+-\\w+-\\w+-\\w+-\\w+>',
            ],
            'extraPatterns' => [
                'OPTIONS,GET' => 'index',
                'OPTIONS,GET {id}' => 'view',
            ]
        ],
        [
            'class' => 'yii\rest\UrlRule',
            'controller' => 'api/v1/client/point',
            'tokens' => [
                '{id}' => '<id:\\w+-\\w+-\\w+-\\w+-\\w+>',
            ],
            'extraPatterns' => [
                'OPTIONS,GET' => 'index',
                'OPTIONS,GET {id}' => 'view',
            ]
        ],
        [
            'class' => 'yii\rest\UrlRule',
            'controller' => 'api/v1/client/user',
            'pluralize' => false,
            'extraPatterns' => [
                'OPTIONS,GET me' => 'me',
                'OPTIONS,PUT' => 'update',
                'OPTIONS,DELETE logout' => 'logout',
                'OPTIONS,POST upload-image' => 'upload-image',
                'OPTIONS,POST external-connect' => 'external-connect',
                'OPTIONS,PUT external-refresh-token' => 'external-refresh-token',
                'OPTIONS,PUT change-password' => 'change-password',
            ]
        ],
        [
            'class' => 'yii\rest\UrlRule',
            'controller' => 'api/v1/client/reservation',
            'pluralize' => false,
            'tokens' => [
                '{id}' => '<id:\\w+-\\w+-\\w+-\\w+-\\w+>',
            ],
            'extraPatterns' => [
                'OPTIONS,GET' => 'index',
                'OPTIONS,POST' => 'create',
                'OPTIONS,DELETE {id}' => 'cancel',
                'OPTIONS,POST comment' => 'create-comment',
                'OPTIONS,GET comment' => 'comment',
            ]
        ],
        [
            'class' => 'yii\rest\UrlRule',
            'controller' => 'api/v1/client/category',
            'extraPatterns' => [
                'OPTIONS,GET' => 'index',
            ]
        ],
        [
            'class' => 'yii\rest\UrlRule',
            'controller' => 'api/v1/client/service',
            'extraPatterns' => [
                'OPTIONS,GET' => 'index',
            ]
        ],
        [
            'class' => 'yii\rest\UrlRule',
            'controller' => 'api/v1/client/pws',
            'tokens' => [
                '{id}' => '<id:\\w+-\\w+-\\w+-\\w+-\\w+>',
            ],
            'extraPatterns' => [
                'OPTIONS,GET' => 'index',
                'OPTIONS,GET {id}' => 'view',
                'OPTIONS,GET {id}/calendar' => 'calendar',
                'OPTIONS,GET {id}/comments' => 'comments',
            ]
        ],
    ];