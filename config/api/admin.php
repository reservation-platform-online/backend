<?php

    // Client
    return [

        [
            'class' => 'yii\rest\UrlRule',
            'controller' => 'api/v1/admin/statistic',
            'extraPatterns' => [
                'GET' => 'index',
            ]
        ],

        [
            'class' => 'yii\rest\UrlRule',
            'controller' => 'api/v1/admin/views/statistics',
            'extraPatterns' => [
                'GET' => 'index',
            ]
        ],
        [
            'class' => 'yii\rest\UrlRule',
            'controller' => 'api/v1/admin/worker',
            'tokens' => [
                '{id}' => '<id:\\w+-\\w+-\\w+-\\w+-\\w+>',
            ],
            'extraPatterns' => [
                'GET' => 'index',
                'GET {id}' => 'view',
            ]
        ],
        [
            'class' => 'yii\rest\UrlRule',
            'controller' => 'api/v1/admin/point',
            'tokens' => [
                '{id}' => '<id:\\w+-\\w+-\\w+-\\w+-\\w+>',
            ],
            'extraPatterns' => [
                'GET' => 'index',
                'GET {id}' => 'view',
            ]
        ],
        [
            'class' => 'yii\rest\UrlRule',
            'controller' => 'api/v1/admin/service',
            'tokens' => [
                '{id}' => '<id:\\w+-\\w+-\\w+-\\w+-\\w+>',
            ],
            'extraPatterns' => [
                'GET' => 'index',
                'GET {id}' => 'view',
            ]
        ],
    ];