<?php

$isProd = true;

return [
    'path' => $isProd ? 'prod' : 'dev',

    'baseUploadsDir' => 'uploads',

    'baseDomain' => 'reservation-platform-online',

    'adminEmail' => 'admin@example.com',
    'senderEmail' => 'noreply@example.com',
    'senderName' => 'Example.com mailer',

    'reportEmail' => 'report@reservation-platform-online',

    'infoEmail' => 'info@reservation-platform-online',
    'nameOfInfoEmail' => 'reservation-platform-online: Info',

    'company' => [
        'name' => 'Ivan Karbashevskyi'
    ],
    'client' => [
        'pws' => [
            'startAt' => '2020-12-21',
            'endAt' => '2020-12-27',
        ]
    ],
    'urls' => [
        'dev' => [
            'api' => 'https://api.dev.reservation-platform-online',
            'client' => 'https://dev.reservation-platform-online'
        ],
        'prod' => [
            'api' => 'https://api.reservation-platform-online',
            'client' => 'https://reservation-platform-online'
        ]
    ],
    'zoom' => [
        'dev' => [
            'clientId' => 'DROy36BsQdir_0aZ8JbjQ',
            'clientSecret' => 'WYsZ4n4n2j0nXpu31jOirMTOgGGaIBjt',
            'redirectUri' => 'https://b2c2a6fe5d71.ngrok.io'
        ],
        'prod' => [
            'clientId' => '3BOXNKEpR_KxsQ0IW2RUA',
            'clientSecret' => 'b3E4znYd8gcvQAEBLhgr57v2tJuqGvj2',
            'redirectUri' => ''
        ]
    ],

    // TODO set to controller REST API
    'cors' => [

        'Origin' => [
            'https://reservation-platform-online'
        ]

    ]

];
