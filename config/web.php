<?php

$params = require __DIR__ . '/params.php';
$db = require __DIR__ . '/db.php';

$rules_system = require __DIR__ . '/api/system.php';
$rules_client = require __DIR__ . '/api/client.php';
$rules_admin = require __DIR__ . '/api/admin.php';
$rules_company = require __DIR__ . '/api/company.php';
$rules_worker = require __DIR__ . '/api/worker.php';
$rules_cron = require __DIR__ . '/api/cron.php';

$rules = [];
$rules = array_merge($rules, $rules_system, $rules_client, $rules_admin, $rules_company, $rules_worker, $rules_cron);

$config = [
    'id' => 'basic',
    'basePath' => dirname(__DIR__),
    'bootstrap' => ['log'],
    'aliases' => [
        '@bower' => '@vendor/bower-asset',
        '@npm' => '@vendor/npm-asset',
        '@webroot' => dirname(dirname(__FILE__)) . '/web',
    ],
    'name' => 'reservation-platform-online',
    'components' => [
        'request' => [
            // !!! insert a secret key in the following (if it is empty) - this is required by cookie validation
            'cookieValidationKey' => 'COOKIE_password_TOKEN_1234567890',
            'baseUrl' => '',
        ],
        'cache' => [
            'class' => 'yii\caching\FileCache',
        ],
        'user' => [
            'identityClass' => 'app\models\User',
            'enableAutoLogin' => false,
            'enableSession' => false,
        ],
        'errorHandler' => [
            'errorAction' => 'site/error',
        ],
//        'mailer' => [
//            'class' => 'yii\swiftmailer\Mailer',
//            'useFileTransport' => false,
//            'transport' => [
//                'class' => 'Swift_SmtpTransport',
//                'host' => 'TODO',
//                'username' => 'TODO',
//                'password' => 'TODO',
//                'port' => '465',
//                'encryption' => 'ssl',
//            ],
//        ],
        'log' => [
            'traceLevel' => YII_DEBUG ? 3 : 0,
            'flushInterval' => 1,
            'targets' => [

                [

                    'class' => 'yii\log\FileTarget',

                    'categories' => ['test'],

                    'exportInterval' => 1,

                    'logFile' => '@app/runtime/logs/my.log',

                ],
                [
                    'class' => 'yii\log\FileTarget',
                    'levels' => ['error', 'warning'],
                ],
            ],
        ],
        'db' => $db,
        'urlManager' => [
            'class' => 'yii\web\UrlManager',
            'enablePrettyUrl' => true,
            'enableStrictParsing' => true,
            'showScriptName' => false,
            'rules' => $rules,
        ],
    ],
    'params' => $params,
    'modules' => [
        'api' => [
            'class' => 'app\modules\api\Module',
        ],
    ],
];

if (YII_ENV_DEV) {
    // configuration adjustments for 'dev' environment
    $config['bootstrap'][] = 'debug';
    $config['modules']['debug'] = [
        'class' => 'yii\debug\Module',
        // uncomment the following to add your IP if you are not connecting from localhost.
        //'allowedIPs' => ['127.0.0.1', '::1'],
    ];

    $config['bootstrap'][] = 'gii';
    $config['modules']['gii'] = [
        'class' => 'yii\gii\Module',
        // uncomment the following to add your IP if you are not connecting from localhost.
        //'allowedIPs' => ['127.0.0.1', '::1'],
    ];
}

return $config;
