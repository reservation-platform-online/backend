<?php

namespace app\models\types;

use app\models\business\BusinessReservation;
use Yii;
use yii\db\ActiveQuery;
use yii\db\ActiveRecord;

/**
 * This is the model class for table "{{%type__payment}}".
 *
 * @property int $id
 * @property string $name
 * @property int $hide
 * @property int $enabled
 *
 * @property BusinessReservation[] $businessReservations
 */
class TypePayment extends ActiveRecord
{
    const ENABLED = 1;
    const DISABLED = 0;
    const SHOW = 0;
    const HIDE = 1;
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return '{{%type__payment}}';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['name'], 'required'],
            [['hide', 'enabled'], 'integer'],
            [['name'], 'string', 'max' => 45],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Name',
            'hide' => 'Hide',
            'enabled' => 'Enabled',
        ];
    }

    /**
     * Gets query for [[BusinessReservations]].
     *
     * @return ActiveQuery
     */
    public function getBusinessReservations()
    {
        return $this->hasMany(BusinessReservation::className(), ['type__payment_id' => 'id']);
    }
}
