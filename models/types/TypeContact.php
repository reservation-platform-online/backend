<?php

namespace app\models\types;

use app\models\contacts\Contact;
use Yii;
use yii\db\ActiveQuery;
use yii\db\ActiveRecord;

/**
 * This is the model class for table "{{%type__contact}}".
 *
 * @property string $id
 * @property int $enabled
 *
 * @property Contact[] $contacts
 */
class TypeContact extends ActiveRecord
{
    const ENABLED = 1;
    const DISABLED = 0;
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return '{{%type__contact}}';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id'], 'required'],
            [['enabled'], 'integer'],
            [['id'], 'string', 'max' => 45],
            [['id'], 'unique'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'enabled' => 'Enabled',
        ];
    }

    /**
     * Gets query for [[Contacts]].
     *
     * @return ActiveQuery
     */
    public function getContacts()
    {
        return $this->hasMany(Contact::className(), ['type__contact_id' => 'id']);
    }
}
