<?php

namespace app\models\types;

use app\models\business\BusinessPointWorkerServiceTypeService;
use Yii;
use yii\db\ActiveQuery;
use yii\db\ActiveRecord;

/**
 * This is the model class for table "{{%type__service}}".
 *
 * @property int $id
 * @property string $name
 * @property int $hide
 * @property int $enabled
 *
 * @property BusinessPointWorkerServiceTypeService[] $businessPointWorkerServiceTypeServices
 */
class TypeService extends ActiveRecord
{
    const ENABLED = 1;
    const DISABLED = 0;
    const SHOW = 0;
    const HIDE = 1;
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return '{{%type__service}}';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['name'], 'required'],
            [['hide', 'enabled'], 'integer'],
            [['name'], 'string', 'max' => 45],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Name',
            'hide' => 'Hide',
            'enabled' => 'Enabled',
        ];
    }

    /**
     * Gets query for [[BusinessPointWorkerServiceTypeServices]].
     *
     * @return ActiveQuery
     */
    public function getBusinessPointWorkerServiceTypeServices()
    {
        return $this->hasMany(BusinessPointWorkerServiceTypeService::className(), ['type__service_id' => 'id']);
    }
}
