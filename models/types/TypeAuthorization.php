<?php

namespace app\models\types;

use app\models\authorizations\Authorization;
use Yii;
use yii\db\ActiveQuery;
use yii\db\ActiveRecord;

/**
 * This is the model class for table "{{%type__authorization}}".
 *
 * @property int $id
 * @property string $name
 *
 * @property Authorization[] $authorizations
 */
class TypeAuthorization extends ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return '{{%type__authorization}}';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['name'], 'required'],
            [['name'], 'string', 'max' => 45],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Name',
        ];
    }

    /**
     * Gets query for [[Authorizations]].
     *
     * @return ActiveQuery
     */
    public function getAuthorizations()
    {
        return $this->hasMany(Authorization::className(), ['type__authorization_id' => 'id']);
    }
}
