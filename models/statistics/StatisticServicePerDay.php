<?php

namespace app\models\statistics;

use Yii;

/**
 * This is the model class for table "{{%statistic__service_per_day}}".
 *
 * @property int $id
 * @property string $service_id
 * @property int $countCanceledReservations
 * @property int $countCreatedReservations
 * @property int $countAcceptedReservations
 * @property int $countSuccessReservations
 * @property string $created_at
 */
class StatisticServicePerDay extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return '{{%statistic__service_per_day}}';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['service_id', 'countCanceledReservations', 'countCreatedReservations', 'countAcceptedReservations', 'countSuccessReservations'], 'required'],
            [['countCanceledReservations', 'countCreatedReservations', 'countAcceptedReservations', 'countSuccessReservations'], 'integer'],
            [['created_at'], 'safe'],
            [['service_id'], 'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'service_id' => 'Service ID',
            'countCanceledReservations' => 'Count Canceled Reservations',
            'countCreatedReservations' => 'Count Created Reservations',
            'countAcceptedReservations' => 'Count Accepted Reservations',
            'countSuccessReservations' => 'Count Success Reservations',
            'created_at' => 'Created At',
        ];
    }
}
