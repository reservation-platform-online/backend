<?php

namespace app\models\statistics;

use Yii;
use yii\db\ActiveRecord;

/**
 * This is the model class for table "{{%statistic}}".
 *
 * @property int $id
 * @property int $users
 * @property int $users_confirmed
 * @property int $reservations
 * @property int $reservations_confirmed
 * @property int $workers
 * @property int $owners
 * @property int $receptions
 * @property int $points
 * @property int $companies
 * @property string $created_at
 */
class Statistic extends ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return '{{%statistic}}';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['users', 'users_confirmed', 'reservations', 'reservations_confirmed', 'workers', 'owners', 'receptions', 'points', 'companies'], 'integer'],
            [['created_at'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'users' => 'Users',
            'users_confirmed' => 'Users Confirmed',
            'reservations' => 'Reservations',
            'reservations_confirmed' => 'Reservations Confirmed',
            'workers' => 'Workers',
            'owners' => 'Owners',
            'receptions' => 'Receptions',
            'points' => 'Points',
            'companies' => 'Companies',
            'created_at' => 'Created At',
        ];
    }
}
