<?php

namespace app\models\statistics;

use Yii;

/**
 * This is the model class for table "{{%statistic__point_per_day}}".
 *
 * @property int $id
 * @property string $point_id
 * @property int $countCanceledReservations
 * @property int $countCreatedReservations
 * @property int $countAcceptedReservations
 * @property int $countSuccessReservations
 * @property string $created_at
 */
class StatisticPointPerDay extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return '{{%statistic__point_per_day}}';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['point_id', 'countCanceledReservations', 'countCreatedReservations', 'countAcceptedReservations', 'countSuccessReservations'], 'required'],
            [['countCanceledReservations', 'countCreatedReservations', 'countAcceptedReservations', 'countSuccessReservations'], 'integer'],
            [['created_at'], 'safe'],
            [['point_id'], 'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'point_id' => 'Point ID',
            'countCanceledReservations' => 'Count Canceled Reservations',
            'countCreatedReservations' => 'Count Created Reservations',
            'countAcceptedReservations' => 'Count Accepted Reservations',
            'countSuccessReservations' => 'Count Success Reservations',
            'created_at' => 'Created At',
        ];
    }
}
