<?php

namespace app\models\statistics;

use Yii;

/**
 * This is the model class for table "{{%statistic__service_reservation_per_point_in_current_date}}".
 *
 * @property int $id
 * @property int $countCanceledReservations
 * @property int $countCreatedReservations
 * @property int $countAcceptedReservations
 * @property int $countSuccessReservations
 * @property string $point_id
 * @property string $service_id
 * @property string $created_at
 */
class StatisticServiceReservationPerPointInCurrentDate extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return '{{%statistic__service_reservation_per_point_in_current_date}}';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['countCanceledReservations', 'countCreatedReservations', 'countAcceptedReservations', 'countSuccessReservations', 'point_id', 'service_id'], 'required'],
            [['countCanceledReservations', 'countCreatedReservations', 'countAcceptedReservations', 'countSuccessReservations'], 'integer'],
            [['created_at'], 'safe'],
            [['point_id', 'service_id'], 'string', 'max' => 45],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'countCanceledReservations' => 'Count Canceled Reservations',
            'countCreatedReservations' => 'Count Created Reservations',
            'countAcceptedReservations' => 'Count Accepted Reservations',
            'countSuccessReservations' => 'Count Success Reservations',
            'point_id' => 'Point ID',
            'service_id' => 'Service ID',
            'created_at' => 'Created At',
        ];
    }
}
