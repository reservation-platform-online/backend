<?php

namespace app\models\promoCodes;

use app\models\business\BusinessCompany;
use Yii;
use yii\db\ActiveQuery;
use yii\db\ActiveRecord;

/**
 * This is the model class for table "{{%promo_code}}".
 *
 * @property string $id
 * @property string $business__company_id
 * @property int $is_percent
 * @property float $discount
 * @property string $code
 * @property int $enabled
 * @property int $hide
 * @property string|null $expires
 * @property int|null $limit
 *
 * @property BusinessCompany $businessCompany
 * @property PromoCodeBusinessReservation[] $promoCodeBusinessReservations
 * @property PromoCodeBusinessService[] $promoCodeBusinessServices
 */
class PromoCode extends ActiveRecord
{
    const ENABLED = 1;
    const DISABLED = 0;
    const SHOW = 0;
    const HIDE = 1;
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return '{{%promo_code}}';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['business__company_id', 'code'], 'required'],
            [['is_percent', 'enabled', 'hide', 'limit'], 'integer'],
            [['discount'], 'number'],
            [['expires'], 'safe'],
            [['id', 'business__company_id', 'code'], 'string', 'max' => 45],
            [['id'], 'unique'],
            [['business__company_id'], 'exist', 'skipOnError' => true, 'targetClass' => BusinessCompany::className(), 'targetAttribute' => ['business__company_id' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'business__company_id' => 'Business Company ID',
            'is_percent' => 'Is Percent',
            'discount' => 'Discount',
            'code' => 'Code',
            'enabled' => 'Enabled',
            'hide' => 'Hide',
            'expires' => 'Expires',
            'limit' => 'Limit',
        ];
    }

    /**
     * Gets query for [[BusinessCompany]].
     *
     * @return ActiveQuery
     */
    public function getBusinessCompany()
    {
        return $this->hasOne(BusinessCompany::className(), ['id' => 'business__company_id']);
    }

    /**
     * Gets query for [[PromoCodeBusinessReservations]].
     *
     * @return ActiveQuery
     */
    public function getPromoCodeBusinessReservations()
    {
        return $this->hasMany(PromoCodeBusinessReservation::className(), ['promo_code_id' => 'id']);
    }

    /**
     * Gets query for [[PromoCodeBusinessServices]].
     *
     * @return ActiveQuery
     */
    public function getPromoCodeBusinessServices()
    {
        return $this->hasMany(PromoCodeBusinessService::className(), ['promo_code_id' => 'id']);
    }
}
