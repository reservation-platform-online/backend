<?php

namespace app\models\promoCodes;

use app\models\business\BusinessReservation;
use Yii;
use yii\db\ActiveQuery;
use yii\db\ActiveRecord;

/**
 * This is the model class for table "{{%promo_code__business__reservation}}".
 *
 * @property string $promo_code_id
 * @property string $business__reservation_id
 *
 * @property BusinessReservation $businessReservation
 * @property PromoCode $promoCode
 */
class PromoCodeBusinessReservation extends ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return '{{%promo_code__business__reservation}}';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['promo_code_id', 'business__reservation_id'], 'required'],
            [['promo_code_id', 'business__reservation_id'], 'string', 'max' => 45],
            [['business__reservation_id'], 'exist', 'skipOnError' => true, 'targetClass' => BusinessReservation::className(), 'targetAttribute' => ['business__reservation_id' => 'id']],
            [['promo_code_id'], 'exist', 'skipOnError' => true, 'targetClass' => PromoCode::className(), 'targetAttribute' => ['promo_code_id' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'promo_code_id' => 'Promo Code ID',
            'business__reservation_id' => 'Business Reservation ID',
        ];
    }

    /**
     * Gets query for [[BusinessReservation]].
     *
     * @return ActiveQuery
     */
    public function getBusinessReservation()
    {
        return $this->hasOne(BusinessReservation::className(), ['id' => 'business__reservation_id']);
    }

    /**
     * Gets query for [[PromoCode]].
     *
     * @return ActiveQuery
     */
    public function getPromoCode()
    {
        return $this->hasOne(PromoCode::className(), ['id' => 'promo_code_id']);
    }
}
