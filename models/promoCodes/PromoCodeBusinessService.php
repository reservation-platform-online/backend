<?php

namespace app\models\promoCodes;

use app\models\business\BusinessService;
use Yii;
use yii\db\ActiveQuery;
use yii\db\ActiveRecord;

/**
 * This is the model class for table "{{%promo_code__business__service}}".
 *
 * @property string $promo_code_id
 * @property string $business__service_id
 *
 * @property BusinessService $businessService
 * @property PromoCode $promoCode
 */
class PromoCodeBusinessService extends ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return '{{%promo_code__business__service}}';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['promo_code_id', 'business__service_id'], 'required'],
            [['promo_code_id', 'business__service_id'], 'string', 'max' => 45],
            [['business__service_id'], 'exist', 'skipOnError' => true, 'targetClass' => BusinessService::className(), 'targetAttribute' => ['business__service_id' => 'id']],
            [['promo_code_id'], 'exist', 'skipOnError' => true, 'targetClass' => PromoCode::className(), 'targetAttribute' => ['promo_code_id' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'promo_code_id' => 'Promo Code ID',
            'business__service_id' => 'Business Service ID',
        ];
    }

    /**
     * Gets query for [[BusinessService]].
     *
     * @return ActiveQuery
     */
    public function getBusinessService()
    {
        return $this->hasOne(BusinessService::className(), ['id' => 'business__service_id']);
    }

    /**
     * Gets query for [[PromoCode]].
     *
     * @return ActiveQuery
     */
    public function getPromoCode()
    {
        return $this->hasOne(PromoCode::className(), ['id' => 'promo_code_id']);
    }
}
