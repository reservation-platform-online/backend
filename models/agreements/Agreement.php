<?php

namespace app\models\agreements;

use Yii;
use yii\db\ActiveQuery;
use yii\db\ActiveRecord;

/**
 * This is the model class for table "{{%agreement}}".
 *
 * @property int $id
 * @property string $title
 * @property string|null $description
 * @property int $is_required
 * @property int $enabled
 * @property int $hide
 *
 * @property AgreementUser[] $agreementUsers
 */
class Agreement extends ActiveRecord
{
    const ENABLED = 1;
    const DISABLED = 0;
    const SHOW = 0;
    const HIDE = 1;
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return '{{%agreement}}';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['title'], 'required'],
            [['description'], 'string'],
            [['is_required', 'enabled', 'hide'], 'integer'],
            [['title'], 'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'title' => 'Title',
            'description' => 'Description',
            'is_required' => 'Is Required',
            'enabled' => 'Enabled',
            'hide' => 'Hide',
        ];
    }

    /**
     * Gets query for [[AgreementUsers]].
     *
     * @return ActiveQuery
     */
    public function getAgreementUsers()
    {
        return $this->hasMany(AgreementUser::className(), ['agreement_id' => 'id']);
    }
}
