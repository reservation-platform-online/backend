<?php

namespace app\models\localities;

use Yii;

/**
 * This is the model class for table "{{%locality__children}}".
 *
 * @property int $parent_id
 * @property int $locality_id
 *
 * @property Locality $locality
 * @property Locality $parent
 */
class LocalityChildren extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return '{{%locality__children}}';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['parent_id', 'locality_id'], 'required'],
            [['parent_id', 'locality_id'], 'integer'],
            [['parent_id', 'locality_id'], 'unique', 'targetAttribute' => ['parent_id', 'locality_id']],
            [['locality_id'], 'exist', 'skipOnError' => true, 'targetClass' => Locality::className(), 'targetAttribute' => ['locality_id' => 'id']],
            [['parent_id'], 'exist', 'skipOnError' => true, 'targetClass' => Locality::className(), 'targetAttribute' => ['parent_id' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'parent_id' => 'Parent ID',
            'locality_id' => 'Locality ID',
        ];
    }

    /**
     * Gets query for [[Locality]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getLocality()
    {
        return $this->hasOne(Locality::className(), ['id' => 'locality_id']);
    }

    /**
     * Gets query for [[Parent]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getParent()
    {
        return $this->hasOne(Locality::className(), ['id' => 'parent_id']);
    }
}
