<?php

namespace app\models\localities;

use app\models\additional\AdditionalAddress;
use app\models\business\BusinessPoint;
use Yii;
use yii\db\ActiveQuery;
use yii\db\ActiveRecord;

/**
 * This is the model class for table "{{%locality}}".
 *
 * @property int $id
 * @property string $name
 * @property int $enabled
 * @property int $hide
 *
 * @property AdditionalAddress[] $additionalAddresses
 * @property BusinessPoint[] $businessPoints
 * @property LocalityChildren[] $localityChildren
 * @property LocalityChildren[] $localityChildren0
 * @property Locality[] $parents
 * @property Locality[] $localities
 */
class Locality extends ActiveRecord
{
    const ENABLED = 1;
    const DISABLED = 0;
    const SHOW = 0;
    const HIDE = 1;

    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return '{{%locality}}';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['name'], 'required'],
            [['enabled', 'hide'], 'integer'],
            [['name'], 'string', 'max' => 45],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Name',
            'enabled' => 'Enabled',
            'hide' => 'Hide',
        ];
    }

    /**
     * Gets query for [[AdditionalAddresses]].
     *
     * @return ActiveQuery
     */
    public function getAdditionalAddresses()
    {
        return $this->hasMany(AdditionalAddress::className(), ['locality_id' => 'id']);
    }

    /**
     * Gets query for [[BusinessPoints]].
     *
     * @return ActiveQuery
     */
    public function getBusinessPoints()
    {
        return $this->hasMany(BusinessPoint::className(), ['locality_id' => 'id']);
    }

    /**
     * Gets query for [[LocalityChildren]].
     *
     * @return ActiveQuery
     */
    public function getLocalityChildren()
    {
        return $this->hasMany(LocalityChildren::className(), ['locality_id' => 'id']);
    }

    /**
     * Gets query for [[LocalityChildren0]].
     *
     * @return ActiveQuery
     */
    public function getLocalityChildren0()
    {
        return $this->hasMany(LocalityChildren::className(), ['parent_id' => 'id']);
    }

    /**
     * Gets query for [[Parents]].
     *
     * @return ActiveQuery
     */
    public function getParents()
    {
        return $this->hasMany(Locality::className(), ['id' => 'parent_id'])->viaTable('{{%locality__children}}', ['locality_id' => 'id']);
    }

    /**
     * Gets query for [[Localities]].
     *
     * @return ActiveQuery
     */
    public function getLocalities()
    {
        return $this->hasMany(Locality::className(), ['id' => 'locality_id'])->viaTable('{{%locality__children}}', ['parent_id' => 'id']);
    }
}
