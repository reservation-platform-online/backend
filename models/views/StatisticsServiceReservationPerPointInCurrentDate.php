<?php

namespace app\models\views;

use Yii;
use yii\db\ActiveRecord;

/**
 * This is the model class for table "{{%statistics__service_reservation_per_point_in_current_date}}".
 *
 * @property string $point_id
 * @property string $service_id
 * @property float|null $countCanceledReservations
 * @property float|null $countCreatedReservations
 * @property float|null $countAcceptedReservations
 * @property float|null $countSuccessReservations
 */
class StatisticsServiceReservationPerPointInCurrentDate extends ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return '{{%statistics__service_reservation_per_point_in_current_date}}';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['point_id', 'service_id'], 'required'],
            [['countCanceledReservations', 'countCreatedReservations', 'countAcceptedReservations', 'countSuccessReservations'], 'number'],
            [['point_id', 'service_id'], 'string', 'max' => 45],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'point_id' => 'Point ID',
            'service_id' => 'Service ID',
            'countCanceledReservations' => 'Count Canceled Reservations',
            'countCreatedReservations' => 'Count Created Reservations',
            'countAcceptedReservations' => 'Count Accepted Reservations',
            'countSuccessReservations' => 'Count Success Reservations',
        ];
    }
}
