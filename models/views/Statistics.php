<?php

namespace app\models\views;

use Yii;

/**
 * This is the model class for table "{{%statistics}}".
 *
 * @property int|null $users
 * @property int|null $users_confirmed
 * @property int|null $reservations
 * @property int|null $reservations_confirmed
 * @property int|null $workers
 * @property int|null $owners
 * @property int|null $receptions
 * @property int|null $points
 * @property int|null $companies
 */
class Statistics extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return '{{%statistics}}';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['users', 'users_confirmed', 'reservations', 'reservations_confirmed', 'workers', 'owners', 'receptions', 'points', 'companies'], 'integer'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'users' => 'Users',
            'users_confirmed' => 'Users Confirmed',
            'reservations' => 'Reservations',
            'reservations_confirmed' => 'Reservations Confirmed',
            'workers' => 'Workers',
            'owners' => 'Owners',
            'receptions' => 'Receptions',
            'points' => 'Points',
            'companies' => 'Companies',
        ];
    }
}
