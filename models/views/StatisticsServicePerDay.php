<?php

namespace app\models\views;

use Yii;
use yii\db\ActiveRecord;

/**
 * This is the model class for table "{{%statistics__service_per_day}}".
 *
 * @property string $service_id
 * @property float|null $countCanceledReservations
 * @property float|null $countCreatedReservations
 * @property float|null $countAcceptedReservations
 * @property float|null $countSuccessReservations
 */
class StatisticsServicePerDay extends ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return '{{%statistics__service_per_day}}';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['service_id'], 'required'],
            [['countCanceledReservations', 'countCreatedReservations', 'countAcceptedReservations', 'countSuccessReservations'], 'number'],
            [['service_id'], 'string', 'max' => 45],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'service_id' => 'Service ID',
            'countCanceledReservations' => 'Count Canceled Reservations',
            'countCreatedReservations' => 'Count Created Reservations',
            'countAcceptedReservations' => 'Count Accepted Reservations',
            'countSuccessReservations' => 'Count Success Reservations',
        ];
    }
}
