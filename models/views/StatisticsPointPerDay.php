<?php

namespace app\models\views;

use Yii;
use yii\db\ActiveRecord;

/**
 * This is the model class for table "{{%statistics__point_per_day}}".
 *
 * @property string $point_id
 * @property float|null $countCanceledReservations
 * @property float|null $countCreatedReservations
 * @property float|null $countAcceptedReservations
 * @property float|null $countSuccessReservations
 */
class StatisticsPointPerDay extends ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return '{{%statistics__point_per_day}}';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['point_id'], 'required'],
            [['countCanceledReservations', 'countCreatedReservations', 'countAcceptedReservations', 'countSuccessReservations'], 'number'],
            [['point_id'], 'string', 'max' => 45],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'point_id' => 'Point ID',
            'countCanceledReservations' => 'Count Canceled Reservations',
            'countCreatedReservations' => 'Count Created Reservations',
            'countAcceptedReservations' => 'Count Accepted Reservations',
            'countSuccessReservations' => 'Count Success Reservations',
        ];
    }
}
