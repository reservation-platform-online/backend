<?php

namespace app\models\categories;

use app\models\business\BusinessService;
use Yii;
use yii\db\ActiveQuery;
use yii\db\ActiveRecord;

/**
 * This is the model class for table "{{%category}}".
 *
 * @property int $id
 * @property string $name
 * @property string $color
 * @property string|null $icon
 * @property int $number_of_children
 * @property int $number_of_services
 * @property int $enabled
 * @property int $hide
 *
 * @property BusinessService[] $businessServices
 * @property CategoryChild[] $categoryChildren
 * @property CategoryChild[] $categoryChildren0
 * @property Category[] $parents
 * @property Category[] $categories
 */
class Category extends ActiveRecord
{

    const ENABLED = 1;
    const DISABLED = 0;
    const SHOW = 0;
    const HIDE = 1;

    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return '{{%category}}';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['name'], 'required'],
            [['number_of_children', 'number_of_services', 'enabled', 'hide'], 'integer'],
            [['name', 'icon'], 'string', 'max' => 45],
            [['color'], 'string', 'max' => 25],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Name',
            'color' => 'Color',
            'icon' => 'Icon',
            'number_of_children' => 'Number Of Children',
            'number_of_services' => 'Number Of Services',
            'enabled' => 'Enabled',
            'hide' => 'Hide',
        ];
    }

    /**
     * Gets query for [[BusinessServices]].
     *
     * @return ActiveQuery
     */
    public function getBusinessServices()
    {
        return $this->hasMany(BusinessService::className(), ['category_id' => 'id']);
    }

    /**
     * Gets query for [[CategoryChildren]].
     *
     * @return ActiveQuery
     */
    public function getCategoryChildren()
    {
        return $this->hasMany(CategoryChild::className(), ['category_id' => 'id']);
    }

    /**
     * Gets query for [[CategoryChildren0]].
     *
     * @return ActiveQuery
     */
    public function getCategoryChildren0()
    {
        return $this->hasMany(CategoryChild::className(), ['parent_id' => 'id']);
    }

    /**
     * Gets query for [[Parents]].
     *
     * @return ActiveQuery
     */
    public function getParents()
    {
        return $this->hasMany(Category::className(), ['id' => 'parent_id'])->viaTable('{{%category__child}}', ['category_id' => 'id']);
    }

    /**
     * Gets query for [[Categories]].
     *
     * @return ActiveQuery
     */
    public function getCategories()
    {
        return $this->hasMany(Category::className(), ['id' => 'category_id'])->viaTable('{{%category__child}}', ['parent_id' => 'id']);
    }
}
