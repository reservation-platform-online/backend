<?php

namespace app\models;

use Yii;
use yii\db\ActiveQuery;
use yii\db\ActiveRecord;

/**
 * This is the model class for table "{{%user__history_password}}".
 *
 * @property string $id
 * @property string $user_id
 * @property string $password_hash
 * @property string $created_at
 *
 * @property User $user
 */
class UserHistoryPassword extends ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return '{{%user__history_password}}';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['user_id', 'password_hash'], 'required'],
            [['created_at'], 'safe'],
            [['id', 'user_id'], 'string', 'max' => 45],
            [['password_hash'], 'string', 'max' => 255],
            [['id'], 'unique'],
            [['user_id', 'password_hash'], 'unique', 'targetAttribute' => ['user_id', 'password_hash']],
            [['user_id'], 'exist', 'skipOnError' => true, 'targetClass' => User::className(), 'targetAttribute' => ['user_id' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'user_id' => 'User ID',
            'password_hash' => 'Password Hash',
            'created_at' => 'Created At',
        ];
    }

    /**
     * Gets query for [[User]].
     *
     * @return ActiveQuery
     */
    public function getUser()
    {
        return $this->hasOne(User::className(), ['id' => 'user_id']);
    }
}
