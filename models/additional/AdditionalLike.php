<?php

namespace app\models\additional;

use app\models\business\BusinessPointWorkerService;
use app\models\User;
use Yii;
use yii\db\ActiveQuery;
use yii\db\ActiveRecord;

/**
 * This is the model class for table "{{%additional__like}}".
 *
 * @property string $id
 * @property string $business__point_worker_service_id
 * @property string $user_id
 * @property int $enabled
 * @property string $created_at
 * @property string $updated_at
 *
 * @property BusinessPointWorkerService $businessPointWorkerService
 * @property User $user
 */
class AdditionalLike extends ActiveRecord
{
    const ENABLED = 1;
    const DISABLED = 0;
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return '{{%additional__like}}';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['business__point_worker_service_id', 'user_id'], 'required'],
            [['enabled'], 'integer'],
            [['created_at', 'updated_at'], 'safe'],
            [['id', 'business__point_worker_service_id', 'user_id'], 'string', 'max' => 45],
            [['id'], 'unique'],
            [['business__point_worker_service_id'], 'exist', 'skipOnError' => true, 'targetClass' => BusinessPointWorkerService::className(), 'targetAttribute' => ['business__point_worker_service_id' => 'id']],
            [['user_id'], 'exist', 'skipOnError' => true, 'targetClass' => User::className(), 'targetAttribute' => ['user_id' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'business__point_worker_service_id' => 'Business Point Worker Service ID',
            'user_id' => 'User ID',
            'enabled' => 'Enabled',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
        ];
    }

    /**
     * Gets query for [[BusinessPointWorkerService]].
     *
     * @return ActiveQuery
     */
    public function getBusinessPointWorkerService()
    {
        return $this->hasOne(BusinessPointWorkerService::className(), ['id' => 'business__point_worker_service_id']);
    }

    /**
     * Gets query for [[User]].
     *
     * @return ActiveQuery
     */
    public function getUser()
    {
        return $this->hasOne(User::className(), ['id' => 'user_id']);
    }
}
