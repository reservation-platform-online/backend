<?php

namespace app\models\additional;

use app\models\business\BusinessReservationUser;
use Yii;
use yii\db\ActiveQuery;
use yii\db\ActiveRecord;

/**
 * This is the model class for table "{{%additional__address__business__reservation__user}}".
 *
 * @property string $additional__address_id
 * @property string $business__reservation__user_id
 *
 * @property AdditionalAddress $additionalAddress
 * @property BusinessReservationUser $businessReservationUser
 */
class AdditionalAddressBusinessReservationUser extends ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return '{{%additional__address__business__reservation__user}}';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['additional__address_id', 'business__reservation__user_id'], 'required'],
            [['additional__address_id', 'business__reservation__user_id'], 'string', 'max' => 45],
            [['additional__address_id'], 'exist', 'skipOnError' => true, 'targetClass' => AdditionalAddress::className(), 'targetAttribute' => ['additional__address_id' => 'id']],
            [['business__reservation__user_id'], 'exist', 'skipOnError' => true, 'targetClass' => BusinessReservationUser::className(), 'targetAttribute' => ['business__reservation__user_id' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'additional__address_id' => 'Additional Address ID',
            'business__reservation__user_id' => 'Business Reservation User ID',
        ];
    }

    /**
     * Gets query for [[AdditionalAddress]].
     *
     * @return ActiveQuery
     */
    public function getAdditionalAddress()
    {
        return $this->hasOne(AdditionalAddress::className(), ['id' => 'additional__address_id']);
    }

    /**
     * Gets query for [[BusinessReservationUser]].
     *
     * @return ActiveQuery
     */
    public function getBusinessReservationUser()
    {
        return $this->hasOne(BusinessReservationUser::className(), ['id' => 'business__reservation__user_id']);
    }
}
