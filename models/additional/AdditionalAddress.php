<?php

namespace app\models\additional;

use app\models\localities\Locality;
use Yii;
use yii\db\ActiveQuery;
use yii\db\ActiveRecord;

/**
 * This is the model class for table "{{%additional__address}}".
 *
 * @property string $id
 * @property int $locality_id
 * @property string $postal_index
 * @property string $address_line
 * @property string|null $address_line2
 * @property string|null $note
 * @property int $enabled
 * @property int $hide
 * @property int $status
 * @property string $created_at
 * @property string $updated_at
 *
 * @property Locality $locality
 * @property AdditionalAddressBusinessReservationUser[] $additionalAddressBusinessReservationUsers
 * @property AdditionalAddressUser[] $additionalAddressUsers
 */
class AdditionalAddress extends ActiveRecord
{
    const ENABLED = 1;
    const DISABLED = 0;
    const SHOW = 0;
    const HIDE = 1;

    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return '{{%additional__address}}';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['locality_id', 'postal_index', 'address_line'], 'required'],
            [['locality_id', 'enabled', 'hide', 'status'], 'integer'],
            [['address_line', 'address_line2', 'note'], 'string'],
            [['created_at', 'updated_at'], 'safe'],
            [['id', 'postal_index'], 'string', 'max' => 45],
            [['id'], 'unique'],
            [['locality_id'], 'exist', 'skipOnError' => true, 'targetClass' => Locality::className(), 'targetAttribute' => ['locality_id' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'locality_id' => 'Locality ID',
            'postal_index' => 'Postal Index',
            'address_line' => 'Address Line',
            'address_line2' => 'Address Line2',
            'note' => 'Note',
            'enabled' => 'Enabled',
            'hide' => 'Hide',
            'status' => 'Status',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
        ];
    }

    /**
     * Gets query for [[Locality]].
     *
     * @return ActiveQuery
     */
    public function getLocality()
    {
        return $this->hasOne(Locality::className(), ['id' => 'locality_id']);
    }

    /**
     * Gets query for [[AdditionalAddressBusinessReservationUsers]].
     *
     * @return ActiveQuery
     */
    public function getAdditionalAddressBusinessReservationUsers()
    {
        return $this->hasMany(AdditionalAddressBusinessReservationUser::className(), ['additional__address_id' => 'id']);
    }

    /**
     * Gets query for [[AdditionalAddressUsers]].
     *
     * @return ActiveQuery
     */
    public function getAdditionalAddressUsers()
    {
        return $this->hasMany(AdditionalAddressUser::className(), ['additional__address_id' => 'id']);
    }
}
