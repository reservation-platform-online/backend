<?php

namespace app\models\additional;

use app\models\User;
use Yii;
use yii\db\ActiveQuery;
use yii\db\ActiveRecord;

/**
 * This is the model class for table "{{%additional__address__user}}".
 *
 * @property string $additional__address_id
 * @property string $user_id
 *
 * @property AdditionalAddress $additionalAddress
 * @property User $user
 */
class AdditionalAddressUser extends ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return '{{%additional__address__user}}';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['additional__address_id', 'user_id'], 'required'],
            [['additional__address_id', 'user_id'], 'string', 'max' => 45],
            [['additional__address_id'], 'exist', 'skipOnError' => true, 'targetClass' => AdditionalAddress::className(), 'targetAttribute' => ['additional__address_id' => 'id']],
            [['user_id'], 'exist', 'skipOnError' => true, 'targetClass' => User::className(), 'targetAttribute' => ['user_id' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'additional__address_id' => 'Additional Address ID',
            'user_id' => 'User ID',
        ];
    }

    /**
     * Gets query for [[AdditionalAddress]].
     *
     * @return ActiveQuery
     */
    public function getAdditionalAddress()
    {
        return $this->hasOne(AdditionalAddress::className(), ['id' => 'additional__address_id']);
    }

    /**
     * Gets query for [[User]].
     *
     * @return ActiveQuery
     */
    public function getUser()
    {
        return $this->hasOne(User::className(), ['id' => 'user_id']);
    }
}
