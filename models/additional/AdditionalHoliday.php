<?php

namespace app\models\additional;

use Yii;
use yii\db\ActiveQuery;
use yii\db\ActiveRecord;

/**
 * This is the model class for table "{{%additional__holiday}}".
 *
 * @property int $id
 * @property string $name
 * @property string $start_at
 * @property string $end_at
 * @property int $enabled
 *
 * @property AdditionalWeekend[] $additionalWeekends
 */
class AdditionalHoliday extends ActiveRecord
{
    const ENABLED = 1;
    const DISABLED = 0;
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return '{{%additional__holiday}}';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['name', 'start_at', 'end_at'], 'required'],
            [['start_at', 'end_at'], 'safe'],
            [['enabled'], 'integer'],
            [['name'], 'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Name',
            'start_at' => 'Start At',
            'end_at' => 'End At',
            'enabled' => 'Enabled',
        ];
    }

    /**
     * Gets query for [[AdditionalWeekends]].
     *
     * @return ActiveQuery
     */
    public function getAdditionalWeekends()
    {
        return $this->hasMany(AdditionalWeekend::className(), ['additional__holiday_id' => 'id']);
    }
}
