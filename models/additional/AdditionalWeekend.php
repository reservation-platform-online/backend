<?php

namespace app\models\additional;

use app\models\business\BusinessWorker;
use Yii;
use yii\db\ActiveQuery;
use yii\db\ActiveRecord;

/**
 * This is the model class for table "{{%additional__weekend}}".
 *
 * @property string $id
 * @property int $additional__holiday_id
 * @property string $business__worker_id
 * @property string $start_at
 * @property string $end_at
 * @property int $enabled
 * @property string $created_at
 * @property string $updated_at
 *
 * @property AdditionalHoliday $additionalHoliday
 * @property BusinessWorker $businessWorker
 */
class AdditionalWeekend extends ActiveRecord
{
    const ENABLED = 1;
    const DISABLED = 0;
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return '{{%additional__weekend}}';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['additional__holiday_id', 'business__worker_id', 'start_at', 'end_at'], 'required'],
            [['additional__holiday_id', 'enabled'], 'integer'],
            [['start_at', 'end_at', 'created_at', 'updated_at'], 'safe'],
            [['id', 'business__worker_id'], 'string', 'max' => 45],
            [['id'], 'unique'],
            [['additional__holiday_id'], 'exist', 'skipOnError' => true, 'targetClass' => AdditionalHoliday::className(), 'targetAttribute' => ['additional__holiday_id' => 'id']],
            [['business__worker_id'], 'exist', 'skipOnError' => true, 'targetClass' => BusinessWorker::className(), 'targetAttribute' => ['business__worker_id' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'additional__holiday_id' => 'Additional Holiday ID',
            'business__worker_id' => 'Business Worker ID',
            'start_at' => 'Start At',
            'end_at' => 'End At',
            'enabled' => 'Enabled',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
        ];
    }

    /**
     * Gets query for [[AdditionalHoliday]].
     *
     * @return ActiveQuery
     */
    public function getAdditionalHoliday()
    {
        return $this->hasOne(AdditionalHoliday::className(), ['id' => 'additional__holiday_id']);
    }

    /**
     * Gets query for [[BusinessWorker]].
     *
     * @return ActiveQuery
     */
    public function getBusinessWorker()
    {
        return $this->hasOne(BusinessWorker::className(), ['id' => 'business__worker_id']);
    }
}
