<?php

namespace app\models;

use app\models\additional\AdditionalAddressUser;
use app\models\additional\AdditionalLike;
use app\models\agreements\AgreementUser;
use app\models\authorizations\Authorization;
use app\models\authorizations\AuthorizationExternal;
use app\models\business\BusinessCompanyUser;
use app\models\business\BusinessReservationUser;
use app\models\business\BusinessWorker;
use app\models\contacts\ContactUser;
use Yii;
use yii\base\Exception;
use yii\db\ActiveQuery;
use yii\db\ActiveRecord;
use yii\web\IdentityInterface;

/**
 * This is the model class for table "{{%user}}".
 *
 * @property string $id
 * @property string $email
 * @property string $first_name
 * @property string $last_name
 * @property string|null $password_hash
 * @property string|null $password_reset_token
 * @property string|null $verification_token
 * @property string|null $photo
 * @property string|null $phone
 * @property string|null $birthday
 * @property int $role 0 - Admin 1 - Company 2 - User
 * @property int $number_of_violations
 * @property int $singel_company_is_created
 * @property int $hide
 * @property int $enabled
 * @property int $status
 * @property string $created_at
 * @property string $updated_at
 *
 * @property AdditionalAddressUser[] $additionalAddressUsers
 * @property AdditionalLike[] $additionalLikes
 * @property AgreementUser[] $agreementUsers
 * @property Authorization[] $authorizations
 * @property AuthorizationExternal[] $authorizationExternals
 * @property BusinessCompanyUser[] $businessCompanyUsers
 * @property BusinessReservationUser[] $businessReservationUsers
 * @property BusinessWorker[] $businessWorkers
 * @property ContactUser[] $contactUsers
 * @property UserHistoryPassword[] $userHistoryPasswords
 */
class User extends ActiveRecord implements IdentityInterface
{
    const ENABLED = 1;
    const DISABLED = 0;
    const SHOW = 0;
    const HIDE = 1;

    const STATUS_DELETED = 0;
    const STATUS_ACTIVE = 1;
    const STATUS_CONFIRMED_EMAIL = 2;
    const STATUS_REGISTRATION_IS_NOT_FINISHED = 3;

    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return '{{%user}}';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['email', 'first_name', 'last_name'], 'required'],
            [['photo'], 'string'],
            [['birthday', 'created_at', 'updated_at'], 'safe'],
            [['role', 'number_of_violations', 'hide', 'enabled', 'status', 'singel_company_is_created'], 'integer'],
            [['id', 'first_name', 'last_name', 'phone'], 'string', 'max' => 45],
            [['email', 'password_hash', 'password_reset_token', 'verification_token'], 'string', 'max' => 255],
            [['id'], 'unique'],
            [['email'], 'unique'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'email' => 'Email',
            'first_name' => 'First Name',
            'last_name' => 'Last Name',
            'password_hash' => 'Password Hash',
            'password_reset_token' => 'Password Reset Token',
            'verification_token' => 'Verification Token',
            'photo' => 'Photo',
            'phone' => 'Phone',
            'singel_company_is_created' => 'Singel company is created',
            'birthday' => 'Birthday',
            'role' => '0 - Admin
1 - Company
2 - User',
            'number_of_violations' => 'Number Of Violations',
            'hide' => 'Hide',
            'enabled' => 'Enabled',
            'status' => 'Status',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
        ];
    }

    public function updateAuthorizationExternal($oldToken, $token)
    {

        $authorizationExternal = AuthorizationExternal::find()->where([
            'access_token' => $oldToken['access_token'],
            'refresh_token' => $oldToken['refresh_token']
        ])->one();

        if ($authorizationExternal && count((array)$token) > 0) {

            $authorizationExternal->access_token = $token->access_token;
            $authorizationExternal->token_type = $token->token_type;
            $authorizationExternal->refresh_token = $token->refresh_token;
            $authorizationExternal->expires_in = $token->expires_in;
            $authorizationExternal->scope = $token->scope;

            return $authorizationExternal->validate() && $authorizationExternal->save();

        }

        return false;

    }

    protected function _sendEmail($email, $subject, $options)
    {

        $emailSend = Yii::$app->mailer->compose(['html' => $options['html']], $options['variables'])
            ->setFrom([Yii::$app->params['infoEmail'] => Yii::$app->params['nameOfInfoEmail']])
            ->setTo($email)
            ->setSubject($subject);

        return $emailSend->send();
    }

    function sendEmail(
        $sendPassword = false
    )
    {

        $EMAIL_TEXT = 'Hejka, dziękujemy za rejestracje na naszej platformie, kliknij poniższy przycisk, aby potwierdzić swój e-mail adres.';
        $EMAIL_SUBJECT = 'Zakończenia rejestracji i potwierdzenia E-mail';

        if ($sendPassword) {

            $EMAIL_TEXT .= "<p>Twoje tymczasowe hasło: <b>$sendPassword</b></p>";

        }

        $this->_sendEmail($this->email, $EMAIL_SUBJECT, [
            'html' => '@app/mail/layouts/user-confirm',
            'variables' => [
                'content' => $EMAIL_TEXT,
                'linkToFinishRegistration' => 'https://reservation-platform-online/confirm/' . $this->verification_token
            ]
        ]);

    }

    function sendEmailSetPassword()
    {

        $EMAIL_TEXT = 'Hejka, potrzebnie wprowadzić nowe hasło';
        $EMAIL_SUBJECT = 'Reset hasła';

        $this->_sendEmail($this->email, $EMAIL_SUBJECT, [
            'html' => '@app/mail/layouts/html',
            'variables' => [
                'content' => $EMAIL_TEXT
            ]
        ]);

    }


    function sendEmailNewPassword($password)
    {

        $EMAIL_TEXT = 'Hejka, to jest twoje nowe hasło: ' . $password;
        $EMAIL_SUBJECT = 'Nowe hasło';

        $this->_sendEmail($this->email, $EMAIL_SUBJECT, [
            'html' => '@app/mail/layouts/new-password',
            'variables' => [
                'content' => $EMAIL_TEXT
            ]
        ]);

    }

    /**
     * Gets query for [[AdditionalAddressUsers]].
     *
     * @return ActiveQuery
     */
    public function getAdditionalAddressUsers()
    {
        return $this->hasMany(AdditionalAddressUser::className(), ['user_id' => 'id']);
    }

    /**
     * Gets query for [[AdditionalLikes]].
     *
     * @return ActiveQuery
     */
    public function getAdditionalLikes()
    {
        return $this->hasMany(AdditionalLike::className(), ['user_id' => 'id']);
    }

    /**
     * Gets query for [[AgreementUsers]].
     *
     * @return ActiveQuery
     */
    public function getAgreementUsers()
    {
        return $this->hasMany(AgreementUser::className(), ['user_id' => 'id']);
    }

    /**
     * Gets query for [[Authorizations]].
     *
     * @return ActiveQuery
     */
    public function getAuthorizations()
    {
        return $this->hasMany(Authorization::className(), ['user_id' => 'id']);
    }

    /**
     * Gets query for [[AuthorizationExternals]].
     *
     * @return ActiveQuery
     */
    public function getAuthorizationExternals()
    {
        return $this->hasMany(AuthorizationExternal::className(), ['user_id' => 'id']);
    }

    /**
     * Gets query for [[BusinessCompanyUsers]].
     *
     * @return ActiveQuery
     */
    public function getBusinessCompanyUsers()
    {
        return $this->hasMany(BusinessCompanyUser::className(), ['user_id' => 'id']);
    }

    /**
     * Gets query for [[BusinessReservationUsers]].
     *
     * @return ActiveQuery
     */
    public function getBusinessReservationUsers()
    {
        return $this->hasMany(BusinessReservationUser::className(), ['user_id' => 'id']);
    }

    /**
     * Gets query for [[BusinessWorkers]].
     *
     * @return ActiveQuery
     */
    public function getBusinessWorkers()
    {
        return $this->hasMany(BusinessWorker::className(), ['user_id' => 'id']);
    }

    /**
     * Gets query for [[ContactUsers]].
     *
     * @return ActiveQuery
     */
    public function getContactUsers()
    {
        return $this->hasMany(ContactUser::className(), ['user_id' => 'id']);
    }

    /**
     * Gets query for [[UserHistoryPasswords]].
     *
     * @return ActiveQuery
     */
    public function getUserHistoryPasswords()
    {
        return $this->hasMany(UserHistoryPassword::className(), ['user_id' => 'id']);
    }

    public static function findIdentityByAccessToken($token, $type = null)
    {

        return static::find()
            ->joinWith(['authorizations a'], true, 'INNER JOIN')
            ->where([
                'a.id' => $token,
                'a.enabled' => Authorization::ENABLED
            ])
            ->andWhere([
                'in', 'status', [self::STATUS_CONFIRMED_EMAIL, self::STATUS_ACTIVE],
            ])
            ->one();

    }

    /**
     * {@inheritdoc}
     */
    public static function findIdentity($id)
    {
        return static::findOne(['id' => $id, 'status' => self::STATUS_CONFIRMED_EMAIL]);
    }

    /**
     * Validates password
     *
     * @param string $password password to validate
     * @return bool if password provided is valid for current user
     */
    public function validatePassword($password)
    {
        return Yii::$app->security->validatePassword($password, $this->password_hash);
    }

    /**
     * Generates password hash from password and sets it to the model
     *
     * @param string $password
     * @throws Exception
     */
    public function setPassword($password)
    {
        $this->password_hash = Yii::$app->security->generatePasswordHash($password);
    }

    /**
     * Generates new password reset token
     * @throws Exception
     */
    public function generatePasswordResetToken()
    {
        $this->password_reset_token = Yii::$app->security->generateRandomString() . '_' . time();
    }

    /**
     * Removes password reset token
     */
    public function removePasswordResetToken()
    {
        $this->password_reset_token = null;
    }

    /**
     * Finds user by email
     *
     * @param string $email
     * @return User|array|ActiveRecord|null
     */
    public static function findByEmail($email)
    {
        return static::find()->where(['email' => $email])->andWhere(['in', 'status', [self::STATUS_ACTIVE, self::STATUS_CONFIRMED_EMAIL]])->one();
    }

    /**
     * {@inheritdoc}
     */
    public function getId()
    {
        return $this->getPrimaryKey();
    }

    /**
     * {@inheritdoc}
     */
    public function validateAuthKey($authKey)
    {
        return $this->getAuthKey() === $authKey;
    }

    /**
     * @inheritDoc
     */
    public function getAuthKey()
    {
        // TODO: Implement getAuthKey() method.
    }

}
