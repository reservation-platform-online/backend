<?php

namespace app\models\galleries;

use app\models\business\BusinessService;
use Yii;
use yii\db\ActiveQuery;
use yii\db\ActiveRecord;

/**
 * This is the model class for table "{{%gallery__service}}".
 *
 * @property string $gallery_id
 * @property string $business__service_id
 *
 * @property BusinessService $businessService
 * @property Gallery $gallery
 */
class GalleryService extends ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return '{{%gallery__service}}';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['gallery_id', 'business__service_id'], 'required'],
            [['gallery_id', 'business__service_id'], 'string', 'max' => 45],
            [['business__service_id'], 'exist', 'skipOnError' => true, 'targetClass' => BusinessService::className(), 'targetAttribute' => ['business__service_id' => 'id']],
            [['gallery_id'], 'exist', 'skipOnError' => true, 'targetClass' => Gallery::className(), 'targetAttribute' => ['gallery_id' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'gallery_id' => 'Gallery ID',
            'business__service_id' => 'Business Service ID',
        ];
    }

    /**
     * Gets query for [[BusinessService]].
     *
     * @return ActiveQuery
     */
    public function getBusinessService()
    {
        return $this->hasOne(BusinessService::className(), ['id' => 'business__service_id']);
    }

    /**
     * Gets query for [[Gallery]].
     *
     * @return ActiveQuery
     */
    public function getGallery()
    {
        return $this->hasOne(Gallery::className(), ['id' => 'gallery_id']);
    }
}
