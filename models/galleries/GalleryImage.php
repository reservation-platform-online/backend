<?php

namespace app\models\galleries;

use Yii;
use yii\db\ActiveQuery;
use yii\db\ActiveRecord;

/**
 * This is the model class for table "{{%gallery__image}}".
 *
 * @property string $id
 * @property string $gallery_id
 * @property string $link
 * @property string|null $name
 * @property int $order
 * @property int $enabled
 * @property int $hide
 * @property string $created_at
 * @property string $updated_at
 *
 * @property Gallery $gallery
 */
class GalleryImage extends ActiveRecord
{

    const ENABLED = 1;
    const DISABLED = 0;
    const SHOW = 0;
    const HIDE = 1;

    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return '{{%gallery__image}}';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['gallery_id', 'link'], 'required'],
            [['link'], 'string'],
            [['enabled', 'hide', 'order'], 'integer'],
            [['created_at', 'updated_at'], 'safe'],
            [['id', 'gallery_id', 'name'], 'string', 'max' => 45],
            [['id'], 'unique'],
            [['gallery_id'], 'exist', 'skipOnError' => true, 'targetClass' => Gallery::className(), 'targetAttribute' => ['gallery_id' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'gallery_id' => 'Gallery ID',
            'link' => 'Link',
            'order' => 'Order',
            'name' => 'Name',
            'enabled' => 'Enabled',
            'hide' => 'Hide',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
        ];
    }

    /**
     * Gets query for [[Gallery]].
     *
     * @return ActiveQuery
     */
    public function getGallery()
    {
        return $this->hasOne(Gallery::className(), ['id' => 'gallery_id']);
    }
}
