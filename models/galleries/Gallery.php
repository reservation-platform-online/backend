<?php

namespace app\models\galleries;

use Yii;
use yii\db\ActiveQuery;
use yii\db\ActiveRecord;

/**
 * This is the model class for table "{{%gallery}}".
 *
 * @property string $id
 * @property string $name
 * @property int $number_of_images
 * @property int $enabled
 * @property int $hide
 * @property string $created_at
 * @property string $updated_at
 *
 * @property GalleryCompany[] $galleryCompanies
 * @property GalleryImage[] $galleryImages
 * @property GalleryService[] $galleryServices
 */
class Gallery extends ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return '{{%gallery}}';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [[ 'name'], 'required'],
            [['number_of_images', 'enabled', 'hide'], 'integer'],
            [['created_at', 'updated_at'], 'safe'],
            [['id', 'name'], 'string', 'max' => 45],
            [['id'], 'unique'],
        ];
    }

    public function beforeSave($insert) {

        if ($this->isNewRecord) {
            $this->id = Yii::$app->db->createCommand('select uuid_v4s()')->queryScalar();
        }

        return parent::beforeSave($insert);

    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Name',
            'number_of_images' => 'Number Of Images',
            'enabled' => 'Enabled',
            'hide' => 'Hide',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
        ];
    }

    /**
     * Gets query for [[GalleryCompanies]].
     *
     * @return ActiveQuery
     */
    public function getGalleryCompanies()
    {
        return $this->hasMany(GalleryCompany::className(), ['gallery_id' => 'id']);
    }

    /**
     * Gets query for [[GalleryImages]].
     *
     * @return ActiveQuery
     */
    public function getGalleryImages()
    {
        return $this->hasMany(GalleryImage::className(), ['gallery_id' => 'id']);
    }

    /**
     * Gets query for [[GalleryServices]].
     *
     * @return ActiveQuery
     */
    public function getGalleryServices()
    {
        return $this->hasMany(GalleryService::className(), ['gallery_id' => 'id']);
    }
}
