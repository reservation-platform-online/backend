<?php

namespace app\models\contacts;

use app\models\types\TypeContact;
use Yii;
use yii\db\ActiveQuery;
use yii\db\ActiveRecord;

/**
 * This is the model class for table "{{%contact}}".
 *
 * @property string $id
 * @property string $value
 * @property string|null $name
 * @property int $status
 * @property int $enabled
 * @property int $hide
 * @property string $created_at
 * @property string $updated_at
 * @property string $type__contact_id
 *
 * @property TypeContact $typeContact
 * @property ContactBusinessCompany[] $contactBusinessCompanies
 * @property ContactUser[] $contactUsers
 */
class Contact extends ActiveRecord
{
    const ENABLED = 1;
    const DISABLED = 0;
    const SHOW = 0;
    const HIDE = 1;
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return '{{%contact}}';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['value', 'type__contact_id'], 'required'],
            [['status', 'enabled', 'hide'], 'integer'],
            [['created_at', 'updated_at'], 'safe'],
            [['id', 'name', 'type__contact_id'], 'string', 'max' => 45],
            [['value'], 'string', 'max' => 100],
            [['id'], 'unique'],
            [['type__contact_id'], 'exist', 'skipOnError' => true, 'targetClass' => TypeContact::className(), 'targetAttribute' => ['type__contact_id' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'value' => 'Value',
            'name' => 'Name',
            'status' => 'Status',
            'enabled' => 'Enabled',
            'hide' => 'Hide',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
            'type__contact_id' => 'Type Contact ID',
        ];
    }

    /**
     * Gets query for [[TypeContact]].
     *
     * @return ActiveQuery
     */
    public function getTypeContact()
    {
        return $this->hasOne(TypeContact::className(), ['id' => 'type__contact_id']);
    }

    /**
     * Gets query for [[ContactBusinessCompanies]].
     *
     * @return ActiveQuery
     */
    public function getContactBusinessCompanies()
    {
        return $this->hasMany(ContactBusinessCompany::className(), ['contact_id' => 'id']);
    }

    /**
     * Gets query for [[ContactUsers]].
     *
     * @return ActiveQuery
     */
    public function getContactUsers()
    {
        return $this->hasMany(ContactUser::className(), ['contact_id' => 'id']);
    }
}
