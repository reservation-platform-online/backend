<?php

namespace app\models\contacts;

use Yii;

/**
 * This is the model class for table "{{%contact__business__company}}".
 *
 * @property string $contact_id
 * @property string $business__company_id
 *
 * @property BusinessCompany $businessCompany
 * @property Contact $contact
 */
class ContactBusinessCompany extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return '{{%contact__business__company}}';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['contact_id', 'business__company_id'], 'required'],
            [['contact_id', 'business__company_id'], 'string', 'max' => 45],
            [['business__company_id'], 'exist', 'skipOnError' => true, 'targetClass' => BusinessCompany::className(), 'targetAttribute' => ['business__company_id' => 'id']],
            [['contact_id'], 'exist', 'skipOnError' => true, 'targetClass' => Contact::className(), 'targetAttribute' => ['contact_id' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'contact_id' => 'Contact ID',
            'business__company_id' => 'Business Company ID',
        ];
    }

    /**
     * Gets query for [[BusinessCompany]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getBusinessCompany()
    {
        return $this->hasOne(BusinessCompany::className(), ['id' => 'business__company_id']);
    }

    /**
     * Gets query for [[Contact]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getContact()
    {
        return $this->hasOne(Contact::className(), ['id' => 'contact_id']);
    }
}
