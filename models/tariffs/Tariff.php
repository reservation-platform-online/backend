<?php

namespace app\models\tariffs;

use Yii;
use yii\db\ActiveQuery;
use yii\db\ActiveRecord;

/**
 * This is the model class for table "{{%tariff}}".
 *
 * @property string $id
 * @property string $name
 * @property string $description
 * @property float $price
 * @property int $enabled
 * @property int $hide
 *
 * @property TariffSubscribe[] $tariffSubscribes
 */
class Tariff extends ActiveRecord
{
    const ENABLED = 1;
    const DISABLED = 0;
    const SHOW = 0;
    const HIDE = 1;
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return '{{%tariff}}';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['name', 'description'], 'required'],
            [['description'], 'string'],
            [['price'], 'number'],
            [['enabled', 'hide'], 'integer'],
            [['id', 'name'], 'string', 'max' => 45],
            [['id'], 'unique'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Name',
            'description' => 'Description',
            'price' => 'Price',
            'enabled' => 'Enabled',
            'hide' => 'Hide',
        ];
    }

    /**
     * Gets query for [[TariffSubscribes]].
     *
     * @return ActiveQuery
     */
    public function getTariffSubscribes()
    {
        return $this->hasMany(TariffSubscribe::className(), ['business__package_id' => 'id']);
    }
}
