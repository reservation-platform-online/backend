<?php

namespace app\models\tariffs;

use Yii;
use yii\db\ActiveQuery;
use yii\db\ActiveRecord;

/**
 * This is the model class for table "{{%tariff__subscribe_history}}".
 *
 * @property string $id
 * @property string $tariff__subscribe_id
 * @property float $cost
 * @property int $status
 * @property string $created_at
 * @property string $updated_at
 *
 * @property TariffSubscribe $tariffSubscribe
 */
class TariffSubscribeHistory extends ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return '{{%tariff__subscribe_history}}';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['tariff__subscribe_id'], 'required'],
            [['cost'], 'number'],
            [['status'], 'integer'],
            [['created_at', 'updated_at'], 'safe'],
            [['id', 'tariff__subscribe_id'], 'string', 'max' => 45],
            [['id'], 'unique'],
            [['tariff__subscribe_id'], 'exist', 'skipOnError' => true, 'targetClass' => TariffSubscribe::className(), 'targetAttribute' => ['tariff__subscribe_id' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'tariff__subscribe_id' => 'Tariff Subscribe ID',
            'cost' => 'Cost',
            'status' => 'Status',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
        ];
    }

    /**
     * Gets query for [[TariffSubscribe]].
     *
     * @return ActiveQuery
     */
    public function getTariffSubscribe()
    {
        return $this->hasOne(TariffSubscribe::className(), ['id' => 'tariff__subscribe_id']);
    }
}
