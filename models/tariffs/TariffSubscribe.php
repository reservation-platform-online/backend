<?php

namespace app\models\tariffs;

use app\models\business\BusinessCompany;
use Yii;
use yii\db\ActiveQuery;
use yii\db\ActiveRecord;

/**
 * This is the model class for table "{{%tariff__subscribe}}".
 *
 * @property string $id
 * @property string $business__package_id
 * @property string $business__company_id
 * @property int $hide
 * @property int $enabled
 * @property int $status
 * @property string $created_at
 * @property string $updated_at
 *
 * @property BusinessCompany $businessCompany
 * @property Tariff $businessPackage
 * @property TariffSubscribeHistory[] $tariffSubscribeHistories
 */
class TariffSubscribe extends ActiveRecord
{
    const ENABLED = 1;
    const DISABLED = 0;
    const SHOW = 0;
    const HIDE = 1;
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return '{{%tariff__subscribe}}';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['business__package_id', 'business__company_id'], 'required'],
            [['hide', 'enabled', 'status'], 'integer'],
            [['created_at', 'updated_at'], 'safe'],
            [['id', 'business__package_id', 'business__company_id'], 'string', 'max' => 45],
            [['id'], 'unique'],
            [['business__company_id'], 'exist', 'skipOnError' => true, 'targetClass' => BusinessCompany::className(), 'targetAttribute' => ['business__company_id' => 'id']],
            [['business__package_id'], 'exist', 'skipOnError' => true, 'targetClass' => Tariff::className(), 'targetAttribute' => ['business__package_id' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'business__package_id' => 'Business Package ID',
            'business__company_id' => 'Business Company ID',
            'hide' => 'Hide',
            'enabled' => 'Enabled',
            'status' => 'Status',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
        ];
    }

    /**
     * Gets query for [[BusinessCompany]].
     *
     * @return ActiveQuery
     */
    public function getBusinessCompany()
    {
        return $this->hasOne(BusinessCompany::className(), ['id' => 'business__company_id']);
    }

    /**
     * Gets query for [[BusinessPackage]].
     *
     * @return ActiveQuery
     */
    public function getBusinessPackage()
    {
        return $this->hasOne(Tariff::className(), ['id' => 'business__package_id']);
    }

    /**
     * Gets query for [[TariffSubscribeHistories]].
     *
     * @return ActiveQuery
     */
    public function getTariffSubscribeHistories()
    {
        return $this->hasMany(TariffSubscribeHistory::className(), ['tariff__subscribe_id' => 'id']);
    }
}
