<?php

namespace app\models\business;

use Yii;
use yii\db\ActiveQuery;
use yii\db\ActiveRecord;

/**
 * This is the model class for table "{{%business__work_time}}".
 *
 * @property string $id
 * @property string $business__worker_id
 * @property string $business__point_worker_service_id
 * @property string $break
 * @property string|null $start_date
 * @property string $start_at
 * @property string|null $end_date
 * @property string $end_at
 * @property int $weekday
 * @property int $enabled
 * @property int $hide
 *
 * @property BusinessPointWorkerService $businessPointWorkerService
 * @property BusinessWorker $businessWorker
 */
class BusinessWorkTime extends ActiveRecord
{

    const ENABLED = 1;
    const DISABLED = 0;
    const SHOW = 0;
    const HIDE = 1;

    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return '{{%business__work_time}}';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['business__worker_id', 'business__point_worker_service_id', 'start_at', 'end_at', 'weekday'], 'required'],
            [['break', 'start_date', 'start_at', 'end_date', 'end_at'], 'safe'],
            [['weekday', 'enabled', 'hide'], 'integer'],
            [['id', 'business__worker_id', 'business__point_worker_service_id'], 'string', 'max' => 45],
            [['id'], 'unique'],
            [['business__point_worker_service_id'], 'exist', 'skipOnError' => true, 'targetClass' => BusinessPointWorkerService::className(), 'targetAttribute' => ['business__point_worker_service_id' => 'id']],
            [['business__worker_id'], 'exist', 'skipOnError' => true, 'targetClass' => BusinessWorker::className(), 'targetAttribute' => ['business__worker_id' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'business__worker_id' => 'Business Worker ID',
            'business__point_worker_service_id' => 'Business Point Worker Service ID',
            'break' => 'Break',
            'start_date' => 'Start Date',
            'start_at' => 'Start At',
            'end_date' => 'End Date',
            'end_at' => 'End At',
            'weekday' => 'Weekday',
            'enabled' => 'Enabled',
            'hide' => 'Hide',
        ];
    }

    /**
     * Gets query for [[BusinessPointWorkerService]].
     *
     * @return ActiveQuery
     */
    public function getBusinessPointWorkerService()
    {
        return $this->hasOne(BusinessPointWorkerService::className(), ['id' => 'business__point_worker_service_id']);
    }

    /**
     * Gets query for [[BusinessWorker]].
     *
     * @return ActiveQuery
     */
    public function getBusinessWorker()
    {
        return $this->hasOne(BusinessWorker::className(), ['id' => 'business__worker_id']);
    }
}
