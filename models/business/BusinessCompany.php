<?php

namespace app\models\business;

use app\models\contacts\ContactBusinessCompany;
use app\models\galleries\GalleryCompany;
use app\models\promoCodes\PromoCode;
use app\models\tariffs\TariffSubscribe;
use Yii;
use yii\db\ActiveQuery;
use yii\db\ActiveRecord;

/**
 * This is the model class for table "{{%business__company}}".
 *
 * @property string $id
 * @property string $name
 * @property string|null $seo_link
 * @property string|null $logo_link
 * @property string|null $description
 * @property string|null $subdomain
 * @property int $type__company_id
 * @property int $enabled
 * @property int $status
 * @property string $created_at
 * @property string $updated_at
 *
 * @property BusinessCompanyUser[] $businessCompanyUsers
 * @property BusinessPoint[] $businessPoints
 * @property BusinessPointWorkerService[] $businessPointWorkerServices
 * @property BusinessService[] $businessServices
 * @property BusinessWorker[] $businessWorkers
 * @property ContactBusinessCompany[] $contactBusinessCompanies
 * @property GalleryCompany[] $galleryCompanies
 * @property PromoCode[] $promoCodes
 * @property TariffSubscribe[] $tariffSubscribes
 */
class BusinessCompany extends ActiveRecord
{
    const TYPE_COMPANY_SINGEL = 1;

    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return '{{%business__company}}';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['name'], 'required'],
            [['logo_link', 'description'], 'string'],
            [['enabled', 'status', 'type__company_id'], 'integer'],
            [['created_at', 'updated_at'], 'safe'],
            [['id', 'name', 'subdomain'], 'string', 'max' => 45],
            [['seo_link'], 'string', 'max' => 255],
            [['id'], 'unique'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Name',
            'seo_link' => 'Seo Link',
            'logo_link' => 'Logo Link',
            'type__company_id' => 'Type company id',
            'description' => 'Description',
            'subdomain' => 'Subdomain',
            'enabled' => 'Enabled',
            'status' => 'Status',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
        ];
    }

    public function beforeSave($insert) {

        if ($this->isNewRecord) {
            $this->id = Yii::$app->db->createCommand('select uuid_v4s()')->queryScalar();
        }

        return parent::beforeSave($insert);

    }

    /**
     * Gets query for [[BusinessCompanyUsers]].
     *
     * @return ActiveQuery
     */
    public function getBusinessCompanyUsers()
    {
        return $this->hasMany(BusinessCompanyUser::className(), ['business__company_id' => 'id']);
    }

    /**
     * Gets query for [[BusinessPoints]].
     *
     * @return ActiveQuery
     */
    public function getBusinessPoints()
    {
        return $this->hasMany(BusinessPoint::className(), ['business__company_id' => 'id']);
    }

    /**
     * Gets query for [[BusinessPointWorkerServices]].
     *
     * @return ActiveQuery
     */
    public function getBusinessPointWorkerServices()
    {
        return $this->hasMany(BusinessPointWorkerService::className(), ['business__company_id' => 'id']);
    }

    /**
     * Gets query for [[BusinessServices]].
     *
     * @return ActiveQuery
     */
    public function getBusinessServices()
    {
        return $this->hasMany(BusinessService::className(), ['business__company_id' => 'id']);
    }

    /**
     * Gets query for [[BusinessWorkers]].
     *
     * @return ActiveQuery
     */
    public function getBusinessWorkers()
    {
        return $this->hasMany(BusinessWorker::className(), ['business__company_id' => 'id']);
    }

    /**
     * Gets query for [[ContactBusinessCompanies]].
     *
     * @return ActiveQuery
     */
    public function getContactBusinessCompanies()
    {
        return $this->hasMany(ContactBusinessCompany::className(), ['business__company_id' => 'id']);
    }

    /**
     * Gets query for [[GalleryCompanies]].
     *
     * @return ActiveQuery
     */
    public function getGalleryCompanies()
    {
        return $this->hasMany(GalleryCompany::className(), ['business__company_id' => 'id']);
    }

    /**
     * Gets query for [[PromoCodes]].
     *
     * @return ActiveQuery
     */
    public function getPromoCodes()
    {
        return $this->hasMany(PromoCode::className(), ['business__company_id' => 'id']);
    }

    /**
     * Gets query for [[TariffSubscribes]].
     *
     * @return ActiveQuery
     */
    public function getTariffSubscribes()
    {
        return $this->hasMany(TariffSubscribe::className(), ['business__company_id' => 'id']);
    }
}
