<?php

namespace app\models\business;

use app\models\promoCodes\PromoCodeBusinessReservation;
use Yii;
use yii\db\ActiveQuery;
use yii\db\ActiveRecord;

/**
 * This is the model class for table "{{%business__reservation}}".
 *
 * @property string $id
 * @property string $business__point_worker_service_id
 * @property string $date
 * @property string $start_at
 * @property string $end_at
 * @property int $weekday
 * @property float $cost
 * @property int $charge
 * @property string|null $meet_link
 * @property string|null $meet_password
 * @property string $interval
 * @property string|null $note
 * @property float $price
 * @property float|null $price_to
 * @property int $payment_is_important
 * @property string|null $timer_time_for_pay
 * @property int $hide
 * @property int $enabled
 * @property int $status 0 - Customer canceled 1 - Created 2 - Confirmed 3 - Finish 4 - Reception canceled 5 - Payment time out 6 - Customer: Employee did not come
 * @property string $created_at
 * @property string $updated_at
 *
 * @property BusinessPointWorkerService $businessPointWorkerService
 * @property BusinessReservationPayment[] $businessReservationPayments
 * @property BusinessReservationUser[] $businessReservationUsers
 * @property PromoCodeBusinessReservation[] $promoCodeBusinessReservations
 */
class BusinessReservation extends ActiveRecord
{

    const ENABLED = 1;
    const DISABLED = 0;
    const SHOW = 0;
    const HIDE = 1;

    const STATUS_USER_CANCELED = 0;
    const STATUS_ENABLED = 1;
    const STATUS_CONFIRMED = 2;
    const STATUS_FINISHED = 3;
    const STATUS_RECEPTION_CANCELED = 4;
    const STATUS_PAYMENT_TIME_OUT = 5;
    const STATUS_USER_WORKER_DID_NOT_COME = 6;

    const ACTIVE_STATUS_LIST = [self::STATUS_ENABLED, self::STATUS_CONFIRMED];
    const HISTORY_STATUS_LIST = [self::STATUS_USER_CANCELED, self::STATUS_FINISHED, self::STATUS_RECEPTION_CANCELED, self::STATUS_PAYMENT_TIME_OUT, self::STATUS_USER_WORKER_DID_NOT_COME];

    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return '{{%business__reservation}}';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['business__point_worker_service_id', 'date', 'start_at', 'end_at'], 'required'],
            [['date', 'start_at', 'end_at', 'interval', 'timer_time_for_pay', 'created_at', 'updated_at'], 'safe'],
            [['weekday', 'charge', 'payment_is_important', 'hide', 'enabled', 'status'], 'integer'],
            [['cost', 'price', 'price_to'], 'number'],
            [['meet_link', 'note'], 'string'],
            [['id', 'business__point_worker_service_id'], 'string', 'max' => 45],
            [['meet_password'], 'string', 'max' => 10],
            [['id'], 'unique'],
            [['business__point_worker_service_id'], 'exist', 'skipOnError' => true, 'targetClass' => BusinessPointWorkerService::className(), 'targetAttribute' => ['business__point_worker_service_id' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'business__point_worker_service_id' => 'Business Point Worker Service ID',
            'date' => 'Date',
            'start_at' => 'Start At',
            'end_at' => 'End At',
            'weekday' => 'Weekday',
            'cost' => 'Cost',
            'charge' => 'Charge',
            'meet_link' => 'Meet Link',
            'meet_password' => 'Meet Password',
            'interval' => 'Interval',
            'note' => 'Note',
            'price' => 'Price',
            'price_to' => 'Price To',
            'payment_is_important' => 'Payment is important',
            'timer_time_for_pay' => 'Timer Time For Pay',
            'hide' => 'Hide',
            'enabled' => 'Enabled',
            'status' => '0 - Customer canceled
1 - Created
2 - Confirmed
3 - Finish
4 - Reception canceled
5 - Payment time out
6 - Customer: Employee did not come',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
        ];
    }

    public function beforeSave($insert) {

        if ($this->isNewRecord) {
            $this->id = Yii::$app->db->createCommand('select uuid_v4s()')->queryScalar();
        }

        return parent::beforeSave($insert);

    }

    /**
     * Gets query for [[BusinessPointWorkerService]].
     *
     * @return ActiveQuery
     */
    public function getBusinessPointWorkerService()
    {
        return $this->hasOne(BusinessPointWorkerService::className(), ['id' => 'business__point_worker_service_id']);
    }

    /**
     * Gets query for [[BusinessReservationPayments]].
     *
     * @return ActiveQuery
     */
    public function getBusinessReservationPayments()
    {
        return $this->hasMany(BusinessReservationPayment::className(), ['business__reservation_id' => 'id']);
    }

    /**
     * Gets query for [[BusinessReservationUsers]].
     *
     * @return ActiveQuery
     */
    public function getBusinessReservationUsers()
    {
        return $this->hasMany(BusinessReservationUser::className(), ['business__reservation_id' => 'id']);
    }

    /**
     * Gets query for [[PromoCodeBusinessReservations]].
     *
     * @return ActiveQuery
     */
    public function getPromoCodeBusinessReservations()
    {
        return $this->hasMany(PromoCodeBusinessReservation::className(), ['business__reservation_id' => 'id']);
    }
}
