<?php

namespace app\models\business;

use app\models\additional\AdditionalWeekend;
use app\models\User;
use Yii;
use yii\db\ActiveQuery;
use yii\db\ActiveRecord;

/**
 * This is the model class for table "{{%business__worker}}".
 *
 * @property string $id
 * @property string $business__company_id
 * @property string|null $user_id
 * @property string $first_name
 * @property string $last_name
 * @property string|null $photo
 * @property string|null $position
 * @property int $number_of_comments
 * @property float $score
 * @property int $enabled
 * @property int $hide
 * @property int $status 1 - Wait for confirm 2 - Confirmed 3 - Deleting 4 - Company Delete
 * @property string $created_at
 * @property string $updated_at
 *
 * @property AdditionalWeekend[] $additionalWeekends
 * @property BusinessPointWorkerService[] $businessPointWorkerServices
 * @property BusinessCounterPerSlot[] $businessCounterPerSlots
 * @property BusinessWorkTime[] $businessWorkTimes
 * @property User $user
 * @property BusinessCompany $businessCompany
 */
class BusinessWorker extends ActiveRecord
{

    const ENABLED = 1;
    const DISABLED = 0;
    const SHOW = 0;
    const HIDE = 1;
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return '{{%business__worker}}';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['business__company_id', 'first_name', 'last_name'], 'required'],
            [['photo'], 'string'],
            [['number_of_comments', 'enabled', 'hide', 'status'], 'integer'],
            [['score'], 'number'],
            [['created_at', 'updated_at'], 'safe'],
            [['id', 'business__company_id', 'user_id', 'first_name', 'last_name', 'position'], 'string', 'max' => 45],
            [['id'], 'unique'],
            [['user_id'], 'exist', 'skipOnError' => true, 'targetClass' => User::className(), 'targetAttribute' => ['user_id' => 'id']],
            [['business__company_id'], 'exist', 'skipOnError' => true, 'targetClass' => BusinessCompany::className(), 'targetAttribute' => ['business__company_id' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'business__company_id' => 'Business Company ID',
            'user_id' => 'User ID',
            'first_name' => 'First Name',
            'last_name' => 'Last Name',
            'photo' => 'Photo',
            'position' => 'Position',
            'number_of_comments' => 'Number Of Comments',
            'score' => 'Score',
            'enabled' => 'Enabled',
            'hide' => 'Hide',
            'status' => '1 - Wait for confirm
2 - Confirmed
3 - Deleting
4 - Company Delete',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
        ];
    }

    public function beforeSave($insert) {

        if ($this->isNewRecord) {
            $this->id = Yii::$app->db->createCommand('select uuid_v4s()')->queryScalar();
        }

        return parent::beforeSave($insert);

    }

    /**
     * Gets query for [[BusinessCounterPerSlots]].
     *
     * @return ActiveQuery
     */
    public function getBusinessCounterPerSlots()
    {
        return $this->hasMany(BusinessCounterPerSlot::className(), ['business__worker_id' => 'id']);
    }

    /**
     * Gets query for [[AdditionalWeekends]].
     *
     * @return ActiveQuery
     */
    public function getAdditionalWeekends()
    {
        return $this->hasMany(AdditionalWeekend::className(), ['business__worker_id' => 'id']);
    }

    /**
     * Gets query for [[BusinessPointWorkerServices]].
     *
     * @return ActiveQuery
     */
    public function getBusinessPointWorkerServices()
    {
        return $this->hasMany(BusinessPointWorkerService::className(), ['business__worker_id' => 'id']);
    }

    /**
     * Gets query for [[BusinessWorkTimes]].
     *
     * @return ActiveQuery
     */
    public function getBusinessWorkTimes()
    {
        return $this->hasMany(BusinessWorkTime::className(), ['business__worker_id' => 'id']);
    }

    /**
     * Gets query for [[User]].
     *
     * @return ActiveQuery
     */
    public function getUser()
    {
        return $this->hasOne(User::className(), ['id' => 'user_id']);
    }

    /**
     * Gets query for [[BusinessCompany]].
     *
     * @return ActiveQuery
     */
    public function getBusinessCompany()
    {
        return $this->hasOne(BusinessCompany::className(), ['id' => 'business__company_id']);
    }
}
