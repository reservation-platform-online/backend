<?php

namespace app\models\business;

use app\models\additional\AdditionalAddressBusinessReservationUser;
use app\models\User;
use yii\db\ActiveQuery;
use yii\db\ActiveRecord;

/**
 * This is the model class for table "{{%business__reservation__user}}".
 *
 * @property string $id
 * @property string $business__reservation_id
 * @property string $user_id
 * @property string $email
 * @property string|null $note
 * @property int $is_initiator
 * @property int $number_of_comments
 * @property string|null $meet_token
 * @property int $enabled
 * @property int $hide
 * @property string $created_at
 * @property string $updated_at
 * @property int $status 0 - Not present 1 - Wait 2 - Present
 *
 * @property AdditionalAddressBusinessReservationUser[] $additionalAddressBusinessReservationUsers
 * @property BusinessComment[] $businessComments
 * @property BusinessReservation $businessReservation
 * @property User $user
 */
class BusinessReservationUser extends ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return '{{%business__reservation__user}}';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['business__reservation_id'], 'required'],
            [['note', 'meet_token'], 'string'],
            [['is_initiator', 'enabled', 'hide', 'status', 'number_of_comments'], 'integer'],
            [['created_at', 'updated_at'], 'safe'],
            [['id', 'business__reservation_id', 'user_id'], 'string', 'max' => 45],
            [['id'], 'unique'],
            [['business__reservation_id'], 'exist', 'skipOnError' => true, 'targetClass' => BusinessReservation::className(), 'targetAttribute' => ['business__reservation_id' => 'id']],
            [['user_id'], 'exist', 'skipOnError' => true, 'targetClass' => User::className(), 'targetAttribute' => ['user_id' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'business__reservation_id' => 'Business Reservation ID',
            'user_id' => 'User ID',
            'note' => 'Note',
            'is_initiator' => 'Is Initiator',
            'meet_token' => 'Meet Token',
            'enabled' => 'Enabled',
            'hide' => 'Hide',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
            'status' => '0 - Not present
1 - Wait
2 - Present',
        ];
    }

    /**
     * Gets query for [[AdditionalAddressBusinessReservationUsers]].
     *
     * @return ActiveQuery
     */
    public function getAdditionalAddressBusinessReservationUsers()
    {
        return $this->hasMany(AdditionalAddressBusinessReservationUser::className(), ['business__reservation__user_id' => 'id']);
    }

    /**
     * Gets query for [[BusinessComments]].
     *
     * @return ActiveQuery
     */
    public function getBusinessComments()
    {
        return $this->hasMany(BusinessComment::className(), ['business__reservation__user_id' => 'id']);
    }

    /**
     * Gets query for [[BusinessReservation]].
     *
     * @return ActiveQuery
     */
    public function getBusinessReservation()
    {
        return $this->hasOne(BusinessReservation::className(), ['id' => 'business__reservation_id']);
    }

    /**
     * Gets query for [[User]].
     *
     * @return ActiveQuery
     */
    public function getUser()
    {
        return $this->hasOne(User::className(), ['id' => 'user_id']);
    }
}
