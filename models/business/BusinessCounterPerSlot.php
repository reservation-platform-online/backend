<?php

namespace app\models\business;

use Yii;
use yii\db\ActiveQuery;
use yii\db\ActiveRecord;

/**
 * This is the model class for table "{{%business__counter_per_slot}}".
 *
 * @property string $id
 * @property string $business__worker_id
 * @property string $start_at
 * @property string $end_at
 * @property string $date
 * @property int $weekday
 * @property int $count
 *
 * @property BusinessWorker $businessWorker
 */
class BusinessCounterPerSlot extends ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return '{{%business__counter_per_slot}}';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['business__point_worker_service_id', 'start_at', 'end_at', 'date'], 'required'],
            [['start_at', 'end_at', 'date'], 'safe'],
            [['weekday', 'count'], 'integer'],
            [['id', 'business__worker_id'], 'string', 'max' => 45],
            [['id'], 'unique'],
            [['business__worker_id'], 'exist', 'skipOnError' => true, 'targetClass' => BusinessWorker::className(), 'targetAttribute' => ['business__worker_id' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'business__worker_id' => 'Business Worker',
            'start_at' => 'Start At',
            'end_at' => 'End At',
            'date' => 'Date',
            'weekday' => 'Weekday',
            'count' => 'Count',
        ];
    }

    /**
     * Gets query for [[BusinessPointWorkerService]].
     *
     * @return ActiveQuery
     */
    public function getBusinessPointWorkerService()
    {
        return $this->hasOne(BusinessWorker::className(), ['id' => 'business__worker_id']);
    }
}
