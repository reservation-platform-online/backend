<?php

namespace app\models\business;

use app\models\types\TypeService;
use Yii;
use yii\db\ActiveQuery;
use yii\db\ActiveRecord;

/**
 * This is the model class for table "{{%business__point_worker_service__type__service}}".
 *
 * @property int $type__service_id
 * @property string $business__point_worker_service_id
 *
 * @property BusinessPointWorkerService $businessPointWorkerService
 * @property TypeService $typeService
 */
class BusinessPointWorkerServiceTypeService extends ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return '{{%business__point_worker_service__type__service}}';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['type__service_id', 'business__point_worker_service_id'], 'required'],
            [['type__service_id'], 'integer'],
            [['business__point_worker_service_id'], 'string', 'max' => 45],
            [['business__point_worker_service_id'], 'exist', 'skipOnError' => true, 'targetClass' => BusinessPointWorkerService::className(), 'targetAttribute' => ['business__point_worker_service_id' => 'id']],
            [['type__service_id'], 'exist', 'skipOnError' => true, 'targetClass' => TypeService::className(), 'targetAttribute' => ['type__service_id' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'type__service_id' => 'Type Service ID',
            'business__point_worker_service_id' => 'Business Point Worker Service ID',
        ];
    }

    /**
     * Gets query for [[BusinessPointWorkerService]].
     *
     * @return ActiveQuery
     */
    public function getBusinessPointWorkerService()
    {
        return $this->hasOne(BusinessPointWorkerService::className(), ['id' => 'business__point_worker_service_id']);
    }

    /**
     * Gets query for [[TypeService]].
     *
     * @return ActiveQuery
     */
    public function getTypeService()
    {
        return $this->hasOne(TypeService::className(), ['id' => 'type__service_id']);
    }
}
