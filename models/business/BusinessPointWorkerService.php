<?php

namespace app\models\business;

use app\models\additional\AdditionalLike;
use Yii;
use yii\db\ActiveQuery;
use yii\db\ActiveRecord;

/**
 * This is the model class for table "{{%business__point_worker_service}}".
 *
 * @property string $id
 * @property string $business__company_id
 * @property string|null $business__point_id NULL when service is online
 * @property string $business__worker_id
 * @property string $business__service_id
 * @property int $limit_reservation_per_slot
 * @property int $number_of_comments
 * @property float $score
 * @property int $enabled
 * @property int $hide
 * @property int $status
 * @property string $created_at
 * @property string $updated_at
 *
 * @property AdditionalLike[] $additionalLikes
 * @property BusinessComment[] $businessComments
 * @property BusinessCompany $businessCompany
 * @property BusinessWorker $businessWorker
 * @property BusinessPoint $businessPoint
 * @property BusinessService $businessService
 * @property BusinessPointWorkerServiceTypeService[] $businessPointWorkerServiceTypeServices
 * @property BusinessReservation[] $businessReservations
 * @property BusinessWorkTime[] $businessWorkTimes
 */
class BusinessPointWorkerService extends ActiveRecord
{
    const ENABLED = 1;
    const DISABLED = 0;
    const SHOW = 0;
    const HIDE = 1;

    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return '{{%business__point_worker_service}}';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['business__company_id', 'business__worker_id', 'business__service_id'], 'required'],
            [['number_of_comments', 'limit_reservation_per_slot', 'enabled', 'hide', 'status'], 'integer'],
            [['created_at', 'updated_at'], 'safe'],
            [['score'], 'number'],
            [['id', 'business__company_id', 'business__point_id', 'business__worker_id', 'business__service_id'], 'string', 'max' => 45],
            [['id'], 'unique'],
            [['business__company_id'], 'exist', 'skipOnError' => true, 'targetClass' => BusinessCompany::className(), 'targetAttribute' => ['business__company_id' => 'id']],
            [['business__worker_id'], 'exist', 'skipOnError' => true, 'targetClass' => BusinessWorker::className(), 'targetAttribute' => ['business__worker_id' => 'id']],
            [['business__point_id'], 'exist', 'skipOnError' => true, 'targetClass' => BusinessPoint::className(), 'targetAttribute' => ['business__point_id' => 'id']],
            [['business__service_id'], 'exist', 'skipOnError' => true, 'targetClass' => BusinessService::className(), 'targetAttribute' => ['business__service_id' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'business__company_id' => 'Business Company ID',
            'business__point_id' => 'NULL when service is online',
            'business__worker_id' => 'Business Worker ID',
            'business__service_id' => 'Business Service ID',
            'limit_reservation_per_slot' => 'Limit Reservation Per Slot',
            'enabled' => 'Enabled',
            'number_of_comments' => 'Number Of Comments',
            'score' => 'Score',
            'hide' => 'Hide',
            'status' => 'Status',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
        ];
    }

    public function beforeSave($insert) {

        if ($this->isNewRecord) {
            $this->id = Yii::$app->db->createCommand('select uuid_v4s()')->queryScalar();
        }

        return parent::beforeSave($insert);

    }

    /**
     * Gets query for [[AdditionalLikes]].
     *
     * @return ActiveQuery
     */
    public function getAdditionalLikes()
    {
        return $this->hasMany(AdditionalLike::className(), ['business__point_worker_service_id' => 'id']);
    }

    /**
     * Gets query for [[BusinessComments]].
     *
     * @return ActiveQuery
     */
    public function getBusinessComments()
    {
        return $this->hasMany(BusinessComment::className(), ['business__point_worker_service_id' => 'id']);
    }

    /**
     * Gets query for [[BusinessCompany]].
     *
     * @return ActiveQuery
     */
    public function getBusinessCompany()
    {
        return $this->hasOne(BusinessCompany::className(), ['id' => 'business__company_id']);
    }

    /**
     * Gets query for [[BusinessWorker]].
     *
     * @return ActiveQuery
     */
    public function getBusinessWorker()
    {
        return $this->hasOne(BusinessWorker::className(), ['id' => 'business__worker_id']);
    }

    /**
     * Gets query for [[BusinessPoint]].
     *
     * @return ActiveQuery
     */
    public function getBusinessPoint()
    {
        return $this->hasOne(BusinessPoint::className(), ['id' => 'business__point_id']);
    }

    /**
     * Gets query for [[BusinessService]].
     *
     * @return ActiveQuery
     */
    public function getBusinessService()
    {
        return $this->hasOne(BusinessService::className(), ['id' => 'business__service_id']);
    }

    /**
     * Gets query for [[BusinessPointWorkerServiceTypeServices]].
     *
     * @return ActiveQuery
     */
    public function getBusinessPointWorkerServiceTypeServices()
    {
        return $this->hasMany(BusinessPointWorkerServiceTypeService::className(), ['business__point_worker_service_id' => 'id']);
    }

    /**
     * Gets query for [[BusinessReservations]].
     *
     * @return ActiveQuery
     */
    public function getBusinessReservations()
    {
        return $this->hasMany(BusinessReservation::className(), ['business__point_worker_service_id' => 'id']);
    }

    /**
     * Gets query for [[BusinessWorkTimes]].
     *
     * @return ActiveQuery
     */
    public function getBusinessWorkTimes()
    {
        return $this->hasMany(BusinessWorkTime::className(), ['business__point_worker_service_id' => 'id']);
    }
}
