<?php

namespace app\models\business;

use Yii;
use yii\db\ActiveQuery;
use yii\db\ActiveRecord;

/**
 * This is the model class for table "{{%business__comment}}".
 *
 * @property string $id
 * @property string $business__point_worker_service_id
 * @property int $is_anonymous
 * @property string $comment
 * @property int $score
 * @property int $enabled
 * @property int $hide
 * @property string $created_at
 * @property string $updated_at
 * @property string $business__reservation__user_id
 *
 * @property BusinessPointWorkerService $businessPointWorkerService
 * @property BusinessReservationUser $businessReservationUser
 */
class BusinessComment extends ActiveRecord
{

    const ENABLED = 1;
    const DISABLED = 0;
    const SHOW = 0;
    const HIDE = 1;

    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return '{{%business__comment}}';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['business__point_worker_service_id', 'comment', 'business__reservation__user_id'], 'required'],
            [['is_anonymous', 'score', 'enabled', 'hide'], 'integer'],
            [['comment'], 'string'],
            [['created_at', 'updated_at'], 'safe'],
            [['id', 'business__point_worker_service_id', 'business__reservation__user_id'], 'string', 'max' => 45],
            [['id'], 'unique'],
            [['business__point_worker_service_id'], 'exist', 'skipOnError' => true, 'targetClass' => BusinessPointWorkerService::className(), 'targetAttribute' => ['business__point_worker_service_id' => 'id']],
            [['business__reservation__user_id'], 'exist', 'skipOnError' => true, 'targetClass' => BusinessReservationUser::className(), 'targetAttribute' => ['business__reservation__user_id' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'business__point_worker_service_id' => 'Business Point Worker Service ID',
            'is_anonymous' => 'Is Anonymous',
            'comment' => 'Comment',
            'score' => 'Score',
            'enabled' => 'Enabled',
            'hide' => 'Hide',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
            'business__reservation__user_id' => 'Business Reservation User ID',
        ];
    }

    public function beforeSave($insert) {

        if ($this->isNewRecord) {
            $this->id = Yii::$app->db->createCommand('select uuid_v4s()')->queryScalar();
        }

        return parent::beforeSave($insert);

    }

    /**
     * Gets query for [[BusinessPointWorkerService]].
     *
     * @return ActiveQuery
     */
    public function getBusinessPointWorkerService()
    {
        return $this->hasOne(BusinessPointWorkerService::className(), ['id' => 'business__point_worker_service_id']);
    }

    /**
     * Gets query for [[BusinessReservationUser]].
     *
     * @return ActiveQuery
     */
    public function getBusinessReservationUser()
    {
        return $this->hasOne(BusinessReservationUser::className(), ['id' => 'business__reservation__user_id']);
    }
}
