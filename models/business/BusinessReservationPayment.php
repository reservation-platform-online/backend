<?php

namespace app\models\business;

use app\models\types\TypePayment;
use Yii;
use yii\db\ActiveQuery;
use yii\db\ActiveRecord;

/**
 * This is the model class for table "{{%business__reservation__payment}}".
 *
 * @property string $id
 * @property string $business__reservation_id
 * @property int $type__payment_id
 * @property float $debit Income
 * @property float $credit Outgoing
 * @property string $created_at
 * @property string $updated_at
 *
 * @property BusinessReservation $businessReservation
 * @property TypePayment $typePayment
 */
class BusinessReservationPayment extends ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return '{{%business__reservation__payment}}';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['business__reservation_id', 'type__payment_id'], 'required'],
            [['type__payment_id'], 'integer'],
            [['debit', 'credit'], 'number'],
            [['created_at', 'updated_at'], 'safe'],
            [['id', 'business__reservation_id'], 'string', 'max' => 45],
            [['id'], 'unique'],
            [['business__reservation_id'], 'exist', 'skipOnError' => true, 'targetClass' => BusinessReservation::className(), 'targetAttribute' => ['business__reservation_id' => 'id']],
            [['type__payment_id'], 'exist', 'skipOnError' => true, 'targetClass' => TypePayment::className(), 'targetAttribute' => ['type__payment_id' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'business__reservation_id' => 'Business Reservation ID',
            'type__payment_id' => 'Type Payment ID',
            'debit' => 'Income',
            'credit' => 'Outgoing',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
        ];
    }

    /**
     * Gets query for [[BusinessReservation]].
     *
     * @return ActiveQuery
     */
    public function getBusinessReservation()
    {
        return $this->hasOne(BusinessReservation::className(), ['id' => 'business__reservation_id']);
    }

    /**
     * Gets query for [[TypePayment]].
     *
     * @return ActiveQuery
     */
    public function getTypePayment()
    {
        return $this->hasOne(TypePayment::className(), ['id' => 'type__payment_id']);
    }
}
