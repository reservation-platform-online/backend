<?php

namespace app\models\business;

use app\models\localities\Locality;
use Yii;
use yii\db\ActiveQuery;
use yii\db\ActiveRecord;

/**
 * This is the model class for table "{{%business__point}}".
 *
 * @property string $id
 * @property string $business__company_id
 * @property int $locality_id
 * @property string $address_line
 * @property string|null $address_line2
 * @property string $postal_index
 * @property float|null $latitude
 * @property float|null $longitude
 * @property int|null $zoom
 * @property int $enabled
 * @property int $hide
 * @property int $status
 * @property string $created_at
 * @property string $updated_at
 *
 * @property Locality $locality
 * @property BusinessCompany $businessCompany
 * @property BusinessPointWorkerService[] $businessPointWorkerServices
 */
class BusinessPoint extends ActiveRecord
{

    const ENABLED = 1;
    const DISABLED = 0;
    const SHOW = 0;
    const HIDE = 1;

    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return '{{%business__point}}';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['business__company_id', 'locality_id', 'address_line', 'postal_index'], 'required'],
            [['locality_id', 'zoom', 'enabled', 'hide', 'status'], 'integer'],
            [['address_line', 'address_line2'], 'string'],
            [['latitude', 'longitude'], 'number'],
            [['created_at', 'updated_at'], 'safe'],
            [['id', 'business__company_id', 'postal_index'], 'string', 'max' => 45],
            [['id'], 'unique'],
            [['locality_id'], 'exist', 'skipOnError' => true, 'targetClass' => Locality::className(), 'targetAttribute' => ['locality_id' => 'id']],
            [['business__company_id'], 'exist', 'skipOnError' => true, 'targetClass' => BusinessCompany::className(), 'targetAttribute' => ['business__company_id' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'business__company_id' => 'Business Company ID',
            'locality_id' => 'Locality ID',
            'address_line' => 'Address Line',
            'address_line2' => 'Address Line2',
            'postal_index' => 'Postal Index',
            'latitude' => 'Latitude',
            'longitude' => 'Longitude',
            'zoom' => 'Zoom',
            'enabled' => 'Enabled',
            'hide' => 'Hide',
            'status' => 'Status',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
        ];
    }

    /**
     * Gets query for [[Locality]].
     *
     * @return ActiveQuery
     */
    public function getLocality()
    {
        return $this->hasOne(Locality::className(), ['id' => 'locality_id']);
    }

    /**
     * Gets query for [[BusinessCompany]].
     *
     * @return ActiveQuery
     */
    public function getBusinessCompany()
    {
        return $this->hasOne(BusinessCompany::className(), ['id' => 'business__company_id']);
    }

    /**
     * Gets query for [[BusinessPointWorkerServices]].
     *
     * @return ActiveQuery
     */
    public function getBusinessPointWorkerServices()
    {
        return $this->hasMany(BusinessPointWorkerService::className(), ['business__point_id' => 'id']);
    }
}
