<?php

namespace app\models\business;

use app\models\categories\Category;
use app\models\galleries\GalleryService;
use app\models\promoCodes\PromoCodeBusinessService;
use Yii;
use yii\db\ActiveQuery;
use yii\db\ActiveRecord;

/**
 * This is the model class for table "{{%business__service}}".
 *
 * @property string $id
 * @property string $business__company_id
 * @property int $category_id
 * @property string $name
 * @property string $description
 * @property string $interval
 * @property float $price
 * @property float|null $price_to
 * @property float|null $discount
 * @property int|null $discount_is_percent
 * @property string $earliest_reservation_time
 * @property string|null $earliest_reservation_date
 * @property string|null $last_reservation_date
 * @property int $payment_is_important
 * @property string|null $timer_time_for_pay
 * @property int $hide
 * @property int $enabled
 * @property int $status
 * @property string $created_at
 * @property string $updated_at
 *
 * @property BusinessPointWorkerService[] $businessPointWorkerServices
 * @property Category $category
 * @property BusinessCompany $businessCompany
 * @property GalleryService[] $galleryServices
 * @property PromoCodeBusinessService[] $promoCodeBusinessServices
 */
class BusinessService extends ActiveRecord
{

    const ENABLED = 1;
    const DISABLED = 0;
    const SHOW = 0;
    const HIDE = 1;

    const PAYMENT_IS_IMPORTANT = 1;
    const PAYMENT_IS_NOT_IMPORTANT = 0;

    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return '{{%business__service}}';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['business__company_id', 'category_id', 'name', 'description', 'price'], 'required'],
            [['category_id', 'discount_is_percent', 'payment_is_important', 'hide', 'enabled', 'status'], 'integer'],
            [['name'], 'string'],
            [['description', 'interval', 'earliest_reservation_time', 'earliest_reservation_date', 'last_reservation_date', 'timer_time_for_pay', 'created_at', 'updated_at'], 'safe'],
            [['price', 'price_to', 'discount'], 'number'],
            [['id', 'business__company_id'], 'string', 'max' => 45],
            [['id'], 'unique'],
            [['category_id'], 'exist', 'skipOnError' => true, 'targetClass' => Category::className(), 'targetAttribute' => ['category_id' => 'id']],
            [['business__company_id'], 'exist', 'skipOnError' => true, 'targetClass' => BusinessCompany::className(), 'targetAttribute' => ['business__company_id' => 'id']],
            ['payment_is_important', 'default', 'value' => 1],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'business__company_id' => 'Business Company ID',
            'category_id' => 'Category ID',
            'name' => 'Name',
            'description' => 'Description',
            'interval' => 'Interval',
            'price' => 'Price',
            'price_to' => 'Price To',
            'discount' => 'Discount',
            'discount_is_percent' => 'Discount Is Percent',
            'earliest_reservation_time' => 'Earliest reservation time',
            'earliest_reservation_date' => 'Earliest reservation date',
            'last_reservation_date' => 'Last reservation date',
            'payment_is_important' => 'Payment is important',
            'timer_time_for_pay' => 'Timer Time For Pay',
            'hide' => 'Hide',
            'enabled' => 'Enabled',
            'status' => 'Status',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
        ];
    }

    public function beforeSave($insert) {

        if ($this->isNewRecord) {
            $this->id = Yii::$app->db->createCommand('select uuid_v4s()')->queryScalar();
        }

        return parent::beforeSave($insert);

    }

    /**
     * Gets query for [[BusinessPointWorkerServices]].
     *
     * @return ActiveQuery
     */
    public function getBusinessPointWorkerServices()
    {
        return $this->hasMany(BusinessPointWorkerService::className(), ['business__service_id' => 'id']);
    }

    /**
     * Gets query for [[Category]].
     *
     * @return ActiveQuery
     */
    public function getCategory()
    {
        return $this->hasOne(Category::className(), ['id' => 'category_id']);
    }

    /**
     * Gets query for [[BusinessCompany]].
     *
     * @return ActiveQuery
     */
    public function getBusinessCompany()
    {
        return $this->hasOne(BusinessCompany::className(), ['id' => 'business__company_id']);
    }

    /**
     * Gets query for [[GalleryServices]].
     *
     * @return ActiveQuery
     */
    public function getGalleryServices()
    {
        return $this->hasMany(GalleryService::className(), ['business__service_id' => 'id']);
    }

    /**
     * Gets query for [[PromoCodeBusinessServices]].
     *
     * @return ActiveQuery
     */
    public function getPromoCodeBusinessServices()
    {
        return $this->hasMany(PromoCodeBusinessService::className(), ['business__service_id' => 'id']);
    }
}
