<?php

namespace app\models\business;

use app\models\User;
use Yii;
use yii\db\ActiveQuery;
use yii\db\ActiveRecord;

/**
 * This is the model class for table "{{%business__company__user}}".
 *
 * @property string $id
 * @property string $business__company_id
 * @property string $user_id
 * @property int $role 1 - Owner 2 - Co-owner 3 - Reception
 * @property int $enabled
 * @property string $created_at
 * @property string $updated_at
 *
 * @property BusinessCompany $businessCompany
 * @property User $user
 */
class BusinessCompanyUser extends ActiveRecord
{
    const ENABLED = 1;
    const DISABLED = 0;

    const ROLE_OWNER = 1;
    const ROLE_CO_OWNER = 2;
    const ROLE_RECEPTION = 3;

    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return '{{%business__company__user}}';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['business__company_id', 'user_id'], 'required'],
            [['role', 'enabled'], 'integer'],
            [['created_at', 'updated_at'], 'safe'],
            [['id', 'business__company_id', 'user_id'], 'string', 'max' => 45],
            [['id'], 'unique'],
            [['business__company_id'], 'exist', 'skipOnError' => true, 'targetClass' => BusinessCompany::className(), 'targetAttribute' => ['business__company_id' => 'id']],
            [['user_id'], 'exist', 'skipOnError' => true, 'targetClass' => User::className(), 'targetAttribute' => ['user_id' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'business__company_id' => 'Business Company ID',
            'user_id' => 'User ID',
            'role' => '1 - Owner
2 - Co-owner
3 - Reception',
            'enabled' => 'Enabled',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
        ];
    }

    /**
     * Gets query for [[BusinessCompany]].
     *
     * @return ActiveQuery
     */
    public function getBusinessCompany()
    {
        return $this->hasOne(BusinessCompany::className(), ['id' => 'business__company_id']);
    }

    /**
     * Gets query for [[User]].
     *
     * @return ActiveQuery
     */
    public function getUser()
    {
        return $this->hasOne(User::className(), ['id' => 'user_id']);
    }
}
