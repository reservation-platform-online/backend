<?php

namespace app\models\authorizations;

use app\models\User;
use Yii;
use yii\db\ActiveQuery;
use yii\db\ActiveRecord;

/**
 * This is the model class for table "{{%authorization__external}}".
 *
 * @property string $id
 * @property string $authorization__platform_id
 * @property string $user_id
 * @property string $external_user_id
 * @property string $code
 * @property string|null $access_token
 * @property string|null $token_type
 * @property string|null $refresh_token
 * @property int|null $expires_in Minutes
 * @property string|null $scope
 * @property int $enabled
 * @property string $created_at
 * @property string $updated_at
 *
 * @property AuthorizationPlatform $authorizationPlatform
 * @property User $user
 */
class AuthorizationExternal extends ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return '{{%authorization__external}}';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['authorization__platform_id', 'user_id', 'code'], 'required'],
            [['code', 'access_token', 'refresh_token', 'scope', 'external_user_id'], 'string'],
            [['expires_in', 'enabled'], 'integer'],
            [['created_at', 'updated_at'], 'safe'],
            [['id', 'authorization__platform_id', 'user_id'], 'string', 'max' => 45],
            [['token_type'], 'string', 'max' => 25],
            [['id'], 'unique'],
            [['authorization__platform_id'], 'exist', 'skipOnError' => true, 'targetClass' => AuthorizationPlatform::className(), 'targetAttribute' => ['authorization__platform_id' => 'id']],
            [['user_id'], 'exist', 'skipOnError' => true, 'targetClass' => User::className(), 'targetAttribute' => ['user_id' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'authorization__platform_id' => 'Authorization Platform ID',
            'user_id' => 'User ID',
            'code' => 'Code',
            'access_token' => 'Access Token',
            'token_type' => 'Token Type',
            'refresh_token' => 'Refresh Token',
            'expires_in' => 'Minutes',
            'external_user_id' => 'External user id',
            'scope' => 'Scope',
            'enabled' => 'Enabled',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
        ];
    }

    /**
     * Gets query for [[AuthorizationPlatform]].
     *
     * @return ActiveQuery
     */
    public function getAuthorizationPlatform()
    {
        return $this->hasOne(AuthorizationPlatform::className(), ['id' => 'authorization__platform_id']);
    }

    /**
     * Gets query for [[User]].
     *
     * @return ActiveQuery
     */
    public function getUser()
    {
        return $this->hasOne(User::className(), ['id' => 'user_id']);
    }
}
