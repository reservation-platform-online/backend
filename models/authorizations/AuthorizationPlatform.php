<?php

namespace app\models\authorizations;

use Yii;
use yii\db\ActiveQuery;
use yii\db\ActiveRecord;

/**
 * This is the model class for table "{{%authorization__platform}}".
 *
 * @property string $id
 * @property string $name
 * @property string $client_id
 * @property string $client_secret
 * @property string|null $app_key
 * @property string|null $public_key
 * @property string|null $redirect_url
 * @property int $enabled
 * @property int $is_dev
 *
 * @property AuthorizationExternal[] $authorizationExternals
 */
class AuthorizationPlatform extends ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return '{{%authorization__platform}}';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['name', 'client_id', 'client_secret'], 'required'],
            [['client_id', 'client_secret', 'app_key', 'public_key', 'redirect_url'], 'string'],
            [['enabled', 'is_dev'], 'integer'],
            [['id', 'name'], 'string', 'max' => 45],
            [['id'], 'unique'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Name',
            'client_id' => 'Client ID',
            'client_secret' => 'Client Secret',
            'app_key' => 'App Key',
            'public_key' => 'Public Key',
            'redirect_url' => 'Redirect Url',
            'enabled' => 'Enabled',
            'is_dev' => 'Is dev',
        ];
    }

    /**
     * Gets query for [[AuthorizationExternals]].
     *
     * @return ActiveQuery
     */
    public function getAuthorizationExternals()
    {
        return $this->hasMany(AuthorizationExternal::className(), ['authorization__platform_id' => 'id']);
    }
}
