<?php

namespace app\models\authorizations;

use Yii;
use yii\db\ActiveQuery;
use yii\db\ActiveRecord;

/**
 * This is the model class for table "{{%authorization__device}}".
 *
 * @property string $id
 * @property string $device_uuid
 * @property string $created_at
 *
 * @property Authorization[] $authorizations
 */
class AuthorizationDevice extends ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return '{{%authorization__device}}';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['device_uuid'], 'required'],
            [['created_at'], 'safe'],
            [['id'], 'string', 'max' => 45],
            [['device_uuid'], 'string', 'max' => 255],
            [['id'], 'unique'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'device_uuid' => 'Device Uuid',
            'created_at' => 'Created At',
        ];
    }

    public function beforeSave($insert) {

        if ($this->isNewRecord) {
            $this->id = Yii::$app->db->createCommand('select uuid_v4s()')->queryScalar();
        }

        return parent::beforeSave($insert);

    }

    /**
     * Gets query for [[Authorizations]].
     *
     * @return ActiveQuery
     */
    public function getAuthorizations()
    {
        return $this->hasMany(Authorization::className(), ['authorization__device_id' => 'id']);
    }
}
