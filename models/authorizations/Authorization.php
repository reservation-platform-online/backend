<?php

namespace app\models\authorizations;

use app\models\types\TypeAuthorization;
use app\models\User;
use Yii;
use yii\db\ActiveQuery;
use yii\db\ActiveRecord;


/**
 * This is the model class for table "{{%authorization}}".
 *
 * @property string $id
 * @property int $type__authorization_id
 * @property string $user_id
 * @property string $authorization__client_id
 * @property string $authorization__device_id
 * @property int $enabled
 * @property string $created_at This is table like refresh_token
 * @property string $updated_at
 *
 * @property AuthorizationClient $authorizationClient
 * @property AuthorizationDevice $authorizationDevice
 * @property TypeAuthorization $typeAuthorization
 * @property User $user
 */
class Authorization extends ActiveRecord
{

    const ENABLED = 1;
    const DISABLED = 0;

    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return '{{%authorization}}';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['user_id', 'authorization__client_id', 'authorization__device_id'], 'required'],
            [['type__authorization_id', 'enabled'], 'integer'],
            [['created_at', 'updated_at'], 'safe'],
            [['id', 'user_id', 'authorization__client_id', 'authorization__device_id'], 'string', 'max' => 45],
            [['id'], 'unique'],
            [['authorization__client_id'], 'exist', 'skipOnError' => true, 'targetClass' => AuthorizationClient::className(), 'targetAttribute' => ['authorization__client_id' => 'id']],
            [['authorization__device_id'], 'exist', 'skipOnError' => true, 'targetClass' => AuthorizationDevice::className(), 'targetAttribute' => ['authorization__device_id' => 'id']],
            [['type__authorization_id'], 'exist', 'skipOnError' => true, 'targetClass' => TypeAuthorization::className(), 'targetAttribute' => ['type__authorization_id' => 'id']],
            [['user_id'], 'exist', 'skipOnError' => true, 'targetClass' => User::className(), 'targetAttribute' => ['user_id' => 'id']],
        ];
    }

    public function beforeSave($insert) {

        if ($this->isNewRecord) {
            $this->id = Yii::$app->db->createCommand('select uuid_v4s()')->queryScalar();
        }

        return parent::beforeSave($insert);

    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'type__authorization_id' => 'Type Authorization ID',
            'user_id' => 'User ID',
            'authorization__client_id' => 'Authorization Client ID',
            'authorization__device_id' => 'Authorization Device ID',
            'enabled' => 'Enabled',
            'created_at' => 'This is table like refresh_token',
            'updated_at' => 'Updated At',
        ];
    }

    /**
     * Gets query for [[AuthorizationClient]].
     *
     * @return ActiveQuery
     */
    public function getAuthorizationClient()
    {
        return $this->hasOne(AuthorizationClient::className(), ['id' => 'authorization__client_id']);
    }

    /**
     * Gets query for [[AuthorizationDevice]].
     *
     * @return ActiveQuery
     */
    public function getAuthorizationDevice()
    {
        return $this->hasOne(AuthorizationDevice::className(), ['id' => 'authorization__device_id']);
    }

    /**
     * Gets query for [[TypeAuthorization]].
     *
     * @return ActiveQuery
     */
    public function getTypeAuthorization()
    {
        return $this->hasOne(TypeAuthorization::className(), ['id' => 'type__authorization_id']);
    }

    /**
     * Gets query for [[User]].
     *
     * @return ActiveQuery
     */
    public function getUser()
    {
        return $this->hasOne(User::className(), ['id' => 'user_id']);
    }
}
