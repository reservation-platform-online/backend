<?php

namespace app\models\authorizations;

use yii\db\ActiveQuery;
use yii\db\ActiveRecord;

/**
 * This is the model class for table "{{%authorization__client}}".
 *
 * @property int $id
 * @property string $name
 * @property int $enabled
 * @property int $status
 * @property string $created_at
 * @property string $updated_at
 *
 * @property Authorization[] $authorizations
 */
class AuthorizationClient extends ActiveRecord
{
    const ENABLED = 1;
    const DISABLED = 0;

    const STATUS_DELETED = 0;
    const STATUS_ACTIVE = 1;

    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return '{{%authorization__client}}';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['name'], 'required'],
            [['enabled', 'status'], 'integer'],
            [['created_at', 'updated_at'], 'safe'],
            [['name'], 'string', 'max' => 45],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Name',
            'enabled' => 'Enabled',
            'status' => 'Status',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
        ];
    }

    /**
     * Gets query for [[Authorizations]].
     *
     * @return ActiveQuery
     */
    public function getAuthorizations()
    {
        return $this->hasMany(Authorization::className(), ['authorization__client_id' => 'id']);
    }
}
