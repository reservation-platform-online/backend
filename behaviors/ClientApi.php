<?php

namespace app\behaviors;

use app\models\authorizations\AuthorizationClient;
use app\models\business\BusinessCompany;
use yii;
use yii\base\Behavior;
use yii\web\HttpException;

class ClientApi extends Behavior
{
    public $only = [];

    public $checkClient = true;
    public $checkDevice = false;
    public $checkWorkerId = false;
    public $checkCompanyId = false;

    public function events()
    {
        return [
            yii\rest\Controller::EVENT_BEFORE_ACTION  => 'getCheckHeaders'
        ];
    }

    public function getCheckHeaders($action) {

        if (count($this->only) > 0) {

            if (!in_array($action->action->id, $this->only)) {

                return true;

            }

        }

        $headers = Yii::$app->request->getHeaders();

        $return = false;

        if ($this->checkClient) {

            if (isset($headers['Client-Id']) && strlen($headers['Client-Id']) > 0) {

                if ($client = AuthorizationClient::findOne([
                    'id' => $headers['Client-Id'],
                    'status' => AuthorizationClient::STATUS_ACTIVE
                ])) {


                    $return = true;

                } else {

                    throw new HttpException(400, 'Not found this client');

                }

            } else {

                throw new HttpException(400, 'Please, write to header Client-Id');

            }

        } else {

            $return = true;

        }

        if ($this->checkCompanyId) {

            if (isset($headers['Company-Id']) && strlen($headers['Company-Id']) > 0) {

                // Check in db

                $userId = Yii::$app->user->id;
                $company = BusinessCompany::find()
                    ->where(['business__company.id' => $headers['Company-Id']])
                    ->innerJoin('business__company__user', "business__company__user.business__company_id = business__company.id AND business__company__user.user_id = '$userId'")
                    ->one();

                if (is_null($company)) {
                    throw new HttpException(400, 'User dont have relation with selected company');
                } else {
                    $return = true;
                }

            } else {

                throw new HttpException(400, 'Please, write to header Company-Id');

            }

        } else {

            $return = true;

        }

        if ($this->checkWorkerId) {

            if (isset($headers['Worker-Id']) && strlen($headers['Worker-Id']) > 0) {

                // Check in db

                $return = true;

            } else {

                throw new HttpException(400, 'Please, write to header Worker-Id');

            }

        } else {

            $return = true;

        }


        if ($return && $this->checkDevice) {

            if (isset($headers['Device-Uuid']) && strlen($headers['Device-Uuid']) > 0) {

                // TODO check in DB
                // Check if device is enabled or not blocked

                $return = true;

            } else {

                throw new HttpException(400, 'Please, write to header your device');

            }

        } else {

            $return = true;

        }

        return $return;

    }

}