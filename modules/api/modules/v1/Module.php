<?php


namespace app\modules\api\modules\v1;

/**
 * admin module definition class
 */
class Module extends \yii\base\Module
{

        /**
     * {@inheritdoc}
     */
    public function init()
    {
        parent::init();

        // custom initialization code goes here

        $this->modules = [
            'client' => [
                // you should consider using a shorter namespace here!
                'class' => 'app\modules\api\modules\v1\modules\client\Module',
            ],
            'admin' => [
                // you should consider using a shorter namespace here!
                'class' => 'app\modules\api\modules\v1\modules\admin\Module',
            ],
            'company' => [
                // you should consider using a shorter namespace here!
                'class' => 'app\modules\api\modules\v1\modules\company\Module',
            ],
            'worker' => [
                // you should consider using a shorter namespace here!
                'class' => 'app\modules\api\modules\v1\modules\worker\Module',
            ],
            'cron' => [
                // you should consider using a shorter namespace here!
                'class' => 'app\modules\api\modules\v1\modules\cron\Module',
            ],
        ];

    }
}
