<?php

namespace app\modules\api\modules\v1\modules\cron\controllers;

use Yii;
use yii\filters\Cors;
use yii\rest\Controller;

/**
 * UserController implements the CRUD actions for Offer model.
 */
class CronController extends Controller
{

    public function beforeAction($action)
    {

        parent::beforeAction($action);


        if (Yii::$app->getRequest()->getMethod() === 'OPTIONS') {

            Yii::$app->getResponse()->getHeaders()->set('Allow', 'POST GET PUT DELETE');

            Yii::$app->end();

        }


        return true;

    }

    public function behaviors()
    {
        $behaviors = parent::behaviors();

        unset($behaviors['authenticator']);

        $behaviors['corsFilter'] = [

            'class' => Cors::className(),

        ];

        return $behaviors;

    }

    /**
     *
     * Lists all Offer models.
     * @param $password
     * @return mixed
     */
    public function actionPerMinute(
        $password
    )
    {

        if ($password === '60400cd6b3300') {

            /**
             * Check if reservation is expires
             */
            Yii::$app->db->createCommand("call event__per_minute();")->queryScalar();

            $this->buildAndSendNotificationOfReservation();

        }

        return true;

    }

    /**
     *
     * Lists all Offer models.
     * @param $password
     * @return mixed
     */
    public function actionPerDay(
        $password
    )
    {

        if ($password === '60400f4e935c0') {

            Yii::$app->db->createCommand("call event__per_day();")->queryScalar();

        }

        return true;

    }

    protected function _sendEmail($email, $subject, $options)
    {

        $emailSend = Yii::$app->mailer->compose(['html' => $options['html']], $options['variables'])
            ->setFrom([Yii::$app->params['infoEmail'] => Yii::$app->params['nameOfInfoEmail']])
            ->setTo($email)
            ->setSubject($subject);

        return $emailSend->send();
    }

    private function buildAndSendNotificationOfReservation()
    {

        /**
         * Get reservation for send notification
         */
        $businessReservation = Yii::$app->db->createCommand("SELECT * FROM business__reservation WHERE ((payment_is_important = 1 AND charge = 1) OR (payment_is_important = 0 AND status = 2)) AND date = current_date() AND subtime(start_at, '00:30:00') >= CURRENT_TIME AND subtime(start_at, '00:30:00') < addtime(CURRENT_TIME, '0:1:0')")->queryScalar();

        $title = '<h1>Masz spotkanie za 30 min.</h1>';

        foreach ($businessReservation as $key => $reservation) {

            $passwordOfRoom = '<p>Hasło do spotkanie: ' . $reservation['meet_password'] . '</p>';
            $linkToRoom = '<p>Link do spotkanie: <a href="' . $reservation['meet_link'] . '">' . $reservation['meet_link'] . '</a></p>';
            $linkToPWS = '<p>Usługa: <a href="https://reservation-platform-online/service/' . $reservation['business__point_worker_service_id'] . '">https://reservation-platform-online/#/service/' . $reservation['business__point_worker_service_id'] . '</a></p>';

            $service = Yii::$app->db->createCommand("SELECT name FROM business__service INNER JOIN business__point_worker_service as pws ON pws.id = '" . $reservation['business__point_worker_service_id'] . "' AND pws.business__service_id = business__service.id;")->queryOne();

            $serviceName = '<h2>' . $service['name'] . '</h2>';

            $clients = Yii::$app->db->createCommand("SELECT user.email as email, user.first_name as first_name, user.last_name as last_name, bru.meet_token as meet_token FROM user INNER JOIN business__reservation__user as bru ON bru.user_id = user.id AND bru.business__reservation_id = '" . $reservation['id'] . "';")->query();

            $clientList = '';

            foreach ($clients as $keyClient => $client) {

                $clientList .= '<p>' . $client['last_name'] . ' ' . $client['first_name'] . '</p>';

                /**
                 * Send to client of reservation notification email
                 */

                $EMAIL_TEXT2 = $title;
                $EMAIL_TEXT2 .= $serviceName;
                if ($client['meet_token'] && strlen($client['meet_token']) > 0) {
                    $EMAIL_TEXT2 .= '<p>Link do spotkanie: <a href="' . $client['meet_token'] . '">' . $client['meet_token'] . '</a></p>';
                }
                if ($reservation['meet_password'] && strlen($reservation['meet_password']) > 0) {
                    $EMAIL_TEXT2 .= $passwordOfRoom;
                }
                $EMAIL_TEXT2 .= $linkToPWS;
                $EMAIL_SUBJECT2 = 'Przypomnienie';

                $this->_sendEmail($client['email'], $EMAIL_SUBJECT2, [
                    'html' => '@app/mail/layouts/new-password',
                    'variables' => [
                        'content' => $EMAIL_TEXT2
                    ]
                ]);

            }

            /**
             * Send to worker notification email
             */
            $workerEmail = Yii::$app->db->createCommand("SELECT email FROM user INNER JOIN business__point_worker_service as pws ON pws.id = '" . $reservation['business__point_worker_service_id'] . "' INNER JOIN business__worker as bw ON pws.business__worker_id = bw.id AND bw.user_id = user.id")->queryOne();

            $EMAIL_TEXT = $title;
            $EMAIL_TEXT .= $serviceName;
            if ($reservation['meet_link'] && strlen($reservation['meet_link']) > 0) {
                $EMAIL_TEXT .= $linkToRoom;
            }
            if ($reservation['meet_password'] && strlen($reservation['meet_password']) > 0) {
                $EMAIL_TEXT .= $passwordOfRoom;
            }
            $EMAIL_TEXT .= $linkToPWS;
            $EMAIL_TEXT .= '<p>Spotkanie masz z:</p>';
            $EMAIL_TEXT .= $clientList;
            $EMAIL_SUBJECT = 'Przypomnienie';

            $this->_sendEmail($workerEmail['email'], $EMAIL_SUBJECT, [
                'html' => '@app/mail/layouts/new-password',
                'variables' => [
                    'content' => $EMAIL_TEXT
                ]
            ]);

        }
    }

}
