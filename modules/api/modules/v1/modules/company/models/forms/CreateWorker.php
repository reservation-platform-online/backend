<?php

namespace app\modules\api\modules\v1\modules\company\models\forms;

use app\models\additional\AdditionalWeekend;
use app\models\business\BusinessPointWorkerService;
use app\models\User;
use app\modules\api\modules\v1\modules\company\models\business\BusinessCompany;
use app\modules\api\modules\v1\modules\company\models\business\BusinessWorker;
use app\modules\api\modules\v1\modules\company\models\business\BusinessWorkTime;
use Throwable;
use Yii;
use yii\base\Model;
use yii\db\DataReader;

/**
 * Class CreateWorker
 * @package app\modules\api\modules\v1\modules\company\models\forms
 *
 * @property string $business__company_id
 * @property string|null $user_id
 * @property string $first_name
 * @property string $last_name
 * @property string|null $photo
 * @property string|null $position
 * @property int $enabled
 *
 * @property AdditionalWeekend[] $additionalWeekends
 * @property BusinessPointWorkerService[] $businessPointWorkerServices
 * @property BusinessWorkTime[] $businessWorkTimes
 * @property User $user
 * @property BusinessCompany $businessCompany
 */
class CreateWorker extends Model
{

    public $userEmail;
    public $firstName;
    public $lastName;
    public $photo;
    public $position;
    public $enabled;

    /**
     * @var BusinessCompany|mixed|null
     */
    private $_company = false;

    /**
     * @var
     */
    private $worker;

    /**
     * @var mixed
     */
    private $_userWorker = false;

    /**
     * @return array the validation rules.
     */
    public function rules()
    {
        return [
            [['firstName', 'lastName'], 'required'],
            ['enabled', 'default', 'value' => 1],
            [['position', 'userEmail', 'photo'], 'string'],
            ['userEmail', 'email']
        ];
    }

    /**
     * Logs in a user using the provided username and password.
     * @return array|false|string|DataReader
     * @throws Throwable
     */
    public function create()
    {

        if ($this->validate()) {

            $this->worker = new BusinessWorker();

            $this->worker->business__company_id = $this->getCompany()->id;
            $this->worker->first_name = $this->firstName;
            $this->worker->last_name = $this->lastName;
            $this->worker->photo = $this->photo;
            $this->worker->position = $this->position;
            $this->worker->enabled = $this->enabled;

            if (!is_null($this->userEmail)) {
                if ($this->getUserWorker()) {
                    $this->worker->user_id = $this->getUserWorker()->id;
                }
            }

            if ($this->worker->validate()) {

                if ($this->worker->save()) {

//                    if ($this->worker->user_id) {
//
//                        $this->sendNotificationEmail();
//
//                    } else {
//
//                        $this->sendFinishRegistrationEmail();
//
//                    }

                    return $this->worker->id;

                }

            } else {

                $response = Yii::$app->response;
                $response->setStatusCode(400);
                $response->data = $this->worker->errors;
                return $response;

            }

        }

        $response = Yii::$app->response;
        $response->setStatusCode(400);
        $response->data = $this->errors;
        return $response;

    }

    protected function _sendEmail($email, $subject, $options) {

        $emailSend = Yii::$app->mailer->compose( ['html' =>  $options['html']], $options['variables'])
            ->setFrom([Yii::$app->params['infoEmail'] => Yii::$app->params['nameOfInfoEmail']])
            ->setTo($email)
            ->setSubject($subject);

        return $emailSend->send();
    }

    private function sendNotificationEmail()
    {

        $EMAIL_TEXT = "Jesteś od teraz też jako pracownik w '" . $this->getCompany()->name . "'.";
        $EMAIL_TEXT .= "<br>";
        $EMAIL_TEXT .= '<a href="https://reservation-platform-online/panel/worker/' . $this->worker->id . '" target="_blank">
                        Głowna strona panelu pracownika
                    </a>';
        $EMAIL_TEXT .= '<a href="https://reservation-platform-online/panel/worker/' . $this->worker->id . '/reservation" target="_blank">
                        Kalendarz rezerwacji
                    </a>';
        $EMAIL_SUBJECT = "Przypisanie roli pracownika w '" . $this->getCompany()->name . "'";


        $this->_sendEmail($this->userEmail, $EMAIL_SUBJECT, [
            'html' => '@app/mail/layouts/html',
            'variables' => [
                'content' => $EMAIL_TEXT,
            ]
        ]);

    }

    private function sendFinishRegistrationEmail()
    {

        $EMAIL_TEXT = "Jesteś od teraz też jako pracownik w '" . $this->getCompany()->name . "'.";
        $EMAIL_TEXT .= "<br>Tutaj będziesz miał link do zakończenia rejestracji.
                    Tam będziesz musiał wpisać hasło i tyle. ";
        $EMAIL_SUBJECT = "Przypisanie roli pracownika w '" . $this->getCompany()->name . "'";


        $this->_sendEmail($this->userEmail, $EMAIL_SUBJECT, [
            'layout' => 'layouts/html',
            'variables' => [
                'content' => $EMAIL_TEXT,
            ]
        ]);

        // TODO send link to finish registration

    }

    private function getCompany()
    {

        if ($this->_company === false) {
            $this->_company = BusinessCompany::findOne(['id' => Yii::$app->request->getHeaders()->get('Company-Id')]);
        }
        return $this->_company;

    }

    private function getUserWorker()
    {

        if ($this->_userWorker === false) {
            $this->_userWorker = User::findOne(['email' => $this->userEmail]);
        }
        return $this->_userWorker;

    }

}
