<?php

namespace app\modules\api\modules\v1\modules\company\models\forms;

use app\models\additional\AdditionalWeekend;
use app\models\business\BusinessPointWorkerService;
use app\models\User;
use app\modules\api\modules\v1\modules\company\models\business\BusinessCompany;
use app\modules\api\modules\v1\modules\company\models\business\BusinessReservation;
use app\modules\api\modules\v1\modules\company\models\business\BusinessWorker;
use app\modules\api\modules\v1\modules\company\models\business\BusinessWorkTime;
use Throwable;
use Yii;
use yii\base\Model;
use yii\db\DataReader;

/**
 * Class CreateWorker
 * @package app\modules\api\modules\v1\modules\company\models\forms
 *
 * @property string $business__company_id
 * @property string|null $user_id
 * @property string $first_name
 * @property string $last_name
 * @property string|null $photo
 * @property string|null $position
 * @property int $enabled
 *
 * @property AdditionalWeekend[] $additionalWeekends
 * @property BusinessPointWorkerService[] $businessPointWorkerServices
 * @property BusinessWorkTime[] $businessWorkTimes
 * @property User $user
 * @property BusinessCompany $businessCompany
 */
class UpdateWorker extends Model
{

    public $id;
    public $userEmail;
    public $firstName;
    public $lastName;
    public $photo;
    public $position;
    public $enabled;

    /**
     * @return array the validation rules.
     */
    public function rules()
    {
        return [
            [['enabled'], 'number'],
            [['position', 'userEmail', 'photo', 'id', 'lastName', 'firstName'], 'string'],
            ['userEmail', 'email']
        ];
    }

    /**
     * Logs in a user using the provided username and password.
     * @return array|false|string|DataReader
     * @throws Throwable
     */
    public function save()
    {

        if ($this->validate()) {

            $worker = BusinessWorker::findOne($this->id);
            $error = null;

            if ($worker) {

                $worker->business__company_id = Yii::$app->request->getHeaders()->get('Company-Id');

                if ($this->firstName) {
                    $worker->first_name = $this->firstName;
                }

                if ($this->lastName) {
                    $worker->last_name = $this->lastName;
                }

                if ($this->photo) {
                    $worker->photo = $this->photo;
                }

                if ($this->position) {
                    $worker->position = $this->position;
                }

                if (!is_null($this->enabled)) {
                    $this->enabled = intval($this->enabled);
//                    if ($worker->enabled === BusinessWorker::ENABLED && $this->enabled === BusinessWorker::DISABLED) {
//                        $error = $this->checkReservation($worker);
//                    }
                    $worker->enabled = $this->enabled;
                }

//                if (!is_null($error)) {
//                    return $error;
//                }

                $user = null;

                if (!is_null($this->userEmail)) {
                    $user = User::findOne(['email' => $this->userEmail]);
                }

                if ($user) {
                    // TODO create and send email with notification about you are connect to worker
                    $worker->user_id = $user->id;
                } else {
                    // TODO create and send email for registration and connect to worker
                }

                if ($worker->validate() && $worker->save()) {

                    return $worker->id;

                } else {

                    $response = Yii::$app->response;
                    $response->setStatusCode(400);
                    $response->data = $worker->errors;
                    return $response;

                }

            }

        }

        $response = Yii::$app->response;
        $response->setStatusCode(400);
        $response->data = $this->errors;
        return $response;

    }

    private function checkReservation($worker)
    {

        $reservation = BusinessReservation::find()
            ->where([
                'in', 'business__reservation.status', [
                    BusinessReservation::STATUS_ENABLED,
                    BusinessReservation::STATUS_CONFIRMED
                ]
            ])
            ->innerJoin(
                'business__point_worker_service',
                "business__point_worker_service.id = business__reservation.business__point_worker_service_id AND business__point_worker_service.business__worker_id = '$worker->id'"
            )
            ->one();

        if ($reservation) {

            $response = Yii::$app->response;
            $response->setStatusCode(400);
            $response->data = [
                'Worker have reservation'
            ];
            return $response;

        } else {

            return null;

        }

    }

}
