<?php
namespace app\modules\api\modules\v1\modules\company\models\forms;

use Yii;
use yii\base\Model;
use yii\helpers\Url;
use yii\web\UploadedFile;

/**
 * Signup form
 */
class UploadFileForm extends Model
{

    /**
     * @var UploadedFile
     */
    public $imageFile;
    public $rootPath; // last char is: /

    public function rules()
    {
        return [
            [['rootPath', 'imageFile'], 'required'],
            [['rootPath'], 'string'],
            [['imageFile'], 'file'],
        ];
    }

    /**
     * @return array|bool|string
     */
    public function upload()
    {

        if ($this->validate()) {

            $path = uniqid();
            $format = '.jpg';

            if (!file_exists($this->rootPath)) {

                mkdir($this->rootPath, 0777, true);

            }

            $filename = $path . $format;
            $fullPath = $this->rootPath . $filename;

            $this->imageFile->saveAs($fullPath);

            return [
                'url' => Url::base('https') . '/' . $fullPath,
                'filename' => $filename
            ];

        }

        $response = Yii::$app->response;
        $response->setStatusCode(400);
        $response->data = $this->errors;
        return $response;

    }

}