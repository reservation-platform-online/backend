<?php

namespace app\modules\api\modules\v1\modules\company\models;

use app\models\additional\AdditionalAddressUser;
use app\models\additional\AdditionalLike;
use app\models\additional\AdditionalWeekend;
use app\models\agreements\AgreementUser;
use app\models\authorizations\Authorization;
use app\models\business\BusinessCompanyUser;
use app\models\business\BusinessReservationUser;
use app\models\business\BusinessWorker;
use app\models\contacts\ContactUser;
use app\models\UserHistoryPassword;
use app\models\zoom\ZoomAccessToken;
use app\models\zoom\ZoomAccessTokenBusinessWorker;
use yii\db\ActiveQuery;
use yii\helpers\Inflector;

/**
 * This is the model class for table "{{%user}}".
 *
 * @property string $id
 * @property string $email
 * @property string $first_name
 * @property string $last_name
 * @property string $password_hash
 * @property string|null $password_reset_token
 * @property string|null $verification_token
 * @property string|null $photo
 * @property string|null $phone
 * @property string|null $birthday
 * @property int $role 0 - Admin 1 - Company 2 - User
 * @property int $number_of_violations
 * @property int $hide
 * @property int $enabled
 * @property int $status
 * @property string $created_at
 * @property string $updated_at
 *
 * @property AdditionalAddressUser[] $additionalAddressUsers
 * @property AdditionalLike[] $additionalLikes
 * @property AgreementUser[] $agreementUsers
 * @property Authorization[] $authorizations
 * @property BusinessCompanyUser[] $businessCompanyUsers
 * @property BusinessReservationUser[] $businessReservationUsers
 * @property BusinessWorker[] $businessWorkers
 * @property ContactUser[] $contactUsers
 * @property UserHistoryPassword[] $userHistoryPasswords
 */
class User extends \app\models\User
{

    /**
     * {@inheritdoc}
     */
    public function fields()
    {
        return [
            'id',
            'email',
            'firstName' => 'first_name',
            'lastName' => 'last_name',
            'photo',
            'phone',
            'birthday',
            'numberOfViolations' => 'number_of_violations',
        ];
    }

}
