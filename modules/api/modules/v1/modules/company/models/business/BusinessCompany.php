<?php

namespace app\modules\api\modules\v1\modules\company\models\business;

use app\models\business\BusinessCompanyUser;
use app\models\business\BusinessPoint;
use app\models\business\BusinessService;
use app\models\business\BusinessWorker;
use app\models\promoCodes\PromoCode;
use app\models\tariffs\TariffSubscribe;
use yii\helpers\Inflector;

/**
 * This is the model class for table "{{%business__company}}".
 *
 * @property string $id
 * @property string $name
 * @property string|null $seo_link
 * @property string|null $logo_link
 * @property string|null $description
 * @property int $enabled
 * @property int $status
 * @property string $created_at
 * @property string $updated_at
 *
 * @property BusinessCompanyUser[] $businessCompanyUsers
 * @property BusinessPoint[] $businessPoints
 * @property BusinessService[] $businessServices
 * @property BusinessWorker[] $businessWorkers
 * @property PromoCode[] $promoCodes
 * @property TariffSubscribe[] $tariffSubscribes
 */
class BusinessCompany extends \app\models\business\BusinessCompany
{

    const STATUS_ACTIVE = 2;

    const SCENARIO_UPDATE = 'update';
    const SCENARIO_UPDATE_LOGO = 'update_logo';

    public function scenarios()
    {
        return [
            self::SCENARIO_UPDATE => ['name', 'seo_link', 'logo_link', 'description', 'enabled'],
            self::SCENARIO_UPDATE_LOGO => ['seo_link'],
        ];
    }

    /**
     * @return array|false
     */
    public function fields() {

        // what you want is to rename the keys of parent::fields()
        $formattedFields = [];
        $fields = [
            'id',
            'name',
            'seo_link',
            'logo_link',
            'description',
            'type__company_id',
            'status',
            'created_at',
            'updated_at',
            'enabled',
        ];
        foreach (parent::fields() as $key => $name) {
            if (in_array($key, $fields)) {
                $formattedFields[Inflector::variablize($key)] = $name;
            }
        }

        return $formattedFields;
    }

    public function load($data, $formName = null)
    {

        foreach ($data as $key => $name){
            $data[Inflector::underscore($key)] = $name;
        }


        return parent::load($data, $formName);
    }

}
