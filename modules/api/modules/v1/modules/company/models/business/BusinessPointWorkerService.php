<?php

namespace app\modules\api\modules\v1\modules\company\models\business;

use app\models\additional\AdditionalLike;
use app\models\business\BusinessComment;
use app\models\business\BusinessCounterPerSlot;
use app\models\business\BusinessPoint;
use app\models\business\BusinessPointWorkerServiceTypeService;

/**
 * This is the model class for table "{{%business__point_worker_service}}".
 *
 * @property string $id
 * @property string $business__company_id
 * @property string|null $business__point_id NULL when service is online
 * @property string $business__worker_id
 * @property string $business__service_id
 * @property int $limit_reservation_per_slot
 * @property int $enabled
 * @property int $hide
 * @property int $status
 * @property int $number_of_comments
 * @property float $score
 * @property string $created_at
 * @property string $updated_at
 *
 * @property AdditionalLike[] $additionalLikes
 * @property BusinessComment[] $businessComments
 * @property BusinessCompany $businessCompany
 * @property BusinessWorker $businessWorker
 * @property BusinessPoint $businessPoint
 * @property BusinessService $businessService
 * @property BusinessPointWorkerServiceTypeService[] $businessPointWorkerServiceTypeServices
 * @property BusinessReservation[] $businessReservations
 * @property BusinessWorkTime[] $businessWorkTimes
 */
class BusinessPointWorkerService extends \app\models\business\BusinessPointWorkerService
{

    /**
     * {@inheritdoc}
     */
    public function fields()
    {
        return [
            'id',
            'companyId' => 'business__company_id',
            'pointId' => 'business__point_id',
            'worker' => function($model) {
                return BusinessWorker::findOne($model->business__worker_id);
            },
            'service' => function($model) {
                return BusinessService::findOne($model->business__service_id);
            },
            'workTimeList' => function($model) {
                return BusinessWorkTime::find()
                    ->where([
                        'business__point_worker_service_id' => $model->id,
                        'business__worker_id' => $model->business__worker_id
                    ])
                    ->orderBy('weekday')
                    ->all();
            },
            'limitReservationPerSlot' => 'limit_reservation_per_slot',
            'enabled',
            'numberOfComments' => 'number_of_comments',
            'score',
            'status',
            'createdAt' => 'created_at',
            'updatedAt' => 'updated_at',
        ];
    }
}
