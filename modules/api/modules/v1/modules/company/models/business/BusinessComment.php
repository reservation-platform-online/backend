<?php

namespace app\modules\api\modules\v1\modules\company\models\business;

use app\models\business\BusinessPointWorkerService;
use app\models\business\BusinessReservationUser;
use app\modules\api\modules\v1\modules\client\models\User;

/**
 * This is the model class for table "{{%business__comment}}".
 *
 * @property string $id
 * @property string $business__point_worker_service_id
 * @property int $is_anonymous
 * @property string $comment
 * @property int $score
 * @property int $enabled
 * @property int $hide
 * @property string $created_at
 * @property string $updated_at
 * @property string $business__reservation__user_id
 *
 * @property BusinessPointWorkerService $businessPointWorkerService
 * @property BusinessReservationUser $businessReservationUser
 */
class BusinessComment extends \app\models\business\BusinessComment
{

    /**
     * {@inheritdoc}
     */
    public function fields()
    {

        return [
            'id',
            'isAnonymous' => 'is_anonymous',
            'comment',
            'score',
            'createdAt' => 'created_at',
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function extraFields()
    {

        return [
            'user' => function($model) {

                $user = User::find()
                    ->innerJoin(BusinessReservationUser::tableName(), "business__reservation__user.user_id = user.id AND business__reservation__user.id = '$model->business__reservation__user_id'")
                    ->one();

                if ($user) {

                    return [
                        'firstName' => $user->first_name,
                        'lastName' => $user->last_name,
                        'photo' => $user->photo,
                        'id' => $user->id,
                    ];

                }

                return '-';

            }
        ];
    }

}
