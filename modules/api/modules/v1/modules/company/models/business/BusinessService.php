<?php

namespace app\modules\api\modules\v1\modules\company\models\business;

use app\models\business\BusinessPointWorkerService;
use app\models\categories\Category;
use app\models\galleries\GalleryService;
use app\models\promoCodes\PromoCodeBusinessService;
use app\modules\api\modules\v1\modules\company\models\galleries\GalleryImage;
use yii\db\ActiveRecord;
use yii\helpers\Inflector;

/**
 * This is the model class for table "{{%business__service}}".
 *
 * @property string $id
 * @property string $business__company_id
 * @property int $category_id
 * @property string $name
 * @property string $description
 * @property string $interval
 * @property float $price
 * @property float|null $price_to
 * @property float|null $discount
 * @property int|null $discount_is_percent
 * @property string $earliest_reservation_time
 * @property string|null $earliest_reservation_date
 * @property string|null $last_reservation_date
 * @property int $payment_is_important
 * @property string|null $timer_time_for_pay
 * @property int $hide
 * @property int $enabled
 * @property int $status
 * @property string $created_at
 * @property string $updated_at
 *
 * @property BusinessPointWorkerService[] $businessPointWorkerServices
 * @property Category $category
 * @property BusinessCompany $businessCompany
 * @property GalleryService[] $galleryServices
 * @property PromoCodeBusinessService[] $promoCodeBusinessServices
 */
class BusinessService extends \app\models\business\BusinessService
{

//    const SCENARIO_UPDATE = 'update';
//
//    public function scenarios()
//    {
//        return [
//            self::SCENARIO_UPDATE => [
//                'name',
//                'categoryId',
//                'interval',
//                'description',
//                'price',
//                'price_to',
//                'discount',
//                'discount_is_percent',
//                'earliest_reservation_time',
//                'earliest_reservation_date',
//                'last_reservation_date',
//                'payment_is_important',
//                'timer_time_for_pay',
//                'enabled'
//            ],
//        ];
//    }

    /**
     * @return array
     */
    public function fields()
    {

        return [
            'id',
            'companyId' => 'business__company_id',
            'categoryId' => 'category_id',
            'name',
            'description',
            'interval',
            'price',
            'priceTo' => 'price_to',
            'discount',
            'discountIsPercent' => 'discount_is_percent',
            'earliestReservationTime' => 'earliest_reservation_time',
            'earliestReservationDate' => 'earliest_reservation_date',
            'lastReservationDate' => 'last_reservation_date',
            'paymentIsImportant' => 'payment_is_important',
            'timerTimeForPay' => 'timer_time_for_pay',
            'hide',
            'enabled',
            'status',
            'createdAt' => 'created_at',
            'updatedAt' => 'updated_at',
            'images' => function () {
                return GalleryImage::find()
                    ->innerJoin('gallery__service', "gallery__service.business__service_id = '$this->id' AND gallery__service.gallery_id = gallery__image.gallery_id")
                    ->where(['gallery__image.enabled' => GalleryImage::ENABLED])
                    ->all();
            }
        ];

    }


    public function extraFields()
    {
        return [
            'pwsList' => function ($model) {
                return \app\modules\api\modules\v1\modules\company\models\business\BusinessPointWorkerService::find()
                    ->where([
                        'business__service_id' => $model->id
                    ])
                    ->all();
            }
        ];
    }

    /**
     * @param array $data
     * @param null $formName
     * @return bool
     */
    public function load($data, $formName = null)
    {

        foreach ($data as $key => $name) {
            $data[Inflector::underscore($key)] = $name;
        }


        return parent::load($data, $formName);
    }

    /**
     * @return array|ActiveRecord|null
     */
    public function hasImages()
    {

        return GalleryImage::find()
            ->where([
                'enabled' => GalleryImage::ENABLED
            ])
            ->innerJoin(GalleryService::tableName(), "gallery__service.business__service_id = '$this->id' AND gallery__service.gallery_id = gallery__image.gallery_id")
            ->one();

    }

}
