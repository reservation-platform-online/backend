<?php

namespace app\modules\api\modules\v1\modules\company\models\business;

use app\models\additional\AdditionalAddressBusinessReservationUser;
use app\modules\api\modules\v1\modules\company\models\User;
use yii\db\ActiveQuery;

/**
 * This is the model class for table "{{%business__reservation__user}}".
 *
 * @property string $id
 * @property string $business__reservation_id
 * @property string $user_id
 * @property string $email
 * @property string|null $note
 * @property int $is_initiator
 * @property int $number_of_comments
 * @property string|null $meet_token
 * @property int $enabled
 * @property int $hide
 * @property string $created_at
 * @property string $updated_at
 * @property int $status 0 - Not present 1 - Wait 2 - Present
 *
 * @property AdditionalAddressBusinessReservationUser[] $additionalAddressBusinessReservationUsers
 * @property BusinessComment[] $businessComments
 * @property BusinessReservation $businessReservation
 * @property User $user
 */
class BusinessReservationUser extends \app\models\business\BusinessReservationUser
{

    /**
     * {@inheritdoc}
     */
    public function fields()
    {
        return [
            'id',
//            'reservationId' => 'business__reservation_id',
            'user',
            'email',
//            'note',
//            'isInitiator' => 'is_initiator',
//            'meetToken' => 'meet_token',
//            'createdAt' => 'created_at',
//            'updatedAt' => 'updated_at',
//            'status',
        ];
    }

    /**
     * Gets query for [[User]].
     *
     * @return ActiveQuery
     */
    public function getUser()
    {
        return $this->hasOne(User::className(), ['id' => 'user_id']);
    }

}
