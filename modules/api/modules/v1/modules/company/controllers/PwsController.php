<?php

namespace app\modules\api\modules\v1\modules\company\controllers;

use app\modules\api\modules\v1\modules\company\models\business\BusinessComment;
use app\modules\api\modules\v1\modules\company\models\business\BusinessService;
use app\modules\api\modules\v1\modules\company\models\customs\Pws;
use Yii;
use yii\db\ActiveRecord;
use yii\filters\auth\HttpBearerAuth;
use yii\rest\Controller;

/**
 * UserController implements the CRUD actions for Offer model.
 */
class PwsController extends Controller
{

    public function beforeAction($action)
    {

        parent::beforeAction($action);


        if (Yii::$app->getRequest()->getMethod() === 'OPTIONS') {

            Yii::$app->getResponse()->getHeaders()->set('Allow', 'POST GET PUT DELETE');

            Yii::$app->end();

        }


        return true;

    }

    public function behaviors()
    {
        $behaviors = parent::behaviors();

        $auth = $behaviors['authenticator'];

        unset($behaviors['authenticator']);

        $behaviors['corsFilter'] = [

            'class' => \yii\filters\Cors::className(),

        ];

        $behaviors['authenticator'] = $auth;

        // avoid authentication on CORS-pre-flight requests (HTTP OPTIONS method)

        $behaviors['authenticator'] = [

            'class' => HttpBearerAuth::className(),
            'only' => [
                'index', 'view', 'comments', 'toggle-enabled'
            ],
            'except' => [
                'options'
            ]

        ];

        $behaviors['client_api'] = [
            'class' => 'app\behaviors\ClientApi',
            'checkCompanyId' => true
        ];

        return $behaviors;
    }

    /**
     * @param null $categoryId
     * @param int $page
     * @param int $count
     * @param string $orderBy
     * @param string $sort
     * @return array|string|ActiveRecord|ActiveRecord[]|null
     */
    public function actionIndex(
        $categoryId = null,
        $page = 1,
        $count = 5,
        $orderBy = 'id',
        $sort = 'desc'
    )
    {

        $query = Pws::find()
            ->where([
                'business__point_worker_service.enabled' => Pws::ENABLED,
                'business__point_worker_service.hide' => Pws::SHOW,
                'business__point_worker_service.business__company_id' => Yii::$app->request->headers->get('Company-Id')
            ])
            ->groupBy('business__point_worker_service.id');

        $query->orderBy($orderBy . " " . $sort);

        return [
            'total' => (integer)$query->count(),
            'models' => $query->limit($count)->offset(($page - 1) * $count)->all(),
        ];

    }

    /**
     *
     * Lists all Offer models.
     * @param $id
     * @return mixed
     */
    public function actionToggleEnable($id)
    {

        $model = Pws::findOne(['id' => $id]);

        if ($model) {

            $model->enabled = $model->enabled === Pws::ENABLED ? Pws::DISABLED : Pws::ENABLED;

            if ($model->validate()) {

                return $model->save();

            }

        }

        $response = Yii::$app->response;
        $response->setStatusCode(400);
        $response->data = $model->errors;
        return $response;

    }

    /**
     * @param $id
     * @return array|string|ActiveRecord|ActiveRecord[]|null
     */
    public function actionView($id)
    {

        return Pws::findOne($id);

    }

    /**
     * @param $id
     * @param int $page
     * @param int $count
     * @param string $orderBy
     * @param string $sort
     * @return array|string|ActiveRecord|ActiveRecord[]|null
     */
    public function actionComments(
        $id,
        $page = 1,
        $count = 5,
        $orderBy = 'created_at',
        $sort = 'desc'
    )
    {

        $query = BusinessComment::find()
        ->where([
            'enabled' => BusinessComment::ENABLED,
            'hide' => BusinessComment::SHOW,
            'business__point_worker_service_id' => $id
        ]);

        $query->orderBy($orderBy . " " . $sort);

        return [
            'total' => (integer)$query->count(),
            'models' => $query->limit($count)->offset(($page - 1) * $count)->all(),
        ];

    }

}
