<?php

namespace app\modules\api\modules\v1\modules\company\controllers;

use app\modules\api\modules\v1\modules\company\models\business\BusinessWorkTime;
use Yii;
use yii\filters\auth\HttpBearerAuth;
use yii\rest\Controller;
use yii\web\HttpException;

/**
 * UserController implements the CRUD actions for Offer model.
 */
class WorkTimeController extends Controller
{

    public function beforeAction($action)
    {

        parent::beforeAction($action);


        if (Yii::$app->getRequest()->getMethod() === 'OPTIONS') {

            Yii::$app->getResponse()->getHeaders()->set('Allow', 'POST GET PUT DELETE');

            Yii::$app->end();

        }


        return true;

    }

    public function behaviors()
    {
        $behaviors = parent::behaviors();

        $auth = $behaviors['authenticator'];

        unset($behaviors['authenticator']);

        $behaviors['corsFilter'] = [

            'class' => \yii\filters\Cors::className(),

        ];

        $behaviors['authenticator'] = $auth;

        // avoid authentication on CORS-pre-flight requests (HTTP OPTIONS method)

        $behaviors['authenticator'] = [

            'class' => HttpBearerAuth::className(),
            'only' => [
                'view', 'create', 'update', 'delete'
            ],
            'except' => [
                'options'
            ]

        ];

        $behaviors['client_api'] = [
            'class' => 'app\behaviors\ClientApi',
            'checkCompanyId' => true
        ];

        return $behaviors;
    }

    /**
     *
     * Lists all Offer models.
     * @param $id
     * @return mixed
     */
    public function actionView($id)
    {

        return BusinessWorkTime::findOne($id);

    }

    /**
     *
     * Lists all Offer models.
     * @param $id
     * @return mixed
     */
    public function actionCreate()
    {

        $model = new BusinessWorkTime();

        try {

            if ($model->load(\Yii::$app->request->post(), '')) {

                if ($model->validate()) {

                    return $model->save();

                }

            }

        } catch (\Exception $error) {
            throw new HttpException(400, $error);
        }

        $response = Yii::$app->response;
        $response->setStatusCode(400);
        $response->data = $model->errors;
        return $response;

    }

    /**
     *
     * Lists all Offer models.
     * @param $id
     * @return mixed
     */
    public function actionUpdate($id)
    {

        if (count(Yii::$app->request->post()) === 0) {
            $response = Yii::$app->response;
            $response->setStatusCode(400);
            $response->data = 'Data is empty';
            return $response;
        }

        $model = BusinessWorkTime::findOne(['id' => $id]);

        if ($model) {

            if ($model->load(Yii::$app->request->post(), '')) {

                if ($model->validate()) {

                    return $model->save();

                }

            }

        }

        $response = Yii::$app->response;
        $response->setStatusCode(400);
        $response->data = $model->errors;
        return $response;

    }

    /**
     *
     * Lists all Offer models.
     * @param $id
     * @return mixed
     */
    public function actionDelete($id)
    {

        $workTime = BusinessWorkTime::findOne($id);

        if ($workTime) {
            return $workTime->delete() ? true : false;
        }

        return false;

    }

}
