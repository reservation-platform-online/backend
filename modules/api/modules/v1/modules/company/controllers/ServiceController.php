<?php

namespace app\modules\api\modules\v1\modules\company\controllers;

use app\models\business\BusinessPointWorkerService;
use app\models\galleries\Gallery;
use app\models\galleries\GalleryImage;
use app\models\galleries\GalleryService;
use app\modules\api\modules\v1\modules\company\models\business\BusinessReservation;
use app\modules\api\modules\v1\modules\company\models\business\BusinessService;
use app\modules\api\modules\v1\modules\company\models\forms\UploadFileForm;
use Yii;
use yii\console\Response;
use yii\filters\auth\HttpBearerAuth;
use yii\rest\Controller;
use yii\web\UploadedFile;

/**
 * UserController implements the CRUD actions for Offer model.
 */
class ServiceController extends Controller
{

    public function beforeAction($action)
    {

        parent::beforeAction($action);


        if (Yii::$app->getRequest()->getMethod() === 'OPTIONS') {

            Yii::$app->getResponse()->getHeaders()->set('Allow', 'POST GET PUT DELETE');

            Yii::$app->end();

        }


        return true;

    }

    public function behaviors()
    {
        $behaviors = parent::behaviors();

        $auth = $behaviors['authenticator'];

        unset($behaviors['authenticator']);

        $behaviors['corsFilter'] = [

            'class' => \yii\filters\Cors::className(),

        ];

        $behaviors['authenticator'] = $auth;

        // avoid authentication on CORS-pre-flight requests (HTTP OPTIONS method)

        $behaviors['authenticator'] = [

            'class' => HttpBearerAuth::className(),
            'only' => [
                'index', 'view', 'create', 'update', 'delete', 'connect-worker', 'disconnect-worker', 'upload-image', 'toggle-enable'
            ],
            'except' => [
                'options'
            ]

        ];

        $behaviors['client_api'] = [
            'class' => 'app\behaviors\ClientApi',
            'checkCompanyId' => true
        ];

        return $behaviors;
    }

    /**
     *
     * Lists all Offer models.
     * @param int $page
     * @param int $count
     * @param null $categoryId
     * @param null $companyId
     * @param string $orderBy
     * @param string $sort
     * @return mixed
     */
    public function actionIndex(
        $page = 1,
        $count = 5,
        $categoryId = null,
        $orderBy = 'id',
        $sort = 'desc'
    )
    {

        $companyId = Yii::$app->request->getHeaders()->get('Company-Id');

        $query = BusinessService::find()
            ->where(['business__company_id' => $companyId]);

        if (!is_null($categoryId)) {
            $query->andWhere(['category_id' => $categoryId]);
        }

        $query->orderBy($orderBy . " " . $sort);

        return [
            'total' => (integer)$query->count(),
            'models' => $query->limit($count)->offset(($page - 1) * $count)->all(),
        ];

    }

    /**
     *
     * Lists all Offer models.
     * @param $id
     * @return mixed
     */
    public function actionView($id)
    {

        return BusinessService::findOne($id);

    }

    /**
     * @param $id
     * @return array|bool|string|Response|\yii\web\Response
     */
    public function actionUploadImage($id)
    {

        $galleryService = GalleryService::findOne(['business__service_id' => $id]);

        if (!$galleryService) {

            $gallery = new Gallery();
            $gallery->name = $id;

            if (!$gallery->validate() || !$gallery->save()) {

                $response = Yii::$app->response;
                $response->setStatusCode(400);
                $response->data = $gallery->errors;
                return $response;

            }

            $galleryService = new GalleryService();
            $galleryService->gallery_id = $gallery->id;
            $galleryService->business__service_id = $id;
            $galleryService->save();

            if (!$galleryService->validate() || !$galleryService->save()) {
                $response = Yii::$app->response;
                $response->setStatusCode(400);
                $response->data = $galleryService->errors;
                return $response;
            }

        }

        $model = new UploadFileForm();
        $model->rootPath = Yii::$app->params['baseUploadsDir'] . '/services/' . $id . '/';

        $galleryImage = GalleryImage::findOne(['gallery_id' => $galleryService->gallery_id]);

        if ($galleryImage) {

            if ($galleryImage->name && file_exists($model->rootPath . $galleryImage->name)) {

                unlink($model->rootPath . $galleryImage->name);

            }

        } else {
            $galleryImage = new GalleryImage();
        }

        $galleryImage->gallery_id = $galleryService->gallery_id;

        $model->load(Yii::$app->request->post(), '');
        $model->imageFile = UploadedFile::getInstanceByName('imageFile');

        $fileUrl = $model->upload();

        $galleryImage->link = $fileUrl['url'];
        $galleryImage->name = $fileUrl['filename'];

        if ($galleryImage->validate() && $galleryImage->save()) {

            return $galleryImage->link;

        }

        $response = Yii::$app->response;
        $response->setStatusCode(400);
        $response->data = $galleryImage->errors;
        return $response;

    }

    /**
     *
     * Lists all Offer models.
     * @return mixed
     */
    public function actionCreate()
    {

        $model = new BusinessService();

        if ($model->load(\Yii::$app->request->post(), '')) {

            $model->business__company_id = Yii::$app->request->headers->get('Company-Id');

            if ($model->validate() && $model->save()) {

                return $model->id;

            }

        }

        $response = Yii::$app->response;
        $response->setStatusCode(400);
        $response->data = $model->errors;
        return $response;

    }

    /**
     *
     * Lists all Offer models.
     * @param $id
     * @return mixed
     */
    public function actionUpdate($id)
    {

        if (count(Yii::$app->request->post()) === 0) {
            $response = Yii::$app->response;
            $response->setStatusCode(400);
            $response->data = 'Data is empty';
            return $response;
        }

        $model = BusinessService::findOne(['id' => $id]);

        if ($model) {

//            $oldEnabled = $model->enabled;

            if ($model->load(Yii::$app->request->post(), '')) {

//                if ($oldEnabled === BusinessService::ENABLED && $model->enabled === BusinessService::DISABLED) {
//
//                    $reservation = BusinessReservation::find()
//                        ->where([
//                            'in', 'business__reservation.status', [
//                                BusinessReservation::STATUS_ENABLED,
//                                BusinessReservation::STATUS_CONFIRMED
//                            ]
//                        ])
//                        ->innerJoin(
//                            'business__point_worker_service',
//                            "business__point_worker_service.id = business__reservation.business__point_worker_service_id AND business__point_worker_service.business__service_id = '$model->id'"
//                        )
//                        ->one();
//
//                    if ($reservation) {
//
//                        $response = Yii::$app->response;
//                        $response->setStatusCode(400);
//                        $response->data = [
//                            'Service have reservation'
//                        ];
//                        return $response;
//
//                    }
//
//                }

                if ($model->validate()) {

                    if (intval($model->enabled) === BusinessService::ENABLED && intval($model->oldAttributes['enabled']) === BusinessService::DISABLED) {

                        if (is_null($model->hasImages())) {

                            $response = Yii::$app->response;
                            $response->setStatusCode(400);
                            $response->data = 'Service no pictures, please add the first picture';
                            return $response;

                        }

                    }

                    return $model->save();

                }

            }

        }

        $response = Yii::$app->response;
        $response->setStatusCode(400);
        $response->data = $model->errors;
        return $response;

    }

    /**
     *
     * Lists all Offer models.
     * @param $id
     * @return mixed
     */
    public function actionToggleEnable($id)
    {

        $model = BusinessService::findOne(['id' => $id]);

        if ($model) {

            $model->enabled = $model->enabled === BusinessService::ENABLED ? BusinessService::DISABLED : BusinessService::ENABLED;

            if ($model->validate()) {

                if (intval($model->enabled) === BusinessService::ENABLED && intval($model->oldAttributes['enabled']) === BusinessService::DISABLED) {

                    if (is_null($model->hasImages())) {

                        $response = Yii::$app->response;
                        $response->setStatusCode(400);
                        $response->data = 'Service no pictures, please add the first picture';
                        return $response;

                    }

                }

                return $model->save();

            }

        }

        $response = Yii::$app->response;
        $response->setStatusCode(400);
        $response->data = $model->errors;
        return $response;

    }

    /**
     *
     * Lists all Offer models.
     * @param $id
     * @return mixed
     */
    public function actionConnectWorker($id)
    {

        if (count(Yii::$app->request->post()) === 0) {
            $response = Yii::$app->response;
            $response->setStatusCode(400);
            $response->data = 'Data is empty';
            return $response;
        }

        $model = BusinessService::findOne(['id' => $id]);

        if ($model) {

            $data = Yii::$app->request->post();

            if (is_string($data['workers'])) {
                $data['workers'] = explode(',', $data['workers']);
            }

            if (array_key_exists('workers', $data)) {

                $returnArray = [];

                foreach ($data['workers'] as $key => $workerId) {

                    $pws = new BusinessPointWorkerService();
                    $pws->business__company_id = $model->business__company_id;
                    $pws->business__service_id = $model->id;
                    $pws->business__worker_id = $workerId;

                    if ($pws->validate()) {
                        $pws->save();
                        array_push($returnArray, $pws->id);
                    } else {

                        $response = Yii::$app->response;
                        $response->setStatusCode(400);
                        $response->data = $pws->errors;
                        return $response;

                    }

                }

                return $returnArray;

            }

            return false;

        }

        $response = Yii::$app->response;
        $response->setStatusCode(400);
        $response->data = $model->errors;
        return $response;

    }

    /**
     *
     * Lists all Offer models.
     * @param $id
     * @return mixed
     */
    public function actionDisconnectWorker($id)
    {

        if (count(Yii::$app->request->post()) === 0) {
            $response = Yii::$app->response;
            $response->setStatusCode(400);
            $response->data = 'Data is empty';
            return $response;
        }


        $data = Yii::$app->request->post();

        if (is_string($data['workers'])) {
            $data['workers'] = explode(',', $data['workers']);
        }

        if (array_key_exists('workers', $data)) {

            foreach ($data['workers'] as $key => $workerId) {

                $pws = BusinessPointWorkerService::findOne(['business__worker_id' => $workerId, 'business__service_id' => $id]);

                if (!$pws->delete()) {

                    $response = Yii::$app->response;
                    $response->setStatusCode(400);
                    $response->data = $pws->errors;
                    return $response;

                }

            }

            return true;

        }

    }

    /**
     *
     * Lists all Offer models.
     * @param $id
     * @return mixed
     */
    public function actionDelete($id)
    {

        // TODO write fn for set enabled to 0
        return false;

    }

}
