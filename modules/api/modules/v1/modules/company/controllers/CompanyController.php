<?php

namespace app\modules\api\modules\v1\modules\company\controllers;

use app\modules\api\modules\v1\modules\admin\models\Statistic;
use app\modules\api\modules\v1\modules\company\models\business\BusinessCompany;
use app\modules\api\modules\v1\modules\company\models\business\BusinessWorker;
use app\modules\api\modules\v1\modules\company\models\forms\UploadFileForm;
use Yii;
use yii\console\Response;
use yii\filters\auth\HttpBearerAuth;
use yii\rest\Controller;
use yii\web\UploadedFile;

/**
 * UserController implements the CRUD actions for Offer model.
 */
class CompanyController extends Controller
{

    public function beforeAction($action)
    {

        parent::beforeAction($action);


        if (Yii::$app->getRequest()->getMethod() === 'OPTIONS') {

            Yii::$app->getResponse()->getHeaders()->set('Allow', 'POST GET PUT DELETE');

            Yii::$app->end();

        }


        return true;

    }

    public function behaviors()
    {
        $behaviors = parent::behaviors();

        $auth = $behaviors['authenticator'];

        unset($behaviors['authenticator']);

        $behaviors['corsFilter'] = [

            'class' => \yii\filters\Cors::className(),

        ];

        $behaviors['authenticator'] = $auth;

        // avoid authentication on CORS-pre-flight requests (HTTP OPTIONS method)

        $behaviors['authenticator'] = [

            'class' => HttpBearerAuth::className(),
            'only' => [
                'index', 'update', 'delete', 'upload-image'
            ],
            'except' => [
                'options'
            ]

        ];

        $behaviors['client_api'] = [
            'class' => 'app\behaviors\ClientApi',
            'checkCompanyId' => true
        ];

        return $behaviors;
    }

    /**
     *
     * Lists all Offer models.
     * @return mixed
     */
    public function actionIndex()
    {

        return BusinessCompany::findOne(['id' => Yii::$app->request->getHeaders()->get('Company-Id')]);

    }

    /**
     *
     * Lists all Offer models.
     * @return mixed
     */
    public function actionUpdate()
    {

        if (count(Yii::$app->request->post()) === 0) {
            $response = Yii::$app->response;
            $response->setStatusCode(400);
            $response->data = 'Data is empty';
            return $response;
        }

        $company = BusinessCompany::findOne(['id' => Yii::$app->request->getHeaders()->get('Company-Id')]);

        if ($company) {

            $company->setScenario(BusinessCompany::SCENARIO_UPDATE);
            $company->load(Yii::$app->request->post(), '');

            if ($company->validate()) {

                return $company->save();

            }

        }

        return $company->errors;

    }

    /**
     * @param $id
     * @return array|bool|string|Response|\yii\web\Response
     */
    public function actionUploadImage()
    {

        $company = BusinessCompany::findOne(['id' => Yii::$app->request->getHeaders()->get('Company-Id')]);

        if ($company) {
            $company->setScenario(BusinessCompany::SCENARIO_UPDATE_LOGO);

            $model = new UploadFileForm();
            $model->rootPath = Yii::$app->params['baseUploadsDir'] . '/companies/' . $company->id . '/';

            if ($company->logo_link) {

                $filename = explode('/', $company->logo_link);
                $filename = $filename[count($filename) - 1];

                if ($filename && file_exists($model->rootPath . $filename)) {

                    unlink($model->rootPath . $filename);

                }

            }

            $model->load(Yii::$app->request->post(), '');
            $model->imageFile = UploadedFile::getInstanceByName('imageFile');

            $fileUrl = $model->upload();

            $company->logo_link = $fileUrl['url'];

            if ($company->validate()) {

                return $company->save();

            }

        }

        $response = Yii::$app->response;
        $response->setStatusCode(400);
        $response->data = $company->errors;
        return $response;

    }

    /**
     *
     * Lists all Offer models.
     * @return mixed
     */
    public function actionDelete()
    {

        // TODO enabled set 0
        return false;

    }

}
