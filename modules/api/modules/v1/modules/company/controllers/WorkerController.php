<?php

namespace app\modules\api\modules\v1\modules\company\controllers;

use app\models\business\BusinessPointWorkerService;
use app\models\galleries\Gallery;
use app\models\galleries\GalleryImage;
use app\models\galleries\GalleryService;
use app\modules\api\modules\v1\modules\company\models\business\BusinessWorker;
use app\modules\api\modules\v1\modules\company\models\forms\CreateWorker;
use app\modules\api\modules\v1\modules\company\models\forms\UpdateWorker;
use app\modules\api\modules\v1\modules\company\models\forms\UploadFileForm;
use Yii;
use yii\console\Response;
use yii\data\ActiveDataProvider;
use yii\filters\auth\HttpBearerAuth;
use yii\rest\Controller;
use yii\web\UploadedFile;

/**
 * UserController implements the CRUD actions for Offer model.
 */
class WorkerController extends Controller
{

    public function beforeAction($action)
    {

        parent::beforeAction($action);


        if (Yii::$app->getRequest()->getMethod() === 'OPTIONS') {

            Yii::$app->getResponse()->getHeaders()->set('Allow', 'POST GET PUT DELETE');

            Yii::$app->end();

        }


        return true;

    }

    public function behaviors()
    {
        $behaviors = parent::behaviors();

        $auth = $behaviors['authenticator'];

        unset($behaviors['authenticator']);

        $behaviors['corsFilter'] = [

            'class' => \yii\filters\Cors::className(),

        ];

        $behaviors['authenticator'] = $auth;

        // avoid authentication on CORS-pre-flight requests (HTTP OPTIONS method)

        $behaviors['authenticator'] = [

            'class' => HttpBearerAuth::className(),
            'only' => [
                'index', 'view', 'create', 'update', 'delete', 'connect-service', 'disconnect-service', 'connect-zoom', 'disconnect-zoom', 'upload-image'
            ],
            'except' => [
                'options'
            ]

        ];

        $behaviors['client_api'] = [
            'class' => 'app\behaviors\ClientApi',
            'checkCompanyId' => true
        ];

        return $behaviors;
    }

    /**
     * @param $id
     * @return array|bool|string|Response|\yii\web\Response
     */
    public function actionUploadImage($id)
    {

        $worker = BusinessWorker::findOne(['id' => $id]);

        $model = new UploadFileForm();
        $model->rootPath = Yii::$app->params['baseUploadsDir'] . '/workers/' . $id . '/';

        if ($worker->photo) {

            $filename = explode('/', $worker->photo);
            $filename = $filename[count($filename) - 1];

            if ($filename && file_exists($model->rootPath . $filename)) {

                unlink($model->rootPath . $filename);

            }

        }

        $model->load(Yii::$app->request->post(), '');
        $model->imageFile = UploadedFile::getInstanceByName('imageFile');

        $fileUrl = $model->upload();

        $worker->photo = $fileUrl['url'];

        if ($worker->validate()) {

            return $worker->save();

        }

        $response = Yii::$app->response;
        $response->setStatusCode(400);
        $response->data = $worker->errors;
        return $response;

    }

    /**
     *
     * Lists all Offer models.
     * @param int $page
     * @param int $count
     * @param string $orderBy
     * @param string $sort
     * @return mixed
     */
    public function actionIndex(
        $page = 1,
        $count = 5,
        $orderBy = 'id',
        $sort = 'desc'
    )
    {

//        return new ActiveDataProvider([
//            'query' => BusinessWorker::find(),
//        ]);

        $companyId = Yii::$app->request->getHeaders()->get('Company-Id');

        $query = BusinessWorker::find()
            ->where(['business__company_id' => $companyId]);

        $query->orderBy($orderBy . " " . $sort);

        return [
            'total' => (integer) $query->count(),
            'models' => $query->limit($count)->offset(($page - 1) * $count)->all(),
        ];

    }

    /**
     *
     * Lists all Offer models.
     * @param $id
     * @return mixed
     */
    public function actionView($id)
    {

        return BusinessWorker::findOne($id);

    }

    /**
     *
     * Lists all Offer models.
     * @param $id
     * @return mixed
     * @throws \Throwable
     */
    public function actionCreate()
    {

        $model = new CreateWorker();

        if ($model->load(\Yii::$app->request->post(), '')) {

            return $model->create();

        }

        $response = Yii::$app->response;
        $response->setStatusCode(400);
        $response->data = $model->errors;
        return $response;

    }

    /**
     *
     * Lists all Offer models.
     * @param $id
     * @return mixed
     */
    public function actionUpdate($id)
    {

        if (count(Yii::$app->request->post()) === 0) {
            $response = Yii::$app->response;
            $response->setStatusCode(400);
            $response->data = 'Data is empty';
            return $response;
        }

        $model = new UpdateWorker();
        $model->id = $id;

        if ($model->load(Yii::$app->request->post(), '')) {

            return $model->save();

        }

        $response = Yii::$app->response;
        $response->setStatusCode(400);
        $response->data = $model->errors;
        return $response;

    }

    /**
     *
     * Lists all Offer models.
     * @param $id
     * @return mixed
     */
    public function actionConnectService($id)
    {

        if (count(Yii::$app->request->post()) === 0) {
            $response = Yii::$app->response;
            $response->setStatusCode(400);
            $response->data = 'Data is empty';
            return $response;
        }

        $model = BusinessWorker::findOne(['id' => $id]);

        if ($model) {

            $data = Yii::$app->request->post();

            if (is_string($data['services'])) {
                $data['services'] = explode(',', $data['services']);
            }

            if (array_key_exists('services', $data)) {

                foreach ($data['services'] as $key => $serviceId) {

                    $pws = new BusinessPointWorkerService();
                    $pws->business__company_id = $model->business__company_id;
                    $pws->business__worker_id = $model->id;
                    $pws->business__service_id = $serviceId;

                    if ($pws->validate()) {
                        $pws->save();
                    } else {

                        $response = Yii::$app->response;
                        $response->setStatusCode(400);
                        $response->data = $pws->errors;
                        return $response;

                    }

                }

                return true;

            }

            return false;

        }

        $response = Yii::$app->response;
        $response->setStatusCode(400);
        $response->data = $model->errors;
        return $response;

    }

    /**
     *
     * Lists all Offer models.
     * @param $id
     * @return mixed
     */
    public function actionDisconnectService($id)
    {

        if (count(Yii::$app->request->post()) === 0) {
            $response = Yii::$app->response;
            $response->setStatusCode(400);
            $response->data = 'Data is empty';
            return $response;
        }


        $data = Yii::$app->request->post();

        if (is_string($data['services'])) {
            $data['services'] = explode(',', $data['services']);
        }

        if (array_key_exists('services', $data)) {

            foreach ($data['services'] as $key => $serviceId) {

                $pws = BusinessPointWorkerService::findOne(['business__service_id' => $serviceId, 'business__worker_id' => $id]);

                if (!$pws->delete()) {

                    $response = Yii::$app->response;
                    $response->setStatusCode(400);
                    $response->data = $pws->errors;
                    return $response;

                }

            }

            return true;

        }

    }

    /**
     *
     * Lists all Offer models.
     * @param $id
     * @return mixed
     */
    public function actionConnectZoom($id)
    {

        if (count(Yii::$app->request->post()) === 0) {
            $response = Yii::$app->response;
            $response->setStatusCode(400);
            $response->data = 'Data is empty';
            return $response;
        }

        $model = BusinessWorker::findOne(['id' => $id]);

        if ($model) {

            $data = Yii::$app->request->post();

            return false;

        }

        $response = Yii::$app->response;
        $response->setStatusCode(400);
        $response->data = $model->errors;
        return $response;

    }

    /**
     *
     * Lists all Offer models.
     * @param $id
     * @return mixed
     */
    public function actionDisconnectZoom($id)
    {

        if (count(Yii::$app->request->post()) === 0) {
            $response = Yii::$app->response;
            $response->setStatusCode(400);
            $response->data = 'Data is empty';
            return $response;
        }

        $model = BusinessWorker::findOne(['id' => $id]);

        if ($model) {

            $data = Yii::$app->request->post();

            return false;

        }

        $response = Yii::$app->response;
        $response->setStatusCode(400);
        $response->data = $model->errors;
        return $response;

    }

    /**
     *
     * Lists all Offer models.
     * @param $id
     * @return mixed
     */
    public function actionDelete($id)
    {

        // TODO write fn for set enabled to 0
        return false;

    }
}
