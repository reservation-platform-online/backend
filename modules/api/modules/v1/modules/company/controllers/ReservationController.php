<?php

namespace app\modules\api\modules\v1\modules\company\controllers;

use app\modules\api\modules\v1\modules\company\models\business\BusinessReservation;
use app\modules\api\modules\v1\modules\company\models\business\BusinessService;
use app\modules\api\modules\v1\modules\company\models\forms\CreateReservation;
use Yii;
use yii\filters\auth\HttpBearerAuth;
use yii\filters\Cors;
use yii\rest\Controller;

/**
 * UserController implements the CRUD actions for Offer model.
 */
class ReservationController extends Controller
{

    public function beforeAction($action)
    {

        parent::beforeAction($action);


        if (Yii::$app->getRequest()->getMethod() === 'OPTIONS') {

            Yii::$app->getResponse()->getHeaders()->set('Allow', 'POST GET PUT DELETE');

            Yii::$app->end();

        }


        return true;

    }

    public function behaviors()
    {
        $behaviors = parent::behaviors();

        $auth = $behaviors['authenticator'];

        unset($behaviors['authenticator']);

        $behaviors['corsFilter'] = [

            'class' => Cors::className(),

        ];

        $behaviors['authenticator'] = $auth;

        // avoid authentication on CORS-pre-flight requests (HTTP OPTIONS method)

        $behaviors['authenticator'] = [

            'class' => HttpBearerAuth::className(),
            'only' => [
                'index', 'view', 'create', 'update', 'delete'
            ],
            'except' => [
                'options'
            ]

        ];

        $behaviors['client_api'] = [
            'class' => 'app\behaviors\ClientApi',
            'checkCompanyId' => true
        ];

        return $behaviors;
    }

    /**
     *
     * Lists all Offer models.
     * @param string $orderBy
     * @param string $sort
     * @param null $start
     * @param null $end
     * @param null $categoryId
     * @param array $statusList
     * @return mixed
     */
    public function actionIndex(
        $orderBy = 'date, start_at',
        $sort = 'asc',
        $start = null,
        $end = null,
        $categoryId = null
    )
    {

        if (is_null($start) || is_null($end)) {

            $response = Yii::$app->response;
            $response->setStatusCode(400);
            $response->data = 'start or end is empty';
            return $response;

        }

        $reservationStatusConfirmed = BusinessReservation::STATUS_CONFIRMED;
        $servicePaymentIsImportant = BusinessService::PAYMENT_IS_IMPORTANT;
        $servicePaymentIsNotImportant = BusinessService::PAYMENT_IS_NOT_IMPORTANT;

        $statusList = Yii::$app->request->queryParams['statusList'];

        if (is_null($statusList)) {
            $statusList = [
                BusinessReservation::STATUS_ENABLED,
                BusinessReservation::STATUS_CONFIRMED,
                BusinessReservation::STATUS_FINISHED,
                BusinessReservation::STATUS_USER_WORKER_DID_NOT_COME,
            ];
        }

        if (gettype($statusList) === 'string') {
            if (!is_null(json_decode($statusList))) {
                $statusList = json_decode($statusList);
            }
        }

        if (gettype($statusList) === 'string') {
            $statusList = explode(",", $statusList);
        }


        $startDate = date("Y-m-d", strtotime($start));
        $endDate = date("Y-m-d", strtotime($end));

        $companyId = Yii::$app->request->getHeaders()->get('Company-Id');

        $query = BusinessReservation::find()
            ->where([
                'business__reservation.payment_is_important' => $servicePaymentIsImportant,
                'business__reservation.status' => $reservationStatusConfirmed,
            ])
            ->orWhere([
                'and',
                ['=', 'business__reservation.payment_is_important', $servicePaymentIsNotImportant],
                ['in', 'business__reservation.status', $statusList]
            ])
            ->andWhere([
                'and',
                ['>=', 'business__reservation.date', $startDate],
                ['<=', 'business__reservation.date', $endDate]
            ])
            ->innerJoin(
                'business__point_worker_service',
                "business__point_worker_service.id = business__reservation.business__point_worker_service_id AND business__point_worker_service.business__company_id = '$companyId'");

        if (!is_null($categoryId)) {

            $query->innerJoin("business__service", "business__service.id = business__point_worker_service.business__service_id AND business__service.category_id = '$categoryId'");

        }

        $query->orderBy($orderBy . " " . $sort);
//
//        var_dump($query->createCommand()->sql);

        return [
            'total' => (integer)$query->count(),
            'models' => $query->all(),
        ];

    }

    /**
     *
     * Lists all Offer models.
     * @param $id
     * @return mixed
     */
    public function actionView($id)
    {

        return BusinessReservation::findOne($id);

    }

    /**
     *
     * Lists all Offer models.
     * @param $id
     * @return mixed
     */
    public function actionCreate()
    {

        $createReservation = new CreateReservation();
        $createReservation->load(Yii::$app->request->post(), '');

        return $createReservation->create();

    }

    /**
     *
     * Lists all Offer models.
     * @param $id
     * @return mixed
     */
    public function actionUpdate($id)
    {

        if (count(Yii::$app->request->post()) === 0) {
            $response = Yii::$app->response;
            $response->setStatusCode(400);
            $response->data = 'Data is empty';
            return $response;
        }

        // TODO check update data, company can custom change data from edit request!!! (Наприклад фірма може зробити так що клієнт відмінив замовлення)

        $model = BusinessReservation::findOne(['id' => $id]);

        if ($model) {

            if ($model->load(Yii::$app->request->post(), '')) {

                if ($model->oldAttributes['status'] !== $model->status) {

                    $model->status = intval($model->status);

                    $checkStatus = [
                        BusinessReservation::STATUS_ENABLED,
                        BusinessReservation::STATUS_RECEPTION_CANCELED,
                        BusinessReservation::STATUS_CONFIRMED,
                        BusinessReservation::STATUS_FINISHED
                    ];

                    if (!in_array($model->status, $checkStatus)) {

                        $response = Yii::$app->response;
                        $response->setStatusCode(400);
                        $response->data = 'Status is not correct';
                        return $response;

                    }

                }

                if ($model->validate()) {

                    return $model->save();

                }

            }

        }

        $response = Yii::$app->response;
        $response->setStatusCode(400);
        $response->data = $model->errors;
        return $response;

    }

    /**
     *
     * Lists all Offer models.
     * @param $id
     * @return mixed
     */
    public function actionDelete($id)
    {

        // TODO write fn for set enabled to 0
        return false;

    }

}
