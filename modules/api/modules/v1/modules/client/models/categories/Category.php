<?php

namespace app\modules\api\modules\v1\modules\client\models\categories;

use app\models\business\BusinessService;
use app\models\categories\CategoryChild;


/**
 * This is the model class for table "{{%category}}".
 *
 * @property int $id
 * @property string $name
 * @property string $color
 * @property string|null $icon
 * @property int $number_of_children
 * @property int $number_of_services
 * @property int $enabled
 * @property int $hide
 *
 * @property BusinessService[] $businessServices
 * @property CategoryChild[] $categoryChildren
 * @property CategoryChild[] $categoryChildren0
 * @property \app\models\categories\Category[] $parents
 * @property Category[] $categories
 */
class Category extends \app\models\categories\Category
{

    public $children = [];

    public function fields() {

//            ->andWhere(['>', 'category.number_of_children', 0])
//            ->orWhere(['>', 'category.number_of_services', 0])

//        $this->children = Category::find()
//            ->where(['category.enabled' => Category::ENABLED, 'category.hide' => Category::SHOW])
//            ->innerJoin('category__child', "category__child.parent_id = $this->id AND category__child.category_id = category.id")
//            ->all();

//        $this->children = ArrayHelper::getColumn(CategoryChild::find()->select('category_id')->where([
//            'parent_id' => $this->id
//        ])->asArray()->all(), 'category_id');

        return [
            'id',
            'name',
            'color',
            'icon',
            'numberOfServices' => 'number_of_services',
            'parentId' => function () {
                $category = CategoryChild::find()->select('parent_id')->where([
                    'category_id' => $this->id
                ])->one();
                return $category->parent_id;
            }
        ];

    }

}
