<?php

namespace app\modules\api\modules\v1\modules\client\models\business;

use app\models\additional\AdditionalLocality;
use app\models\business\BusinessCompany;
use app\models\business\BusinessPointWorkerService;

/**
 * This is the model class for table "{{%business__point}}".
 *
 * @property string $id
 * @property string $business__company_id
 * @property int $additional__locality_id
 * @property string $address_line
 * @property string|null $address_line2
 * @property string $postal_index
 * @property float|null $latitude
 * @property float|null $longitude
 * @property int|null $zoom
 * @property int $status
 * @property string $created_at
 * @property string $updated_at
 *
 * @property AdditionalLocality $additionalLocality
 * @property BusinessCompany $businessCompany
 * @property BusinessPointWorkerService[] $businessPointWorkerServices
 */
class BusinessPoint extends \app\models\business\BusinessPoint
{

    public function fields() {

        return [
            'id',
            'companyId' => 'business__company_id',
            'localityId' => 'additional__locality_id',
            'addressLine' => 'address_line',
            'addressLine2' => 'address_line2',
            'postalIndex' => 'postal_index',
            'latitude',
            'longitude',
            'zoom',
            'status',
            'createdAt' => 'created_at',
            'updatedAt' => 'updated_at',
        ];

    }

}
