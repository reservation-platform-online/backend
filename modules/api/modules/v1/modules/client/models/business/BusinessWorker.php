<?php

namespace app\modules\api\modules\v1\modules\client\models\business;

use app\models\additional\AdditionalWeekend;
use app\models\business\BusinessPointWorkerService;
use app\models\User;
use app\models\zoom\ZoomAccessToken;
use app\models\zoom\ZoomAccessTokenBusinessWorker;
use yii\db\ActiveQuery;

/**
 * This is the model class for table "{{%business__worker}}".
 *
 * @property string $id
 * @property string $business__company_id
 * @property string|null $user_id
 * @property string $first_name
 * @property string $last_name
 * @property string|null $photo
 * @property string|null $position
 * @property int $number_of_comments
 * @property float $score
 * @property int $enabled
 * @property int $hide
 * @property int $status 1 - Wait for confirm 2 - Confirmed 3 - Deleting 4 - Company Delete
 * @property string $created_at
 * @property string $updated_at
 *
 * @property AdditionalWeekend[] $additionalWeekends
 * @property BusinessPointWorkerService[] $businessPointWorkerServices
 * @property BusinessWorkTime[] $businessWorkTimes
 * @property User $user
 * @property BusinessCompany $businessCompany
 */

class BusinessWorker extends \app\models\business\BusinessWorker
{

    public function fields() {

        return [
            'id',
            'firstName' => 'first_name',
            'lastName' => 'last_name',
            'numberOfComments' => 'number_of_comments',
            'score',
            'photo',
//            'userId' => 'user_id',
//            'position',
            'status',
            'company' => 'businessCompany'
        ];

    }

    /**
     * Gets query for [[BusinessCompany]].
     *
     * @return ActiveQuery
     */
    public function getBusinessCompany()
    {
        return $this->hasOne(BusinessCompany::className(), ['id' => 'business__company_id'])->andWhere([
            '!=', 'type__company_id', BusinessCompany::TYPE_COMPANY_SINGEL
        ]);
    }

}
