<?php

namespace app\modules\api\modules\v1\modules\client\models\business;

use app\models\business\BusinessPointWorkerService;
use app\models\business\BusinessReservationPayment;
use app\models\business\BusinessReservationUser;
use app\models\promoCodes\PromoCodeBusinessReservation;
use app\modules\api\modules\v1\modules\client\models\customs\business\reservation\BusinessService;
use Yii;

/**
 * This is the model class for table "{{%business__reservation}}".
 *
 * @property string $id
 * @property string $business__point_worker_service_id
 * @property string $date
 * @property string $start_at
 * @property string $end_at
 * @property int $weekday
 * @property float $cost
 * @property int $charge
 * @property string|null $meet_link
 * @property string|null $meet_password
 * @property string $interval
 * @property string|null $note
 * @property float $price
 * @property float|null $price_to
 * @property int $payment_is_important
 * @property string|null $timer_time_for_pay
 * @property int $hide
 * @property int $enabled
 * @property int $status 0 - Customer canceled 1 - Created 2 - Confirmed 3 - Finish 4 - Reception canceled 5 - Payment time out 6 - Customer: Employee did not come
 * @property string $created_at
 * @property string $updated_at
 *
 * @property BusinessPointWorkerService $businessPointWorkerService
 * @property BusinessReservationPayment[] $businessReservationPayments
 * @property BusinessReservationUser[] $businessReservationUsers
 * @property PromoCodeBusinessReservation[] $promoCodeBusinessReservations
 */
class BusinessReservation extends \app\models\business\BusinessReservation
{

    /**
     * {@inheritdoc}
     */
    public function fields()
    {
        return [
            'id',
            'date',
            'startAt' => 'start_at',
            'endAt' => 'end_at',
            'reservationUser' => function($model) {
                $businessReservationUser = BusinessReservationUser::find()->where([
                    'user_id' => Yii::$app->user->id,
                    'business__reservation_id' => $model->id
                ])->one();
                return [
                    'numberOfComments' => $businessReservationUser->number_of_comments,
                    'id' => $businessReservationUser->id
                ];
            },
            'weekday',
            'pwsId' => 'business__point_worker_service_id',
            'cost',
            'charge',
            'status',
            'note',
            'interval',
            'meetLink' => 'meet_link',
            'price',
            'paymentIsImportant' => 'payment_is_important',
            'timerTimeForPay' => 'timer_time_for_pay',
            'priceTo' => 'price_to',
            'createdAt' => 'created_at',
            'updatedAt' => 'updated_at',
            'service' => function ($model) {

                return BusinessService::find()
                    ->innerJoin('business__point_worker_service', "business__point_worker_service.id = '$model->business__point_worker_service_id' AND business__point_worker_service.business__service_id = business__service.id")
                    ->one();

            },
            'worker' => function ($model) {

                return BusinessWorker::find()
                    ->innerJoin('business__point_worker_service', "business__point_worker_service.id = '$model->business__point_worker_service_id' AND business__point_worker_service.business__worker_id = business__worker.id")
                    ->one();

            }
        ];
    }
}
