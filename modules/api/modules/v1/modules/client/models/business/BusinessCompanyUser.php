<?php

namespace app\modules\api\modules\v1\modules\client\models\business;

use app\modules\api\modules\v1\modules\client\models\User;
use yii\db\ActiveQuery;

/**
 * This is the model class for table "{{%business__company__user}}".
 *
 * @property string $id
 * @property string $business__company_id
 * @property string $user_id
 * @property int $role 1 - Owner 2 - Co-owner 3 - Reception
 * @property int $enabled
 * @property string $created_at
 * @property string $updated_at
 *
 * @property BusinessCompany $businessCompany
 * @property User $user
 */
class BusinessCompanyUser extends \app\models\business\BusinessCompanyUser
{

    /**
     * {@inheritdoc}
     */
    public function fields()
    {
        return [
            'id',
            'company' => function($model) {
                return $model->businessCompany;
            },
            'role',
            'createdAt' => 'created_at',
            'updatedAt' => 'updated_at',
        ];
    }

    /**
     * Gets query for [[BusinessCompany]].
     *
     * @return ActiveQuery
     */
    public function getBusinessCompany()
    {
        return $this->hasOne(BusinessCompany::className(), ['id' => 'business__company_id']);
    }

    /**
     * Gets query for [[User]].
     *
     * @return ActiveQuery
     */
    public function getUser()
    {
        return $this->hasOne(User::className(), ['id' => 'user_id']);
    }

}
