<?php

namespace app\modules\api\modules\v1\modules\client\models\business;

use app\models\business\BusinessCompanyUser;
use app\models\business\BusinessPoint;
use app\models\business\BusinessService;
use app\models\business\BusinessWorker;
use app\models\promoCodes\PromoCode;
use app\models\tariffs\TariffSubscribe;

/**
 * This is the model class for table "{{%business__company}}".
 *
 * @property string $id
 * @property string $name
 * @property string|null $seo_link
 * @property string|null $logo_link
 * @property string|null $description
 * @property int $enabled
 * @property int $status
 * @property string $created_at
 * @property string $updated_at
 *
 * @property BusinessCompanyUser[] $businessCompanyUsers
 * @property BusinessPoint[] $businessPoints
 * @property BusinessService[] $businessServices
 * @property BusinessWorker[] $businessWorkers
 * @property PromoCode[] $promoCodes
 * @property TariffSubscribe[] $tariffSubscribes
 */
class BusinessCompany extends \app\models\business\BusinessCompany
{

    const STATUS_ACTIVE = 2;

    public function fields() {

        return [
            'id',
            'name',
            'seoLink' => 'seo_link',
            'logoLink' => 'logo_link',
            'description',
            'typeCompanyId' => 'type__company_id',
            'status',
            'createdAt' => 'created_at',
            'updatedAt' => 'updated_at',
        ];

    }

}
