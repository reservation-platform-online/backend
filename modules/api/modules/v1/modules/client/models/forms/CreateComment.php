<?php

namespace app\modules\api\modules\v1\modules\client\models\forms;

use app\models\business\BusinessPointWorkerService;
use app\models\business\BusinessReservationUser;
use app\models\User;
use app\modules\api\modules\v1\modules\client\models\business\BusinessComment;
use app\modules\api\modules\v1\modules\client\models\business\BusinessReservation;
use app\modules\api\modules\v1\modules\client\models\business\BusinessService;
use app\modules\api\modules\v1\modules\client\models\business\BusinessWorkTime;
use PDO;
use Throwable;
use Yii;
use yii\base\Model;
use yii\db\DataReader;
use yii\web\HttpException;

/**
 * LoginForm is the model behind the login form.
 *
 * @property-read User|null $user This property is read-only.
 *
 *  0 - Customer canceled
 *  1 - Created
 *  2 - Confirmed
 *  3 - Finish
 *  4 - Reception canceled
 *  5 - Payment time out
 *  6 - Customer: Employee did not come
 *
 */
class CreateComment extends Model
{

    public $score;
    public $comment;
    public $reservationUserId;
    public $pwsId;
    public $isAnonymous;

    // TODO reservation for other person

    /**
     * @return array the validation rules.
     */
    public function rules()
    {
        return [
            [
                [
                    "score",
                    "comment",
                    "reservationUserId",
                    "pwsId",
                    "isAnonymous",
                ],
                'required'
            ],
        ];
    }

    /**
     * Logs in a user using the provided username and password.
     * @return array|false|string|DataReader
     * @throws Throwable
     */
    public function create()
    {

        if ($this->validate()) {

            $checkIfExist = BusinessComment::find()->where([
                'business__point_worker_service_id' => $this->pwsId,
                'business__reservation__user_id' => $this->reservationUserId
            ])->one();

            if (is_null($checkIfExist)) {

                $businessComment = new BusinessComment();
                $businessComment->business__point_worker_service_id = $this->pwsId;
                $businessComment->business__reservation__user_id = $this->reservationUserId;
                $businessComment->comment = $this->comment;
                $businessComment->is_anonymous = $this->isAnonymous;
                $businessComment->score = $this->score;

                if ($businessComment->validate()) {

                    return $businessComment->save();

                }

                $response = Yii::$app->response;
                $response->setStatusCode(400);
                $response->data = $businessComment->errors;
                return $response;

            } else {

                $response = Yii::$app->response;
                $response->setStatusCode(400);
                $response->data = 'Is exist';
                return $response;

            }

        }

        $response = Yii::$app->response;
        $response->setStatusCode(400);
        $response->data = $this->errors;
        return $response;

    }

}
