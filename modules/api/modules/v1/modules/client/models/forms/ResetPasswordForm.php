<?php

namespace app\modules\api\modules\v1\modules\client\models\forms;

use app\models\User;
use Throwable;
use Yii;
use yii\base\Model;
use yii\web\Response;

/**
 * LoginForm is the model behind the login form.
 *
 * @property-read User|null $user This property is read-only.
 *
 */
class ResetPasswordForm extends Model
{

    public $email;
    public $password;
    private $_user = false;

    /**
     * @return array the validation rules.
     */
    public function rules()
    {
        return [
            // username and password are both required
            [['email', 'password'], 'required'],
        ];
    }

    /**
     * Logs in a user using the provided username and password.
     * @return bool|\yii\console\Response|Response
     * @throws Throwable
     */
    public function resetPassword()
    {

        if ($this->validate()) {


            // TODO edit procedure on production db for update password (verification token to password reset token)
            // TODO rewrite this fn to corect form (delete $passwordHash = 'user__update_password()_' . Yii::$app->db->createCommand("select SHA2('$this->password',512)")->queryScalar();)

            $passwordHash = 'user__update_password()_' . Yii::$app->db->createCommand("select SHA2('$this->password',512)")->queryScalar();

            if ($this->getUser()) {

                $this->getUser()->password_hash = $passwordHash;

                if ($this->getUser()->validate() && $this->getUser()->save()) {

//                    $this->getUser()->sendEmailNewPassword($this->password);
                    return true;

                } else {

                    $response = Yii::$app->response;
                    $response->setStatusCode(400);
                    $response->data = $this->getUser()->errors;
                    return $response;

                }

            }

        }

        $response = Yii::$app->response;
        $response->setStatusCode(400);
        $response->data = $this->errors;
        return $response;
    }

    /**
     * Finds user by [[email]]
     *
     * @return User|null
     */
    public function getUser()
    {
        if ($this->_user === false) {
            $this->_user = User::findByEmail($this->email);
        }

        return $this->_user;
    }
}
