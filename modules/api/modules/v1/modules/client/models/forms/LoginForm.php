<?php

namespace app\modules\api\modules\v1\modules\client\models\forms;

use app\models\User;
use PDO;
use Throwable;
use Yii;
use yii\base\Model;
use yii\web\HttpException;

/**
 * LoginForm is the model behind the login form.
 *
 * @property-read User|null $user This property is read-only.
 *
 */
class LoginForm extends Model
{

    public $email;
    public $password;
    private $_user = false;

    /**
     * @return array the validation rules.
     */
    public function rules()
    {
        return [
            // username and password are both required
            [['email', 'password'], 'required'],
        ];
    }

    /**
     * Logs in a user using the provided username and password.
     * @return array whether the user is logged in successfully
     * @throws Throwable
     */
    public function login()
    {

        if ($this->validate()) {

            // TODO check header
            $clientId = Yii::$app->request->getHeaders()->get('Client-Id');
            $deviceUuid = Yii::$app->request->getHeaders()->get('Device-Uuid');
            $inTypeAuthorization = 1;

            $sql = "CALL authorization(:inEmail, :inPasswordBeforeHash, :inDeviceUuid, :inClientId, :inTypeAuthorization, @outAuthorizationId)";

            $command = Yii::$app->db->createCommand($sql);

            $command->bindParam(":inEmail", $this->email, PDO::PARAM_STR);
            $command->bindParam(":inPasswordBeforeHash", $this->password, PDO::PARAM_STR);
            $command->bindParam(":inClientId", $clientId, PDO::PARAM_STR);
            $command->bindParam(":inDeviceUuid", $deviceUuid, PDO::PARAM_STR);
            $command->bindParam(":inTypeAuthorization", $inTypeAuthorization, PDO::PARAM_INT); // TODO type

            try {

                $uuid = $command->queryScalar();

                if (is_bool($uuid) && !$uuid) {

                    $command->pdoStatement->execute();
                    $command->pdoStatement->nextRowset();

                } else {
                    return $uuid;
                }

            } catch (\Exception $error) {
                throw new HttpException(400, $error->errorInfo[2]);
            }

        }

        $response = Yii::$app->response;
        $response->setStatusCode(400);
        $response->data = $this->errors;
        return $response;
    }

    /**
     * Finds user by [[email]]
     *
     * @return User|null
     */
    public function getUser()
    {
        if ($this->_user === false) {
            $this->_user = User::findByEmail($this->email);
        }

        return $this->_user;
    }
}
