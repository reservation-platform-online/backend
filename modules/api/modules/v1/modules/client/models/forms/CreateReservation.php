<?php

namespace app\modules\api\modules\v1\modules\client\models\forms;

use app\models\business\BusinessPointWorkerService;
use app\models\business\BusinessReservationUser;
use app\models\User;
use app\modules\api\modules\v1\modules\client\models\business\BusinessReservation;
use app\modules\api\modules\v1\modules\client\models\business\BusinessService;
use app\modules\api\modules\v1\modules\client\models\business\BusinessWorkTime;
use Throwable;
use Yii;
use yii\base\Model;
use yii\db\DataReader;

/**
 * LoginForm is the model behind the login form.
 *
 * @property-read User|null $user This property is read-only.
 *
 *  0 - Customer canceled
 *  1 - Created
 *  2 - Confirmed
 *  3 - Finish
 *  4 - Reception canceled
 *  5 - Payment time out
 *  6 - Customer: Employee did not come
 *
 */
class CreateReservation extends Model
{

    const STATUS_CUSTOMER_CANCELED = 0;
    const STATUS_CREATED = 1;
    const STATUS_CONFIRMED = 2;
    const STATUS_FINISH = 3;
    const STATUS_RECEPTION_CANCELED = 4;
    const STATUS_PAYMENT_TIME_OUT = 5;
    const STATUS_EMPLOYEE_DID_NOT_COME = 6;

    public $date;
    public $start;
    public $end;
    public $pwsId;
    public $note;

    // TODO reservation for other person

    /**
     * @return array the validation rules.
     */
    public function rules()
    {
        return [
            // username and password are both required
            [['date', 'start', 'end', 'pwsId'], 'required'],
            [['note'], 'string'],
            [['date'], 'date', 'format' => 'php:Y-m-d'],
            ['date', function ($attribute, $params, $validator) {
                if (!(strtotime($this->$attribute) >= strtotime(date('Y-m-d')))) {
                    $this->addError($attribute, 'Date must be today or letter');
                }
            }],
            ['pwsId', 'exist', 'targetClass' => BusinessPointWorkerService::class, 'targetAttribute' => ['pwsId' => 'id']],
            ['pwsId', function ($attribute, $params, $validator) {
                $reservation = BusinessReservation::find()->where([
                    'business__point_worker_service_id' => $this->pwsId,
                    'date' => $this->date,
                    'start_at' => $this->start
                ])->andWhere(['in', 'status', [self::STATUS_CREATED, self::STATUS_CONFIRMED, self::STATUS_FINISH]])->one();
                if ($reservation) {
                    $this->addError($attribute, 'Reservation is exist');
                }
            }],
        ];
    }

    /**
     * Logs in a user using the provided username and password.
     * @return array|false|string|DataReader
     * @throws Throwable
     */
    public function create()
    {

        if ($this->validate()) {

            // TODO check if cutt time is not after start_at

            $weekday = date("N", strtotime($this->date));
            // TODO check interval (end - start = [result] === interval)

            $workerWorkTime = BusinessWorkTime::find()
            ->where([
                'weekday' => $weekday,
                'business__point_worker_service_id' => $this->pwsId
            ])->andWhere([
                '<=', 'start_at', $this->start
            ])->andWhere([
                '>=', 'end_at', $this->end // TODO check if is correct
            ]);

            if (date("Y-m-d", strtotime($this->date)) === date("Y-m-d")) {

                $workerWorkTime->andWhere([
                    '>=', 'end_at', date("H:i:s")
                ])->andWhere([
                    '<=', 'start_at', date("H:i:s")
                ]);

            }

            // TODO create zoom meeting

            $workerWorkTime = $workerWorkTime->one();

            if (is_null($workerWorkTime)) {

                $response = Yii::$app->response;
                $response->setStatusCode(400);
                $response->data = [
                    'error' => 'Start or end is not correct'
                ];
                return $response;

            }

            $service = BusinessService::find()
                ->innerJoin('business__point_worker_service', "business__point_worker_service.id = '$this->pwsId' AND business__point_worker_service.business__service_id = business__service.id")
                ->one();

            $reservation = new BusinessReservation();
            $reservation->business__point_worker_service_id = $this->pwsId;
            $reservation->date = $this->date;
            $reservation->start_at = $this->start;
            $reservation->end_at = $this->end;
            $reservation->note = $this->note;
            $reservation->interval = $service->interval;
            $reservation->price = $service->price;
            $reservation->timer_time_for_pay = $service->timer_time_for_pay;
            $reservation->payment_is_important = $service->payment_is_important;
            $reservation->price_to = $service->price_to;
            $reservation->weekday = $weekday;

            if ($reservation->validate() && $reservation->save()) {

                $reservationUser = new BusinessReservationUser();
                $reservationUser->user_id = Yii::$app->user->id;
                $reservationUser->business__reservation_id = $reservation->getPrimaryKey();

                if ($reservationUser->validate() && $reservationUser->save()) {

                    return $reservation->id;

                } else {

                    $response = Yii::$app->response;
                    $response->setStatusCode(400);
                    $response->data = $reservationUser->errors;
                    return $response;

                }

            } else {

                $response = Yii::$app->response;
                $response->setStatusCode(400);
                $response->data = $reservation->errors;
                return $response;

            }

        }

        $response = Yii::$app->response;
        $response->setStatusCode(400);
        $response->data = $this->errors;
        return $response;

    }

}
