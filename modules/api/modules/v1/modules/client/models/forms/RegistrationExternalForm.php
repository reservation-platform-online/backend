<?php

namespace app\modules\api\modules\v1\modules\client\models\forms;

use app\models\User;
use PDO;
use Throwable;
use Yii;
use yii\base\Model;
use yii\console\Response;
use yii\web\HttpException;

/**
 * LoginForm is the model behind the login form.
 *
 * @property-read User|null $user This property is read-only.
 *
 */
class RegistrationExternalForm extends Model
{

    const EXTERNAL_EMAIL_IS_VERIFIED = 1;

    public $email;
    public $emailIsVerified;
    public $phoneNumber;
    public $picUrl;
    public $firstName;
    public $lastName;

    private $_user = false;

    /**
     * @return array the validation rules.
     */
    public function rules()
    {
        return [
            // username and password are both required
            [['email'], 'required'],
            [['emailIsVerified'], 'number'],
        ];
    }

    /**
     * Logs in a user using the provided username and password.
     * @return User|array|Response|\yii\web\Response
     * @throws Throwable
     */
    public function registration()
    {

        if ($this->validate()) {

            $user = null;

            $registrationForm = new RegistrationForm();

            $registrationForm->setScenario(RegistrationForm::SCENARIO_GENERATE_PASSWORD);

            $registrationForm->email = $this->email;
            $registrationForm->phone = $this->phoneNumber;
            $registrationForm->firstName = $this->firstName;
            $registrationForm->lastName = $this->lastName;

            if ($registrationForm->registration()) {

                $user = $this->getUser();

            }

            if ($user) {

                if (
                    is_null($user->photo) &&
                    !is_null($this->picUrl)
                ) {

                    $user->photo = $this->picUrl;

                }

                if (
                    $this->emailIsVerified === self::EXTERNAL_EMAIL_IS_VERIFIED &&
                    $user->status === User::STATUS_ACTIVE
                ) {

                    $user->status = User::STATUS_CONFIRMED_EMAIL;

                }

                $user->save();

                return [
                    'type' => 'user',
                    'password' => $registrationForm->password,
                    'data' => $user,
                    'error' => null
                ];

            }

            $response = Yii::$app->response;
            $response->setStatusCode(400);
            $response->data = $registrationForm->errors;

            return [
                'type' => 'error',
                'password' => null,
                'data' => null,
                'error' => $response
            ];

        }

        $response = Yii::$app->response;
        $response->setStatusCode(400);
        $response->data = $this->errors;

        return [
            'type' => 'error',
            'password' => null,
            'data' => null,
            'error' => $response
        ];

    }

    /**
     * Finds user by [[email]]
     *
     * @return User|null
     */
    public function getUser()
    {
        if ($this->_user === false) {
            $this->_user = User::findByEmail($this->email);
        }

        return $this->_user;
    }
}
