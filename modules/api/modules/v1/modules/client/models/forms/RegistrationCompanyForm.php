<?php

namespace app\modules\api\modules\v1\modules\client\models\forms;

use app\models\business\BusinessCompany;
use app\models\business\BusinessCompanyUser;
use app\models\User;
use Exception;
use Throwable;
use Yii;
use yii\base\Model;
use yii\db\DataReader;
use yii\web\HttpException;

/**
 * LoginForm is the model behind the login form.
 *
 * @property-read User|null $user This property is read-only.
 *
 */
class RegistrationCompanyForm extends Model
{

    public $name;
    public $website;
    public $firstName;
    public $lastName;
    public $email;
    public $password;
    public $password2;
    public $typeCompanyId;

    public $agreements;

    private $_user = false;
    private $phone = null;
    private $birthday = null;

    const SCENARIO_CREATE = 'create';
    const SCENARIO_REGISTER_AND_CREATE = 'register_and_create';

    public function scenarios()
    {
        return [
            self::SCENARIO_CREATE => ['name', 'typeCompanyId'],
            self::SCENARIO_REGISTER_AND_CREATE => ['email', 'password', 'password2', 'firstName', 'lastName', 'name', 'typeCompanyId'],
        ];
    }

    /**
     * @return array the validation rules.
     */
    public function rules()
    {
        return [
            // username, email and password are all required in "register" scenario
            [['name', 'typeCompanyId'], 'required', 'on' => self::SCENARIO_CREATE],

            // username and password are required in "login" scenario
            [['email', 'password', 'password2', 'firstName', 'lastName', 'name', 'typeCompanyId'], 'required', 'on' => self::SCENARIO_REGISTER_AND_CREATE],

            [['email', 'password', 'password2', 'firstName', 'lastName', 'name'], 'required'],
            [['firstName', 'lastName', 'email', 'password', 'password2'], 'string'],
            [['typeCompanyId'], 'integer'],
            ['email', 'email'],
            ['email', 'unique', 'targetClass' => '\app\models\User', 'message' => 'This email address has already been taken.'],
            ['password2', 'compare', 'compareAttribute' => 'password', 'message' => "Passwords don't match" ],
        ];
    }

    /**
     * Logs in a user using the provided username and password.
     * @return array|false|string|DataReader
     * @throws HttpException
     */
    public function registration()
    {

        $this->typeCompanyId = intval($this->typeCompanyId);

        if ($this->validate()) {

            $connection = Yii::$app->db;

            $transaction = $connection->beginTransaction();
            try {

                $return = [];

                switch ($this->getScenario()) {

                    case self::SCENARIO_REGISTER_AND_CREATE:

                        $userId = $this->createUser();
                        $companyId = $this->createCompany($userId);

                        $return = [
                            'userId' => $userId,
                            'companyId' => $companyId,
                        ];
                        break;

                    case self::SCENARIO_CREATE:

                        $companyId = $this->createCompany(Yii::$app->user->id);
                        $return = [
                            'companyId' => $companyId,
                        ];
                        break;

                }

                $transaction->commit();

                return $return;

            } catch (Exception $e) {

                $transaction->rollBack();

                $response = Yii::$app->response;
                $response->setStatusCode(400);
                $response->data = $e;
                return $response;

            } catch (Throwable $e) {

                $transaction->rollBack();

                $response = Yii::$app->response;
                $response->setStatusCode(400);
                $response->data = $e;
                return $response;

            }

        }

        $response = Yii::$app->response;
        $response->setStatusCode(400);
        $response->data = $this->errors;
        return $response;
    }

    /**
     * Finds user by [[email]]
     *
     * @return User|null
     */
    public function getUser()
    {
        if ($this->_user === false) {
            $this->_user = User::findByEmail($this->email);
        }

        return $this->_user;
    }

    private function createUser()
    {

        $registration = new RegistrationForm();
        $registration->setScenario(RegistrationForm::SCENARIO_STANDARD);
        $registration->load($this->getAttributes(), '');

        if ($registration->validate()) {
            return $registration->registration();
        }

        throw new HttpException(400, $registration->errors);

    }

    private function createCompany($userId)
    {

        $company = new BusinessCompany();
        $company->name = $this->name;
        $company->type__company_id = $this->typeCompanyId;

        if ($company->validate() && $company->save()) {

            $companyUser = new BusinessCompanyUser();
            $companyUser->user_id = $userId;
            $companyUser->business__company_id = $company->id;
            $companyUser->role = BusinessCompanyUser::ROLE_OWNER;

//            if ($this->typeCompanyId === BusinessCompany::TYPE_COMPANY_SINGEL) {
//
//                $worker = new BusinessWorker();
//                $worker->user_id = $userId;
//                $worker->business__company_id = $company->id;
//                $worker->first_name = $this->firstName;
//                $worker->last_name = $this->lastName;
//
//                $response = Yii::$app->response;
//                $response->setStatusCode(400);
//                $response->data = $worker->getAttributes();
//                return $response;
//
//                if (!$worker->validate() || !$worker->save()) {
//
//                    throw new HttpException(400, $worker->errors);
//
//                }
//
//            }

            if ($companyUser->validate() && $companyUser->save()) {

//                $this->sendEmailCreateCompany($company);

                return $company->id;

            } else {

                throw new HttpException(400, $company->errors);

            }

        } else {

            throw new HttpException(400, $company->errors);

        }

    }

    protected function _sendEmail($email, $subject, $options) {

        $emailSend = Yii::$app->mailer->compose(['html' =>  $options['html']], $options['variables'])
            ->setFrom([Yii::$app->params['infoEmail'] => Yii::$app->params['nameOfInfoEmail']])
            ->setTo($email)
            ->setSubject($subject);

        return $emailSend->send();
    }

    private function sendEmailCreateCompany($company)
    {

        if ($company) {

            $EMAIL_TEXT = "Cześć, rejestracja '$company->name' wykonano pomyślnie, możesz już dodawać pracowników oraz usługi, oglądać kalendarz i rządzić rezerwacjami i pozostałymi możliwościami w panelu.";
            $EMAIL_TEXT .= "<p>Link do panelu: <a href='https://reservation-platform-online/panel/company/$company->id'>https://reservation-platform-online/#/panel/company/$company->id</a> </p>";
            $EMAIL_SUBJECT = $company->name . ': Rejestracja wykonano pomyślnie.';


            $this->_sendEmail($this->email, $EMAIL_SUBJECT, [
                'html' => '@app/mail/layouts/html',
                'variables' => [
                    'content' => $EMAIL_TEXT,
                ]
            ]);
        }

    }

}
