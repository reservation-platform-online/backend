<?php

namespace app\modules\api\modules\v1\modules\client\models\forms;

use app\models\agreements\AgreementUser;
use app\models\User;
use Exception;
use PDO;
use Yii;
use yii\base\Model;
use yii\db\DataReader;
use yii\db\Exception as ExceptionAlias;
use yii\web\HttpException;

/**
 * LoginForm is the model behind the login form.
 *
 * @property-read User|null $user This property is read-only.
 *
 */
class RegistrationForm extends Model
{

    public $firstName;
    public $lastName;
    public $email;
    public $password;
    public $password2;
    public $phone;
    public $birthday;
    public $agreements;

    private $_user = false;

    const SCENARIO_STANDARD = 'standard';
    const SCENARIO_GENERATE_PASSWORD = 'generate_password';

    public function scenarios()
    {
        return [
            self::SCENARIO_STANDARD => ['email', 'password', 'password2', 'firstName', 'lastName', 'phone', 'birthday', 'agreements'],
            self::SCENARIO_GENERATE_PASSWORD => ['email', 'password', 'password2', 'firstName', 'lastName', 'phone', 'birthday'],
        ];
    }

    /**
     * @return array the validation rules.
     */
    public function rules()
    {
        return [
            // username, email and password are all required in "register" scenario
            [['email', 'password', 'password2', 'firstName', 'lastName', 'agreements'], 'required', 'on' => self::SCENARIO_STANDARD],

            // username and password are required in "login" scenario
            [['email', 'password', 'password2', 'firstName', 'lastName'], 'required', 'on' => self::SCENARIO_GENERATE_PASSWORD],

            [['firstName', 'lastName', 'email', 'password', 'password2', 'phone', 'agreements'], 'string'],
            ['email', 'email'],
            ['email', 'unique', 'targetClass' => '\app\models\User', 'message' => 'This email address has already been taken.'],
            [['birthday'], 'safe'],
            [['birthday'], 'date', 'format' => 'php:Y-m-d'],
            ['password2', 'compare', 'compareAttribute' => 'password', 'message' => "Passwords don't match"],
        ];
    }

    /**
     * Logs in a user using the provided username and password.
     * @return array|false|string|DataReader
     * @throws ExceptionAlias|HttpException
     */
    public function registration()
    {

        switch ($this->getScenario()) {

            case self::SCENARIO_GENERATE_PASSWORD:

                $this->password = uniqid();
                $this->password2 = $this->password;

                break;

        }

        if ($this->validate()) {


            $clientId = Yii::$app->request->getHeaders()->get('Client-Id');
            $deviceUuid = Yii::$app->request->getHeaders()->get('Device-Uuid');

            $sql = "CALL user__insert(:firstName, :lastName, :email, :passwordBeforeHash, :phone, :birthday, :inDeviceUuid, :inClientId, @userId);";

            $command = Yii::$app->db->createCommand($sql);

            $command->bindParam(":firstName", $this->firstName, PDO::PARAM_STR);
            $command->bindParam(":lastName", $this->lastName, PDO::PARAM_STR);
            $command->bindParam(":email", $this->email, PDO::PARAM_STR);
            $command->bindParam(":passwordBeforeHash", $this->password, PDO::PARAM_STR);
            $command->bindParam(":phone", $this->phone, PDO::PARAM_STR);
            $command->bindParam(":birthday", $this->birthday, PDO::PARAM_STR);
            $command->bindParam(":inDeviceUuid", $deviceUuid, PDO::PARAM_STR);
            $command->bindParam(":inClientId", $clientId, PDO::PARAM_STR);


            try {

                $uuid = $command->queryScalar();

                if (is_bool($uuid) && !$uuid) {

                    $command->pdoStatement->execute();
                    $command->pdoStatement->nextRowset();

                } else {

                    switch ($this->getScenario()) {

                        case self::SCENARIO_GENERATE_PASSWORD:

//                            $this->getUser()->sendEmail($this->password);

                            break;

                        case self::SCENARIO_STANDARD:

                            $this->createAgreementsUser($uuid);

//                            $this->getUser()->sendEmail();

                            break;

                    }


                    return $uuid;

                }

            } catch (Exception $error) {
                throw new HttpException(400, $error->errorInfo[2]);
            }

            return true;

        }

        $response = Yii::$app->response;
        $response->setStatusCode(400);
        $response->data = $this->errors;
        return $response;
    }

    /**
     * Finds user by [[email]]
     *
     * @return User|null
     */
    public function getUser()
    {
        if ($this->_user === false) {
            $this->_user = User::findByEmail($this->email);
        }

        return $this->_user;
    }

    private function createAgreementsUser($uuid)
    {
        foreach (json_decode($this->agreements) as $key => $agreement) {
            $agreementUser = new AgreementUser();
            $agreementUser->user_id = $uuid;
            $agreementUser->agreement_id = $agreement->id;
            $agreementUser->answer = $agreement->checked;

            if ($agreementUser->validate()) {
                $agreementUser->save();
            }
        }
    }
}
