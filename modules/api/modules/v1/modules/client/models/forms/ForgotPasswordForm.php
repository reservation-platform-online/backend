<?php

namespace app\modules\api\modules\v1\modules\client\models\forms;

use app\models\User;
use PDO;
use Throwable;
use Yii;
use yii\base\Model;
use yii\db\DataReader;
use yii\web\HttpException;

/**
 * LoginForm is the model behind the login form.
 *
 * @property-read User|null $user This property is read-only.
 *
 */
class ForgotPasswordForm extends Model
{

    public $email;
    private $_user = false;

    /**
     * @return array the validation rules.
     */
    public function rules()
    {
        return [
            // username and password are both required
            [['email'], 'required'],
            ['email', 'email']
        ];
    }

    /**
     * Logs in a user using the provided username and password.
     * @return array|false|string|DataReader
     * @throws Throwable
     */
    public function forgotPassword()
    {

        if ($this->validate()) {

            $sql = "CALL authorization(:email, @token)";

            $command = Yii::$app->db->createCommand($sql);

            $command->bindParam(":email", $this->email, PDO::PARAM_STR);

            try {

                $uuid = $command->queryScalar();

                if (is_bool($uuid) && !$uuid) {

                    $command->pdoStatement->execute();
                    $command->pdoStatement->nextRowset();

                } else {
                    return $uuid;
                }

            } catch (\Exception $error) {
                throw new HttpException(400, $error->errorInfo[2]);
            }

        }

        $response = Yii::$app->response;
        $response->setStatusCode(400);
        $response->data = $this->errors;
        return $response;
    }

    /**
     * Finds user by [[email]]
     *
     * @return User|null
     */
    public function getUser()
    {
        if ($this->_user === false) {
            $this->_user = User::findByEmail($this->email);
        }

        return $this->_user;
    }
}
