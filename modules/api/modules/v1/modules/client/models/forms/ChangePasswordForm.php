<?php

namespace app\modules\api\modules\v1\modules\client\models\forms;

use app\models\User;
use Exception;
use PDO;
use Throwable;
use Yii;
use yii\base\Model;
use yii\db\DataReader;
use yii\web\HttpException;

/**
 * LoginForm is the model behind the login form.
 *
 * @property-read User|null $user This property is read-only.
 *
 */
class ChangePasswordForm extends Model
{

    const VIA_OLD_PASSWORD = 'old_password';
    const VIA_VERIFICATION_TOKEN = 'verification_token';

    public $email;
    public $verificationToken;
    public $password;
    public $newPassword;
    public $newPasswordRepeat;

    /**
     * @return array the validation rules.
     */
    public function rules()
    {
        return [
            // username and password are required in "VIA_OLD_PASSWORD" scenario
            [['password', 'newPassword', 'newPasswordRepeat'], 'required', 'on' => self::VIA_OLD_PASSWORD],

            // username, email and password are all required in "VIA_VERIFICATION_TOKEN" scenario
            [['email', 'verificationToken', 'newPassword', 'newPasswordRepeat'], 'required', 'on' => self::VIA_VERIFICATION_TOKEN],

            ['email', 'email']
        ];
    }

    public function scenarios()
    {
        return [
            self::VIA_OLD_PASSWORD => ['password', 'newPassword', 'newPasswordRepeat'],
            self::VIA_VERIFICATION_TOKEN => ['verificationToken', 'newPassword', 'newPasswordRepeat', 'email'],
        ];
    }

    /**
     * Logs in a user using the provided username and password.
     * @return array|false|string|DataReader
     * @throws Throwable
     */
    public function changePassword()
    {

        if ($this->validate()) {

            $sql = "CALL user__update_password(:email, :password, :newPassword, :newPasswordRepeat, :verificationToken)";

            $command = Yii::$app->db->createCommand($sql);

            $command->bindParam(":email", $this->email, PDO::PARAM_STR);
            $command->bindParam(":password", $this->password, PDO::PARAM_STR);
            $command->bindParam(":newPassword", $this->newPassword, PDO::PARAM_STR);
            $command->bindParam(":newPasswordRepeat", $this->newPasswordRepeat, PDO::PARAM_STR);
            $command->bindParam(":verificationToken", $this->verificationToken, PDO::PARAM_STR);

            try {

                $uuid = $command->queryScalar();

                if (is_bool($uuid) && !$uuid) {

                    $command->pdoStatement->execute();
                    $command->pdoStatement->nextRowset();

                } else {
                    return $uuid;
                }

            } catch (Exception $error) {
                throw new HttpException(400, $error->errorInfo[2]);
            }

        }

        $response = Yii::$app->response;
        $response->setStatusCode(400);
        $response->data = $this->errors;
        return $response;
    }
}
