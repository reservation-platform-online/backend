<?php

namespace app\modules\api\modules\v1\modules\client\models\customs;

use app\models\additional\AdditionalLike;
use app\models\business\BusinessPoint;
use app\models\business\BusinessPointWorkerService;
use app\models\business\BusinessPointWorkerServiceTypeService;
use app\models\business\BusinessService;
use app\modules\api\modules\v1\modules\client\models\additional\AdditionalWeekend;
use app\modules\api\modules\v1\modules\client\models\business\BusinessReservation;
use app\modules\api\modules\v1\modules\client\models\business\BusinessWorker;
use app\modules\api\modules\v1\modules\client\models\business\BusinessWorkTime;
use yii\db\ActiveQuery;

/**
 * This is the model class for table "{{%business__point_worker_service}}".
 *
 * @property string $id
 * @property string $business__service_id
 * @property string $business__worker_id
 * @property string|null $business__point_id NULL when service is online
 * @property float $price
 * @property float|null $price_to
 * @property int $enabled
 * @property int $hide
 * @property int $status
 * @property string $created_at
 * @property string $updated_at
 *
 * @property AdditionalLike[] $additionalLikes
 * @property BusinessPoint $businessPoint
 * @property BusinessService $businessService
 * @property BusinessWorker $businessWorker
 * @property BusinessPointWorkerServiceTypeService[] $businessPointWorkerServiceTypeServices
 * @property BusinessReservation[] $businessReservations
 */
class _Pws extends BusinessPointWorkerService
{

    public function fields() {

        return [
            'id',
            'serviceId' => 'business__service_id',
//            'worker' => 'businessWorker',
            'pointId' => 'business__point_id',
            'price',
            'priceTo' => 'price_to',
            'status',
            'workTime' => function($model) {
                return BusinessWorkTime::find()->where([
                    'business__point_worker_service_id' => $model->id
                ])->all();
            },
            'reservations' => function($model) {
                return BusinessReservation::find()->where([
                    'business__point_worker_service_id' => $model->id
                ])->andWhere(['>=', 'date', \Yii::$app->params['client']['pws']['startAt']])->andWhere(['<=', 'date', \Yii::$app->params['client']['pws']['endAt']])->all();
            },
//            'weekend' => function($model) {
//                return AdditionalWeekend::find()->where([
//                    'business__worker_id' => $model->business__worker_id
//                ])->andWhere(['>=', 'start_at', '2020-12-21'])->andWhere(['<=', 'end_at', '2020-12-27'])->all();
//            }
        ];

    }

    /**
     * Gets query for [[BusinessWorker]].
     *
     * @return ActiveQuery
     */
//    public function getBusinessWorker()
//    {
//        return $this->hasOne(BusinessWorker::className(), ['id' => 'business__worker_id']);
//    }

}
