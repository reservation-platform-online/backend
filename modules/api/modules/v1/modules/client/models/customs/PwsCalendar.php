<?php

namespace app\modules\api\modules\v1\modules\client\models\customs;

use app\models\additional\AdditionalLike;
use app\models\business\BusinessCounterPerSlot;
use app\models\business\BusinessPoint;
use app\models\business\BusinessPointWorkerService;
use app\models\business\BusinessPointWorkerServiceTypeService;
use app\modules\api\modules\v1\modules\client\models\business\BusinessReservation;
use app\modules\api\modules\v1\modules\client\models\business\BusinessService;
use app\modules\api\modules\v1\modules\client\models\business\BusinessWorker;
use app\modules\api\modules\v1\modules\client\models\business\BusinessWorkTime;
use yii\helpers\ArrayHelper;

/**
 * This is the model class for table "{{%business__point_worker_service}}".
 *
 * @property string $id
 * @property string $business__service_id
 * @property string $business__worker_id
 * @property string|null $business__point_id NULL when service is online
 * @property float $price
 * @property float|null $price_to
 * @property int $enabled
 * @property int $hide
 * @property int $status
 * @property string $created_at
 * @property string $updated_at
 *
 * @property AdditionalLike[] $additionalLikes
 * @property BusinessPoint $businessPoint
 * @property BusinessService $businessService
 * @property BusinessWorker $businessWorker
 * @property BusinessPointWorkerServiceTypeService[] $businessPointWorkerServiceTypeServices
 * @property BusinessReservation[] $businessReservations
 */
class PwsCalendar extends BusinessPointWorkerService
{

    public function fields()
    {

        return [
            'disabled' => function ($model) {

                $reservations = BusinessCounterPerSlot::find()
                    ->where([
                        'business__counter_per_slot.count' => $model->limit_reservation_per_slot
                    ])
                    ->andWhere(['>=', 'date', \Yii::$app->params['client']['pws']['startAt']])
                    ->andWhere(['<=', 'date', \Yii::$app->params['client']['pws']['endAt']])
                    ->innerJoin('business__work_time',
                        "business__work_time.business__point_worker_service_id = '$model->id'
                        AND business__work_time.business__worker_id = business__counter_per_slot.business__worker_id 
                        AND business__work_time.weekday = business__counter_per_slot.weekday")
                    ->all();

                return ArrayHelper::toArray($reservations, [
                    'app\models\business\BusinessCounterPerSlot' => [
                        'date',
                        'startAt' => 'start_at',
                        'endAt' => 'end_at',
                        'weekday',
                        'count',
                    ],
                ]);
            },

        ];

    }

}
