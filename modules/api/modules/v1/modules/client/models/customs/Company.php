<?php

namespace app\modules\api\modules\v1\modules\client\models\customs;

use app\models\additional\AdditionalLike;
use app\models\business\BusinessCompany;
use app\models\business\BusinessPoint;
use app\models\business\BusinessPointWorkerServiceTypeService;
use app\models\business\BusinessService;
use app\models\business\BusinessWorker;
use app\modules\api\modules\v1\modules\client\models\business\BusinessReservation;

/**
 * This is the model class for table "{{%business__point_worker_service}}".
 *
 * @property string $id
 * @property string $business__service_id
 * @property string $business__worker_id
 * @property string|null $business__point_id NULL when service is online
 * @property float $price
 * @property float|null $price_to
 * @property int $enabled
 * @property int $hide
 * @property int $status
 * @property string $created_at
 * @property string $updated_at
 *
 * @property AdditionalLike[] $additionalLikes
 * @property BusinessPoint $businessPoint
 * @property BusinessService $businessService
 * @property BusinessWorker $businessWorker
 * @property BusinessPointWorkerServiceTypeService[] $businessPointWorkerServiceTypeServices
 * @property BusinessReservation[] $businessReservations
 */
class Company extends BusinessCompany
{

    public function fields() {

        return [
            'id',
            'name',
            'seoLink' => 'seo_link',
            'logoLink' => 'logo_link',
            'description',
            'price' => function($model) {
                return [
                    'from' => 100.00,
                    'to' => 250.00
                ];
            },
            'firstOpenSlot' => function($model) {
                return true;
            }
        ];

    }

}
