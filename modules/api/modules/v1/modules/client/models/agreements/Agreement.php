<?php

namespace app\modules\api\modules\v1\modules\client\models\agreements;

use app\models\agreements\AgreementUser;

/**
 * This is the model class for table "{{%agreement}}".
 *
 * @property int $id
 * @property string $title
 * @property string|null $description
 * @property int $is_required
 * @property int $enabled
 * @property int $hide
 *
 * @property AgreementUser[] $agreementUsers
 */
class Agreement extends \app\models\agreements\Agreement
{

    public function fields()
    {

        return [
            'id',
            'title',
            'checked' => function () {
                return 0;
            },
            'description',
            'isRequired' => 'is_required'
        ];

    }

}
