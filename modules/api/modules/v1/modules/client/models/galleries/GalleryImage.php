<?php
namespace app\modules\api\modules\v1\modules\client\models\galleries;

use app\models\galleries\Gallery;

/**
 * This is the model class for table "{{%gallery__image}}".
 *
 * @property string $id
 * @property string $gallery_id
 * @property string $link
 * @property string|null $name
 * @property int $enabled
 * @property int $order
 * @property int $hide
 * @property string $created_at
 * @property string $updated_at
 *
 * @property Gallery $gallery
 */
class GalleryImage extends \app\models\galleries\GalleryImage
{

    /**
     * {@inheritdoc}
     */
    public function fields()
    {
        return [
            'id',
            'galleryId' => 'gallery_id',
            'link',
            'order',
            'name',
        ];
    }

}
