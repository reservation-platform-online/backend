<?php

namespace app\modules\api\modules\v1\modules\client\models\authorizations;

use app\models\authorizations\AuthorizationExternal;

/**
 * This is the model class for table "{{%authorization__platform}}".
 *
 * @property string $id
 * @property string $name
 * @property string $client_id
 * @property string $client_secret
 * @property string|null $app_key
 * @property string|null $public_key
 * @property string|null $redirect_url
 * @property int $enabled
 *
 * @property AuthorizationExternal[] $authorizationExternals
 */
class AuthorizationPlatform extends \app\models\authorizations\AuthorizationPlatform
{

    /**
     * {@inheritdoc}
     */
    public function fields()
    {
        return [
            'id',
            'name',
            'isDev' => 'is_dev',
            'clientId' => 'client_id',
            'publicKey' => 'public_key',
            'redirectUrl' => 'redirect_url',
        ];
    }

}
