<?php

namespace app\modules\api\modules\v1\modules\client\models\additional;

use app\models\additional\AdditionalHoliday;
use app\modules\api\modules\v1\modules\client\models\business\BusinessWorker;

/**
 * This is the model class for table "{{%additional__weekend}}".
 *
 * @property string $id
 * @property int $additional__holiday_id
 * @property string $business__worker_id
 * @property string $start_at
 * @property string $end_at
 * @property int $enabled
 * @property string $created_at
 * @property string $updated_at
 *
 * @property AdditionalHoliday $additionalHoliday
 * @property BusinessWorker $businessWorker
 */
class AdditionalWeekend extends \app\models\additional\AdditionalWeekend
{

    /**
     * {@inheritdoc}
     */
    public function fields()
    {
        return [
            'startAt' => 'start_at',
            'endAt' => 'end_at',
        ];
    }
}
