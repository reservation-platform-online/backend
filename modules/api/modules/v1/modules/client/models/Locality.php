<?php

namespace app\modules\api\modules\v1\modules\client\models;

use app\models\business\BusinessPoint;

/**
 * This is the model class for table "{{%additional__locality}}".
 *
 * @property int $id
 * @property string $name
 * @property int $enabled
 * @property int $hide
 * @property int|null $country_id
 * @property int|null $region_id
 *
 * @property BusinessPoint[] $businessPoints
 */
class Locality extends \app\models\localities\Locality
{


    /**
     * {@inheritdoc}
     */
    public function fields()
    {
        return [
            'id',
            'name',
        ];
    }

}
