<?php

namespace app\modules\api\modules\v1\modules\client\controllers;

use app\models\business\BusinessCompany;
use app\modules\api\modules\v1\modules\client\models\forms\RegistrationCompanyForm;
use Yii;
use yii\db\DataReader;
use yii\filters\auth\HttpBearerAuth;
use yii\rest\Controller;

/**
 * UserController implements the CRUD actions for Offer model.
 */
class CompanyController extends Controller
{

    public function beforeAction($action)
    {

        parent::beforeAction($action);


        if (Yii::$app->getRequest()->getMethod() === 'OPTIONS') {

            Yii::$app->getResponse()->getHeaders()->set('Allow', 'POST GET PUT DELETE');

            Yii::$app->end();

        }


        return true;

    }

    public function behaviors()
    {
        $behaviors = parent::behaviors();

        $auth = $behaviors['authenticator'];

        unset($behaviors['authenticator']);

        $behaviors['corsFilter'] = [

            'class' => \yii\filters\Cors::className(),

        ];

        $behaviors['authenticator'] = $auth;

        // avoid authentication on CORS-pre-flight requests (HTTP OPTIONS method)

        $behaviors['authenticator'] = [

            'class' => HttpBearerAuth::className(),
            'only' => [
                'create'
            ],
            'except' => [
                'options'
            ]

        ];

        $behaviors['client_api'] = [
            'class' => 'app\behaviors\ClientApi',
        ];

        return $behaviors;

    }

    /**
     *
     * Lists all Offer models.
     * @param int $page
     * @param int $count
     * @param string $orderBy
     * @param string $sort
     * @param null $status
     * @param null $search
     * @return mixed
     */
    public function actionIndex(
        $page = 1,
        $count = 5,
        $orderBy = 'id',
        $sort = 'desc',
        $status = null
    )
    {

        $query = BusinessCompany::find();

        if (!is_null($status)) {

            $query->where(['status' => $status]);

        }

        $query->orderBy($orderBy . " " . $sort)->limit($count);

        return [
            'total' => (integer) $query->count(),
            'models' => $query->offset(($page - 1) * $count)->all(),
        ];

    }

    /**
     * @param $id
     * @return BusinessCompany
     */
    public function actionView($id) {

        return BusinessCompany::findOne($id);

    }

    /**
     * @return BusinessCompany|array|false|string|DataReader
     */
    public function actionCreate() {


        $registrationForm = new RegistrationCompanyForm();
        $registrationForm->setScenario(RegistrationCompanyForm::SCENARIO_CREATE);
        $registrationForm->load(Yii::$app->request->post(), '');

        return $registrationForm->registration();

    }

    /**
     * @return bool
     */
    public function actionUpdate() {

        return true;

    }

}
