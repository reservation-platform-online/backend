<?php

namespace app\modules\api\modules\v1\modules\client\controllers;

use app\models\authorizations\Authorization;
use app\models\authorizations\AuthorizationDevice;
use app\models\authorizations\AuthorizationExternal;
use app\modules\api\modules\v1\modules\client\models\agreements\Agreement;
use app\modules\api\modules\v1\modules\client\models\authorizations\AuthorizationPlatform;
use app\modules\api\modules\v1\modules\client\models\forms\ConfirmEmailForm;
use app\modules\api\modules\v1\modules\client\models\forms\LoginForm;
use app\modules\api\modules\v1\modules\client\models\forms\RegistrationCompanyForm;
use app\modules\api\modules\v1\modules\client\models\forms\RegistrationExternalForm;
use app\modules\api\modules\v1\modules\client\models\forms\RegistrationForm;
use app\modules\api\modules\v1\modules\client\models\forms\ResetPasswordForm;
use Throwable;
use Yii;
use yii\filters\Cors;
use yii\rest\Controller;
use yii\web\Response;

/**
 * UserController implements the CRUD actions for Offer model.
 */
class AuthController extends Controller
{

    public function beforeAction($action)
    {

        parent::beforeAction($action);


        if (Yii::$app->getRequest()->getMethod() === 'OPTIONS') {

            Yii::$app->getResponse()->getHeaders()->set('Allow', 'POST GET PUT DELETE');

            Yii::$app->end();

        }


        return true;

    }

    public function behaviors()
    {
        $behaviors = parent::behaviors();

        unset($behaviors['authenticator']);

        $behaviors['corsFilter'] = [

            'class' => Cors::className(),

        ];

        $behaviors['client_api'] = [
            'class' => 'app\behaviors\ClientApi',
        ];

        return $behaviors;
    }

    /**
     * @return mixed|\yii\console\Response|Response
     * @throws Throwable
     */
    public function actionExternalLogin()
    {

        $code = Yii::$app->request->post('code');
        $platform = 'zoom';

        if (Yii::$app->request->post('platform')) {
            $platform = Yii::$app->request->post('platform');
        }

        if ($platform === 'zoom') {

            $zoom = Yii::$app->zoom;
            $zoom->code = $code;

            if ($zoom->postLogin()) {

                if ($zoom->getUser()) {

                    $registrationExternalForm = new RegistrationExternalForm();

                    $registrationExternalForm->email = $zoom->user['email'];
                    $registrationExternalForm->phoneNumber = $zoom->user['phone_number'];
                    $registrationExternalForm->picUrl = $zoom->user['pic_url'];
                    $registrationExternalForm->firstName = $zoom->user['first_name'];
                    $registrationExternalForm->lastName = $zoom->user['last_name'];

                    // Registration function check first if user is exist if is true, function return user data
                    $user = $registrationExternalForm->registration();

                    // If registration function return data with type user is correct
                    if ($user['type'] === 'user') {

                        $platformId = AuthorizationPlatform::findOne([
                            'client_id' => $zoom->clientId,
                            'client_secret' => $zoom->clientSecret
                        ])->id;

                        AuthorizationExternal::deleteAll([
                            'user_id' => $user['data']->id,
                            'external_user_id' => $zoom->user['id'],
                            'authorization__platform_id' => $platformId
                        ]);

                        $authorizationExternal = new AuthorizationExternal();

                        $authorizationExternal->authorization__platform_id = $platformId;
                        $authorizationExternal->user_id = $user['data']->id;
                        $authorizationExternal->external_user_id = $zoom->user['id'];
                        $authorizationExternal->access_token = $zoom->token['access_token'];
                        $authorizationExternal->token_type = $zoom->token['token_type'];
                        $authorizationExternal->refresh_token = $zoom->token['refresh_token'];
                        $authorizationExternal->expires_in = $zoom->token['expires_in'];
                        $authorizationExternal->scope = $zoom->token['scope'];
                        $authorizationExternal->code = $zoom->code;

                        if ($authorizationExternal->validate() && $authorizationExternal->save()) {

                            $clientId = Yii::$app->request->getHeaders()->get('Client-Id');
                            $deviceUuid = Yii::$app->request->getHeaders()->get('Device-Uuid');

                            $device = AuthorizationDevice::findOne(['device_uuid' => $deviceUuid]);

                            if (!$device) {
                                $device = new AuthorizationDevice();
                                $device->device_uuid = $deviceUuid;
                                if ($device->validate()) {
                                    $device->save();
                                } else {
                                    $response = Yii::$app->response;
                                    $response->setStatusCode(400);
                                    $response->data = $device->errors;

                                    return $response;
                                }
                            }

                            $loginForm = new Authorization();
                            $loginForm->user_id = $user['data']->id;
                            $loginForm->authorization__device_id = $device->id;
                            $loginForm->authorization__client_id = $clientId;

                            if ($loginForm->validate() && $loginForm->save()) {

                                $response = Yii::$app->response;
                                $response->setStatusCode(201);
                                $response->data = $loginForm->id;

                                return $response;

                            }

                            $response = Yii::$app->response;
                            $response->setStatusCode(400);
                            $response->data = $loginForm->errors;

                            return $response;

                        } else {

                            $response = Yii::$app->response;
                            $response->setStatusCode(400);
                            $response->data = $authorizationExternal->errors;

                            return $response;

                        }

                    } else {

                        return $user['error'];

                    }

                }

            }

        }

    }

    /**
     *
     * Lists all Offer models.
     * @param null $isDev
     * @return mixed
     */
//    public function actionPlatformList(
//        $isDev = null
//    )
//    {
//
//        $models = AuthorizationPlatform::find();
//
//        if (!is_null($isDev)) {
//            $isDev = intval($isDev);
//            if ($isDev === 0 || $isDev === 1) {
//                $models->where([
//                    'is_dev' => $isDev
//                ]);
//
//            } else {
//
//                $response = Yii::$app->response;
//                $response->setStatusCode(400);
//                $response->data = 'isDev must be number 1 or 0';
//                return $response;
//
//            }
//        }
//
//        return $models->all();
//
//    }

    /**
     *
     * Lists all Offer models.
     * @return mixed
     */
    public function actionDeauthorization()
    {

        $data = Yii::$app->request->post();

        if (!is_null($data['payload']['user_id'])) {

            if (AuthorizationExternal::deleteAll(['external_user_id' => $data['payload']['user_id']])) {

                return true;

            }

        }

        return false;

    }

    /**
     *
     * Lists all Offer models.
     * @return mixed
     */
    public function actionLogin()
    {

        $loginForm = new LoginForm();
        $loginForm->load(Yii::$app->request->post(), '');

        return $loginForm->login();

    }

    /**
     *
     * Lists all Offer models.
     * @return mixed
     */
    public function actionRegistration()
    {

        $registrationForm = new RegistrationForm();
        $registrationForm->setScenario(RegistrationForm::SCENARIO_STANDARD);
        $registrationForm->load(Yii::$app->request->post(), '');

        return $registrationForm->registration();

    }

    /**
     *
     * Lists all Offer models.
     * @return mixed
     */
    public function actionRegistrationCompany()
    {

        $registrationForm = new RegistrationCompanyForm();
        $registrationForm->setScenario(RegistrationCompanyForm::SCENARIO_REGISTER_AND_CREATE);
        $registrationForm->load(Yii::$app->request->post(), '');

        return $registrationForm->registration();

    }

    /**
     *
     * Lists all Offer models.
     * @return mixed
     */
    public function actionResetPassword()
    {

        $resetPassword = new ResetPasswordForm();
        $resetPassword->load(Yii::$app->request->post(), '');
        $resetPassword->password = uniqid();

        return $resetPassword->resetPassword();

    }

    /**
     *
     * Lists all Offer models.
     * @return mixed
     */
    public function actionAgreements()
    {

        return Agreement::find()->where([
            'enabled' => Agreement::ENABLED,
            'hide' => Agreement::SHOW
        ])->all();

    }

    /**
     *
     * Lists all Offer models.
     * @return mixed
     */
    public function actionConfirmEmail()
    {

        $confirmEmail = new ConfirmEmailForm();
        $confirmEmail->load(Yii::$app->request->post(), '');

        return $confirmEmail->confirm();

    }

}
