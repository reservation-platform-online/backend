<?php

namespace app\modules\api\modules\v1\modules\client\controllers;

use app\modules\api\modules\v1\modules\client\models\business\BusinessReservation;
use app\modules\api\modules\v1\modules\client\models\business\BusinessService;
use app\modules\api\modules\v1\modules\client\models\business\BusinessWorker;
use app\modules\api\modules\v1\modules\client\models\business\BusinessWorkTime;
use app\modules\api\modules\v1\modules\client\models\categories\Category;
use app\modules\api\modules\v1\modules\client\models\customs\Pws;
use app\modules\api\modules\v1\modules\client\models\customs\PwsCalendar;
use app\modules\api\modules\v1\modules\client\models\forms\CreateReservation;
use app\modules\api\modules\v1\modules\client\models\Locality;
use Yii;
use yii\console\Response;
use yii\db\ActiveRecord;
use yii\filters\auth\HttpBearerAuth;
use yii\rest\Controller;

/**
 * UserController implements the CRUD actions for Offer model.
 */
class ServiceController extends Controller
{

    public function beforeAction($action)
    {

        parent::beforeAction($action);


        if (Yii::$app->getRequest()->getMethod() === 'OPTIONS') {

            Yii::$app->getResponse()->getHeaders()->set('Allow', 'POST GET PUT DELETE');

            Yii::$app->end();

        }


        return true;

    }

    public function behaviors()
    {
        $behaviors = parent::behaviors();

        unset($behaviors['authenticator']);

        $behaviors['corsFilter'] = [

            'class' => \yii\filters\Cors::className(),

        ];

        $behaviors['client_api'] = [
            'class' => 'app\behaviors\ClientApi',
        ];

        return $behaviors;
    }

    public function actionIndex(
        $page = 1,
        $count = 5,
        $subcategoryId = null,
        $companyId = null,
        $orderBy = 'id',
        $sort = 'desc'
    )
    {

        $query = BusinessService::find()
            ->where(['enabled' => BusinessService::ENABLED, 'hide' => BusinessService::SHOW]);

        if (!is_null($subcategoryId)) {
            $query->andWhere(['business__subcategory_id' => $subcategoryId]);
        }

        if (!is_null($companyId)) {
            $query->andWhere(['business__company_id' => $companyId]);
        }

        $query->orderBy($orderBy . " " . $sort)->limit($count);

        return [
            'total' => (integer)$query->count(),
            'models' => $query->offset(($page - 1) * $count)->all(),
        ];
    }

}
