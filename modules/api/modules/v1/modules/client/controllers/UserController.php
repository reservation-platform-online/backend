<?php

namespace app\modules\api\modules\v1\modules\client\controllers;

use app\models\authorizations\Authorization;
use app\models\authorizations\AuthorizationExternal;
use app\modules\api\modules\v1\modules\client\models\authorizations\AuthorizationPlatform;
use app\modules\api\modules\v1\modules\client\models\forms\ChangePasswordForm;
use app\modules\api\modules\v1\modules\client\models\User;
use app\modules\api\modules\v1\modules\company\models\forms\UploadFileForm;
use Throwable;
use Yii;
use yii\console\Response;
use yii\filters\auth\HttpBearerAuth;
use yii\filters\Cors;
use yii\rest\Controller;
use yii\web\UploadedFile;

/**
 * UserController implements the CRUD actions for Offer model.
 */
class UserController extends Controller
{

    public function beforeAction($action)
    {

        parent::beforeAction($action);


        if (Yii::$app->getRequest()->getMethod() === 'OPTIONS') {

            Yii::$app->getResponse()->getHeaders()->set('Allow', 'POST GET PUT DELETE');

            Yii::$app->end();

        }


        return true;

    }

    public function behaviors()
    {
        $behaviors = parent::behaviors();

        $auth = $behaviors['authenticator'];

        unset($behaviors['authenticator']);

        $behaviors['corsFilter'] = [

            'class' => Cors::className(),

        ];

        $behaviors['authenticator'] = $auth;

        // avoid authentication on CORS-pre-flight requests (HTTP OPTIONS method)

        $behaviors['authenticator'] = [

            'class' => HttpBearerAuth::className(),
            'only' => [
                'me', 'logout', 'update', 'change-password', 'external-connect', 'upload-image', 'external-refresh-token'
            ],
            'except' => [
                'options'
            ]

        ];

        $behaviors['client_api'] = [
            'class' => 'app\behaviors\ClientApi',
        ];

        return $behaviors;

    }

    /**
     *
     * Lists all Offer models.
     * @return mixed
     */
    public function actionMe()
    {

        return User::findOne(Yii::$app->user->id);

    }

    public function actionExternalRefreshToken()
    {
        $platform = 'zoom';

        if (Yii::$app->request->post('platform')) {
            $platform = Yii::$app->request->post('platform');
        }

        if ($platform === 'zoom') {

            $authorizationExternal = AuthorizationExternal::find()->where([
                'user_id' => Yii::$app->user->id
            ])->one();

            if (is_null($authorizationExternal)) {

                $response = Yii::$app->response;
                $response->setStatusCode(400);
                $response->data = 'You need to connect a service to generate rooms';
                return $response;

            }

            $zoom = Yii::$app->zoom;
            $zoom->token = $authorizationExternal;

            return Yii::$app->user->identity->updateAuthorizationExternal($zoom->token, $zoom->postRefreshToken());

        }

    }

    public function actionExternalConnect()
    {

        $code = Yii::$app->request->post('code');
        $platform = 'zoom';

        if (Yii::$app->request->post('platform')) {
            $platform = Yii::$app->request->post('platform');
        }

        if ($platform === 'zoom') {

            $zoom = Yii::$app->zoom;
            $zoom->code = $code;

            if ($zoom->postLogin('client/profile')) {

                if ($zoom->getUser()) {

                    AuthorizationExternal::deleteAll([
                        'user_id' => Yii::$app->user->id
                    ]);

                    $authorizationExternal = new AuthorizationExternal();

                    $authorizationExternal->authorization__platform_id = AuthorizationPlatform::findOne([
                        'client_id' => $zoom->clientId,
                        'client_secret' => $zoom->clientSecret
                    ])->id;
                    $authorizationExternal->user_id = Yii::$app->user->id;
                    $authorizationExternal->external_user_id = $zoom->user['id'];
                    $authorizationExternal->access_token = $zoom->token['access_token'];
                    $authorizationExternal->token_type = $zoom->token['token_type'];
                    $authorizationExternal->refresh_token = $zoom->token['refresh_token'];
                    $authorizationExternal->expires_in = $zoom->token['expires_in'];
                    $authorizationExternal->scope = $zoom->token['scope'];
                    $authorizationExternal->code = $zoom->code;

                    if ($authorizationExternal->validate() && $authorizationExternal->save()) {

                        return true;

                    } else {

                        $response = Yii::$app->response;
                        $response->setStatusCode(400);
                        $response->data = $authorizationExternal->errors;

                        return $response;

                    }

                }

            }

        }

    }

    /**
     *
     * Lists all Offer models.
     * @return mixed
     */
    public function actionChangePassword()
    {

        $changePassword = new ChangePasswordForm();
        $changePassword->setScenario(ChangePasswordForm::VIA_OLD_PASSWORD);
        $changePassword->load(Yii::$app->request->post(), '');
        $changePassword->email = Yii::$app->user->identity->email;

        return $changePassword->changePassword();

    }

    /**
     *
     * Lists all Offer models.
     * @return mixed
     * @throws Throwable
     */
    public function actionConfirmEmail()
    {

        return Yii::$app->request->post();

    }

    /**
     *
     * Lists all Offer models.
     * @return mixed
     */
    public function actionUpdate()
    {

        if (count(Yii::$app->request->post()) === 0) {
            $response = Yii::$app->response;
            $response->setStatusCode(400);
            $response->data = 'Data is empty';
            return $response;
        }

        $user = User::findOne(['id' => Yii::$app->user->id]);

        if ($user) {

            $user->load(Yii::$app->request->post(), '');

            if ($user->validate()) {

                return $user->save();

            }

        }

        $response = Yii::$app->response;
        $response->setStatusCode(400);
        $response->data = $user->errors;
        return $response;

    }

    /**
     *
     * Lists all Offer models.
     * @return mixed
     */
    public function actionLogout()
    {

        if (Yii::$app->request->isDelete) {

            $token = explode (" ", Yii::$app->request->headers['authorization'])[1];
            if ($authorization = Authorization::findOne(['id' => $token, 'enabled' => Authorization::ENABLED])) {

                return $authorization->delete();

            }

        }

        return false;

    }

    /**
     * @param $id
     * @return array|bool|string|Response|\yii\web\Response
     */
    public function actionUploadImage()
    {

        $user = User::findOne(['id' => Yii::$app->user->id]);

        $model = new UploadFileForm();
        $model->rootPath = Yii::$app->params['baseUploadsDir'] . '/users/' . Yii::$app->user->id . '/';

        if ($user->photo) {

            $filename = explode('/', $user->photo);
            $filename = $filename[count($filename) - 1];

            if ($filename && file_exists($model->rootPath . $filename)) {

                unlink($model->rootPath . $filename);

            }

        }

        $model->load(Yii::$app->request->post(), '');
        $model->imageFile = UploadedFile::getInstanceByName('imageFile');

        $fileUrl = $model->upload();

        $user->photo = $fileUrl['url'];

        if ($user->validate()) {

            return $user->save();

        }

        $response = Yii::$app->response;
        $response->setStatusCode(400);
        $response->data = $user->errors;
        return $response;

    }

}
