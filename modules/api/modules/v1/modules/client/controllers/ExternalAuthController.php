<?php

namespace app\modules\api\modules\v1\modules\client\controllers;

use app\modules\api\modules\v1\modules\client\models\forms\zoom\Callback;
use Yii;
use yii\console\Response;
use yii\rest\Controller;

/**
 * UserController implements the CRUD actions for Offer model.
 */
class ExternalAuthController extends Controller
{

    public function beforeAction($action)
    {

        parent::beforeAction($action);


        if (Yii::$app->getRequest()->getMethod() === 'OPTIONS') {

            Yii::$app->getResponse()->getHeaders()->set('Allow', 'POST GET PUT DELETE');

            Yii::$app->end();

        }


        return true;

    }

    public function behaviors()
    {
        $behaviors = parent::behaviors();

        unset($behaviors['authenticator']);

        $behaviors['corsFilter'] = [

            'class' => \yii\filters\Cors::className(),

        ];

        $behaviors['client_api'] = [
            'class' => 'app\behaviors\ClientApi',
        ];

        return $behaviors;
    }

    /**
     *
     * Lists all Offer models.
     * @return mixed
     */
    public function actionIndex(
    )
    {

        return [];

    }

    /**
     *
     * Lists all Offer models.
     * @return mixed
     */
    public function actionCallback()
    {

        $model = new Callback();
        $model->code = $_GET['code'];

        return $model->run();

    }

    /**
     *
     * Lists all Offer models.
     * @return mixed
     *
     * {
         * "event": "app_deauthorized",
         * "payload": {
             * "user_data_retention": "false",
             * "account_id": "EabCDEFghiLHMA",
             * "user_id": "z9jkdsfsdfjhdkfjQ",
             * "signature": "827edc3452044f0bc86bdd5684afb7d1e6becfa1a767f24df1b287853cf73000",
             * "deauthorization_time": "2019-06-17T13:52:28.632Z",
             * "client_id": "ADZ9k9bTWmGUoUbECUKU_a"
         * }
     * }
     *
     */
    public function actionDeauthorization(
    )
    {

        return [];

    }

    /**
     * @param $accessToken
     * @return array|bool
     */
//    public function actionCreateAccessToken(
//        $accessToken
//    )
//    {
//
//        if (is_null($accessToken)) {
//            return 'accessToken is required';
//        }
//
//        $zoomAccessToken = new ZoomAccessToken();
//        $zoomAccessToken->access_token = $accessToken;
//
//        if ($zoomAccessToken->validate()) {
//
//            return $zoomAccessToken->save();
//
//        }
//
//        return $zoomAccessToken->errors;
//
//    }

    /**
     * @param $accessToken
     * @return bool|Response|\yii\web\Response
     */
//    public function actionUpdateAccessToken(
//        $accessToken
//    )
//    {
//
//        $zoomAccessToken = ZoomAccessToken::find()->where(['access_token' => $accessToken])->one();
//        if ($zoomAccessToken) {
//
//            $zoomAccessToken->access_token = $accessToken;
//
//            return $zoomAccessToken->save();
//
//        }
//
//        $response = Yii::$app->response;
//        $response->setStatusCode(400);
//        $response->data = 'Access Token not found';
//        return $response;
//
//    }

    /**
     * @return bool|Response|\yii\web\Response
     * @throws \GuzzleHttp\Exception\GuzzleException
     * @throws \Throwable
     */
//    public function actionCreateMeeting()
//    {
//
//        $model = new CreateMeeting();
//        $model->load(Yii::$app->request->post(), '');
//        return $model->run();
//
//    }

}
