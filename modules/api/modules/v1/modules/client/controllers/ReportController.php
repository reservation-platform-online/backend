<?php

namespace app\modules\api\modules\v1\modules\client\controllers;
use Yii;
use yii\rest\Controller;

/**
 * UserController implements the CRUD actions for Offer model.
 */
class ReportController extends Controller
{

    public function beforeAction($action)
    {

        parent::beforeAction($action);


        if (Yii::$app->getRequest()->getMethod() === 'OPTIONS') {

            Yii::$app->getResponse()->getHeaders()->set('Allow', 'POST GET PUT DELETE');

            Yii::$app->end();

        }


        return true;

    }

    public function behaviors()
    {
        $behaviors = parent::behaviors();

        $behaviors['corsFilter'] = [

            'class' => \yii\filters\Cors::className(),

        ];

        $behaviors['client_api'] = [
            'class' => 'app\behaviors\ClientApi',
        ];

        return $behaviors;
    }

    public function actionReport() {

        $payload = Yii::$app->request->post();

        if (is_null($payload['description'])) {

            $response = Yii::$app->response;
            $response->setStatusCode(400);
            $response->data = 'Description is empty.';
            return $response;

        }

        $EMAIL_TEXT = $payload['description'];
        $EMAIL_SUBJECT = 'Report from app';

        Yii::$app->mailer->compose()
            ->setFrom([Yii::$app->params['infoEmail'] => Yii::$app->params['nameOfInfoEmail']])
            ->setTo(Yii::$app->params['reportEmail'])
            ->setSubject($EMAIL_SUBJECT)
            ->setHtmlBody('
                    LOGO reservation-platform-online
                    <br/> 
                    <p>
                        ' . $EMAIL_TEXT . '
                    </p>
                ')
            ->send();

        return true;

    }

}
