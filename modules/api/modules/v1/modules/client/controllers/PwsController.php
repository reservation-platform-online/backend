<?php

namespace app\modules\api\modules\v1\modules\client\controllers;

use app\modules\api\modules\v1\modules\client\models\business\BusinessComment;
use app\modules\api\modules\v1\modules\client\models\business\BusinessReservation;
use app\modules\api\modules\v1\modules\client\models\business\BusinessService;
use app\modules\api\modules\v1\modules\client\models\business\BusinessWorker;
use app\modules\api\modules\v1\modules\client\models\business\BusinessWorkTime;
use app\modules\api\modules\v1\modules\client\models\categories\Category;
use app\modules\api\modules\v1\modules\client\models\customs\Pws;
use app\modules\api\modules\v1\modules\client\models\customs\PwsCalendar;
use app\modules\api\modules\v1\modules\client\models\forms\CreateReservation;
use app\modules\api\modules\v1\modules\client\models\Locality;
use Yii;
use yii\console\Response;
use yii\db\ActiveRecord;
use yii\filters\auth\HttpBearerAuth;
use yii\rest\Controller;

/**
 * UserController implements the CRUD actions for Offer model.
 */
class PwsController extends Controller
{

    public function beforeAction($action)
    {

        parent::beforeAction($action);


        if (Yii::$app->getRequest()->getMethod() === 'OPTIONS') {

            Yii::$app->getResponse()->getHeaders()->set('Allow', 'POST GET PUT DELETE');

            Yii::$app->end();

        }


        return true;

    }

    public function behaviors()
    {
        $behaviors = parent::behaviors();

        unset($behaviors['authenticator']);

        $behaviors['corsFilter'] = [

            'class' => \yii\filters\Cors::className(),

        ];

        $behaviors['client_api'] = [
            'class' => 'app\behaviors\ClientApi',
        ];

        return $behaviors;
    }

    /**
     * @param null $start
     * @param null $end
     * @param null $pointId
     * @param null $companyId
     * @param null $categoryId
     * @param int $page
     * @param int $count
     * @param string $orderBy
     * @param string $sort
     * @return array|string|ActiveRecord|ActiveRecord[]|null
     */
    public function actionIndex(
        $start = null,
        $end = null,
        $pointId = null,
        $companyId = null,
        $categoryId = null,
        $page = 1,
        $count = 5,
        $orderBy = 'id',
        $sort = 'desc'
    )
    {

        if (is_null($categoryId)) {
            $response = Yii::$app->response;
            $response->setStatusCode(400);
            $response->data = 'Need write categoryId';
            return $response;
        }

        $startDate = date("Y-m-d", strtotime($start));
        $endDate = date("Y-m-d", strtotime($end));

        \Yii::$app->params['client']['pws']['startAt'] = $startDate;
        \Yii::$app->params['client']['pws']['endAt'] = $endDate;

        $businessWorkerEnabled = BusinessWorker::ENABLED;
        $businessServiceEnabled = BusinessService::ENABLED; // TODO
        $businessWorkTimeEnabled = BusinessWorkTime::ENABLED;

        $query = Pws::find()
            ->where([
                'business__point_worker_service.enabled' => Pws::ENABLED,
                'business__point_worker_service.hide' => Pws::SHOW
            ])
            ->innerJoin('business__worker', "business__worker.enabled = '$businessWorkerEnabled' AND business__worker.id = business__point_worker_service.business__worker_id")
            ->innerJoin('business__service', "business__service.category_id = $categoryId AND business__service.id = business__point_worker_service.business__service_id")
            ->innerJoin('business__work_time', "business__work_time.enabled = '$businessWorkTimeEnabled' AND business__work_time.business__point_worker_service_id = business__point_worker_service.id AND business__work_time.business__worker_id = business__point_worker_service.business__worker_id")
            ->groupBy('business__point_worker_service.id');

        if (!is_null($companyId)) {

            $query->andWhere(['business__point_worker_service.business__company_id' => $companyId]);

        }

        $query->orderBy($orderBy . " " . $sort);

        return [
            'total' => (integer)$query->count(),
            'models' => $query->limit($count)->offset(($page - 1) * $count)->all(),
        ];

    }

    /**
     * @param $id
     * @return array|string|ActiveRecord|ActiveRecord[]|null
     */
    public function actionView($id)
    {

        return Pws::findOne($id);

    }

    /**
     * @param $id
     * @param null $start
     * @param null $end
     * @return array|string|ActiveRecord|ActiveRecord[]|null
     */
    public function actionCalendar(
        $id,
        $start = null,
        $end = null
    )
    {

        $startDate = date("Y-m-d", strtotime($start));
        $endDate = date("Y-m-d", strtotime($end));

        \Yii::$app->params['client']['pws']['startAt'] = $startDate;
        \Yii::$app->params['client']['pws']['endAt'] = $endDate;

        return PwsCalendar::findOne($id);

    }

    /**
     * @param $id
     * @param int $page
     * @param int $count
     * @param string $orderBy
     * @param string $sort
     * @return array|string|ActiveRecord|ActiveRecord[]|null
     */
    public function actionComments(
        $id,
        $page = 1,
        $count = 5,
        $orderBy = 'created_at',
        $sort = 'desc'
    )
    {

        $query = BusinessComment::find()
        ->where([
            'enabled' => BusinessComment::ENABLED,
            'hide' => BusinessComment::SHOW,
            'business__point_worker_service_id' => $id
        ]);

        $query->orderBy($orderBy . " " . $sort);

        return [
            'total' => (integer)$query->count(),
            'models' => $query->limit($count)->offset(($page - 1) * $count)->all(),
        ];

    }

}
