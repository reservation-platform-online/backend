<?php

namespace app\modules\api\modules\v1\modules\client\controllers;

use app\modules\api\modules\v1\modules\client\models\business\BusinessComment;
use app\modules\api\modules\v1\modules\client\models\business\BusinessReservation;
use app\modules\api\modules\v1\modules\client\models\forms\CreateComment;
use app\modules\api\modules\v1\modules\client\models\forms\CreateReservation;
use Yii;
use yii\console\Response;
use yii\filters\auth\HttpBearerAuth;
use yii\filters\Cors;
use yii\rest\Controller;

/**
 * UserController implements the CRUD actions for Offer model.
 */
class ReservationController extends Controller
{

    public function beforeAction($action)
    {

        parent::beforeAction($action);


        if (Yii::$app->getRequest()->getMethod() === 'OPTIONS') {

            Yii::$app->getResponse()->getHeaders()->set('Allow', 'POST GET PUT DELETE');

            Yii::$app->end();

        }


        return true;

    }

    public function behaviors()
    {
        $behaviors = parent::behaviors();

        $auth = $behaviors['authenticator'];

        unset($behaviors['authenticator']);

        $behaviors['corsFilter'] = [

            'class' => Cors::className(),

        ];

        $behaviors['authenticator'] = $auth;

        // avoid authentication on CORS-pre-flight requests (HTTP OPTIONS method)

        $behaviors['authenticator'] = [

            'class' => HttpBearerAuth::className(),
            'only' => [
                'create', 'cancel', 'index', 'comment'
            ],
            'except' => [
                'options'
            ]

        ];

        $behaviors['client_api'] = [
            'class' => 'app\behaviors\ClientApi',
        ];

        return $behaviors;

    }

    public function actionCreate()
    {

        $createReservation = new CreateReservation();
        $createReservation->load(Yii::$app->request->post(), '');

        return $createReservation->create();
    }

    public function actionComment(
        $pwsId,
        $reservationUserId
    )
    {
        return BusinessComment::find()->where([
            'business__point_worker_service_id' => $pwsId,
            'business__reservation__user_id' => $reservationUserId,
            'enabled' => BusinessComment::ENABLED,
            'hide' => BusinessComment::SHOW,
        ])->one();
    }

    public function actionCreateComment()
    {
        $createComment = new CreateComment();
        $createComment->load(Yii::$app->request->post(), '');

        return $createComment->create();
    }

    /**
     * @param $id
     * @return bool|Response|\yii\web\Response
     */
    public function actionCancel(
        $id
    )
    {
        $reservation = BusinessReservation::findOne($id);
        if ($reservation) {
            $reservation->status = 0;
            return $reservation->save();
        }
        $response = Yii::$app->response;
        $response->setStatusCode(400);
        $response->data = 'Id is not correct';
        return $response;
    }

    public function actionIndex(
        $page = 1,
        $count = 25,
        $active = 1,
        $orderBy = 'business__reservation.date, business__reservation.start_at',
        $sort = 'ASC'
    )
    {

        $active = intval($active);

        $userId = Yii::$app->user->id;
        $query = BusinessReservation::find()
            ->where(['business__reservation.enabled' => BusinessReservation::ENABLED, 'business__reservation.hide' => BusinessReservation::SHOW])
            ->andWhere([
                'in', 'business__reservation.status', $active === 1 ? BusinessReservation::ACTIVE_STATUS_LIST : BusinessReservation::HISTORY_STATUS_LIST
            ])
            ->innerJoin('business__reservation__user', "business__reservation__user.user_id = '$userId' AND business__reservation__user.business__reservation_id = business__reservation.id");

        $query->orderBy($orderBy . " " . $sort)->limit($count);

        return [
            'total' => (integer)$query->count(),
            'models' => $query->offset(($page - 1) * $count)->all(),
        ];

    }

}
