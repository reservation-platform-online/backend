<?php

namespace app\modules\api\modules\v1\modules\client\controllers;

use app\modules\api\modules\v1\modules\client\models\categories\Category;
use Yii;
use yii\filters\Cors;
use yii\rest\Controller;

/**
 * UserController implements the CRUD actions for Offer model.
 */
class CategoryController extends Controller
{

    public function beforeAction($action)
    {

        parent::beforeAction($action);


        if (Yii::$app->getRequest()->getMethod() === 'OPTIONS') {

            Yii::$app->getResponse()->getHeaders()->set('Allow', 'POST GET PUT DELETE');

            Yii::$app->end();

        }


        return true;

    }

    public function behaviors()
    {
        $behaviors = parent::behaviors();

        unset($behaviors['authenticator']);

        $behaviors['corsFilter'] = [

            'class' => Cors::className(),

        ];

        $behaviors['client_api'] = [
            'class' => 'app\behaviors\ClientApi',
        ];

        return $behaviors;
    }

    public function actionIndex(
        $page = null,
        $count = null,
        $orderBy = 'name',
        $categoryId = null,
        $sort = 'ASC'
    )
    {

        $query = Category::find()
            ->where([
                'category.enabled' => Category::ENABLED,
                'category.hide' => Category::SHOW
            ]);

        if (!is_null($categoryId)) {

            $query
                ->innerJoin('category__child', "category__child.parent_id = $categoryId AND category__child.category_id = category.id");
//            ELSE IF
//            $query
//                ->leftJoin('category__child', 'category__child.category_id = category.id')
//                ->andWhere(['category__child.category_id' => NULL]);
        }

        $query->orderBy($orderBy . " " . $sort);

        if (is_null($page) && is_null($count)) {
            $models = $query->all();
        } else {
            $models = $query->limit($count)->offset(($page - 1) * $count)->all();
        }

        return [
            'total' => (integer)$query->count(),
            'models' => $models,
        ];
    }

}
