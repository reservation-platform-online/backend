<?php

namespace app\modules\api\modules\v1\modules\client\controllers;

use app\modules\api\modules\v1\modules\client\models\business\BusinessPoint;
use Yii;
use yii\filters\Cors;
use yii\rest\Controller;

/**
 * UserController implements the CRUD actions for Offer model.
 */
class PointController extends Controller
{

    public function beforeAction($action)
    {

        parent::beforeAction($action);


        if (Yii::$app->getRequest()->getMethod() === 'OPTIONS') {

            Yii::$app->getResponse()->getHeaders()->set('Allow', 'POST GET PUT DELETE');

            Yii::$app->end();

        }


        return true;

    }

    public function behaviors()
    {
        $behaviors = parent::behaviors();

        unset($behaviors['authenticator']);

        $behaviors['corsFilter'] = [

            'class' => Cors::className(),

        ];

        $behaviors['client_api'] = [
            'class' => 'app\behaviors\ClientApi',
        ];

        return $behaviors;
    }

    /**
     *
     * Lists all Offer models.
     * @param int $page
     * @param int $count
     * @param string $orderBy
     * @param string $sort
     * @return mixed
     */
    public function actionIndex(
        $page = 1,
        $count = 5,
        $orderBy = 'id',
        $sort = 'desc'
    )
    {

        $query = BusinessPoint::find()->where(['enabled' => BusinessPoint::ENABLED]);

        $query->orderBy($orderBy . " " . $sort)->limit($count);

        return [
            'total' => (integer) $query->count(),
            'models' => $query->offset(($page - 1) * $count)->all(),
        ];

    }

    /**
     *
     * Lists all Offer models.
     * @param $id
     * @return mixed
     */
    public function actionView($id)
    {

        return BusinessPoint::findOne($id);

    }

}
