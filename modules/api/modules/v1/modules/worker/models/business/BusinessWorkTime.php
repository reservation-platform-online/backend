<?php

namespace app\modules\api\modules\v1\modules\worker\models\business;


use app\models\business\BusinessPointWorkerService;
use yii\helpers\Inflector;

/**
 * This is the model class for table "{{%business__work_time}}".
 *
 * @property string $id
 * @property string $business__worker_id
 * @property string $business__point_worker_service_id
 * @property string|null $start_date
 * @property string $start_at
 * @property string|null $end_date
 * @property string $end_at
 * @property int $weekday
 * @property int $enabled
 * @property int $hide
 *
 * @property BusinessPointWorkerService $businessPointWorkerService
 * @property BusinessWorker $businessWorker
 */
class BusinessWorkTime extends \app\models\business\BusinessWorkTime
{

    public function fields()
    {

        return [
            'id',
            'workerId' => 'business__worker_id',
            'pwsId' => 'business__point_worker_service_id',
            'startDate' => 'start_date',
            'startAt' => 'start_at',
            'endDate' => 'end_date',
            'endAt' => 'end_at',
            'weekday',
        ];
    }

    public function load($data, $formName = null)
    {

        foreach ($data as $key => $name){
            $data[Inflector::underscore($key)] = $name;
        }


        return parent::load($data, $formName);
    }

}
