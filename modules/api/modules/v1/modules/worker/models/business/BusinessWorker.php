<?php

namespace app\modules\api\modules\v1\modules\worker\models\business;

use app\models\additional\AdditionalWeekend;
use app\models\User;
use yii\db\ActiveQuery;
use yii\helpers\Inflector;

/**
 * This is the model class for table "{{%business__worker}}".
 *
 * @property string $id
 * @property string $business__company_id
 * @property string|null $user_id
 * @property string $first_name
 * @property string $last_name
 * @property string|null $photo
 * @property string|null $position
 * @property int $number_of_comments
 * @property float $score
 * @property int $enabled
 * @property int $hide
 * @property int $status 1 - Wait for confirm 2 - Confirmed 3 - Deleting 4 - Company Delete
 * @property string $created_at
 * @property string $updated_at
 *
 * @property AdditionalWeekend[] $additionalWeekends
 * @property BusinessPointWorkerService[] $businessPointWorkerServices
 * @property BusinessWorkTime[] $businessWorkTimes
 * @property User $user
 * @property BusinessCompany $businessCompany
 */
class BusinessWorker extends \app\models\business\BusinessWorker
{

    const SCENARIO_UPDATE = 'update';
    const SCENARIO_UPDATE_PHOTO = 'update_logo';
    const SCENARIO_UPDATE_ENABLED = 'update_enabled';

    public function scenarios()
    {
        return [
            self::SCENARIO_UPDATE => ['first_name', 'last_name', 'enabled'],
            self::SCENARIO_UPDATE_PHOTO => ['photo'],
            self::SCENARIO_UPDATE_ENABLED => ['enabled'],
        ];
    }

    public function fields() {

        return [
            'id',
            'companyId' => 'business__company_id',
            'firstName' => 'first_name',
            'lastName' => 'last_name',
            'score',
            'photo',
            'status',
            'position',
            'enabled',
//            'businessWorkTimes',
            'createdAt' => 'created_at',
            'updatedAt' => 'updated_at',
        ];

    }

    public function extraFields()
    {
        return [
            'services' => function($model) {
                return BusinessService::find()
                    ->innerJoin(
                        'business__point_worker_service',
                        "business__point_worker_service.business__worker_id = '$model->id'" .
                        "AND business__point_worker_service.business__service_id = business__service.id"
                    )
                    ->all();
            },
            'pwsList' => function($model) {
                return BusinessPointWorkerService::find()
                    ->where([
                        'business__worker_id' => $model->id
                    ])
                    ->all();
            }
        ];
    }

    /**
     * Gets query for [[BusinessWorkTimes]].
     *
     * @return ActiveQuery
     */
    public function getBusinessWorkTimes()
    {
        return $this->hasMany(BusinessWorkTime::className(), ['business__worker_id' => 'id']);
    }

    public function load($data, $formName = null)
    {

        foreach ($data as $key => $name){
            $data[Inflector::underscore($key)] = $name;
        }


        return parent::load($data, $formName);
    }

}
