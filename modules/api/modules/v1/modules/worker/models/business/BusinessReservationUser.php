<?php

namespace app\modules\api\modules\v1\modules\worker\models\business;

use app\models\additional\AdditionalAddressBusinessReservationUser;
use app\models\business\BusinessComment;
use app\models\User;
use Yii;
use yii\db\ActiveQuery;
use yii\db\ActiveRecord;

/**
 * This is the model class for table "{{%business__reservation__user}}".
 *
 * @property string $id
 * @property string $business__reservation_id
 * @property string $user_id
 * @property string|null $note
 * @property int $is_initiator
 * @property int $number_of_comments
 * @property string|null $meet_token
 * @property int $enabled
 * @property int $hide
 * @property string $created_at
 * @property string $updated_at
 * @property int $status 0 - Not present 1 - Wait 2 - Present
 *
 * @property AdditionalAddressBusinessReservationUser[] $additionalAddressBusinessReservationUsers
 * @property BusinessComment[] $businessComments
 * @property BusinessReservation $businessReservation
 * @property User $user
 */
class BusinessReservationUser extends \app\models\business\BusinessReservationUser
{

    /**
     * {@inheritdoc}
     */
    public function fields()
    {
        return [
            'id',
            'reservationId' => 'business__reservation_id',
            'userId' => 'user_id',
            'note',
            'isInitiator' => 'is_initiator',
            'meetToken' => 'meet_token',
            'enabled',
            'hide',
            'status',
        ];
    }
}
