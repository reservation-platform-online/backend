<?php

namespace app\modules\api\modules\v1\modules\worker\controllers;

use app\modules\api\modules\v1\modules\worker\models\business\BusinessReservation;
use app\modules\api\modules\v1\modules\worker\models\forms\CreateReservation;
use app\modules\api\modules\v1\modules\worker\models\forms\CreateReservationByEmail;
use app\modules\api\modules\v1\modules\worker\models\forms\CreateRoomForm;
use Yii;
use yii\filters\auth\HttpBearerAuth;
use yii\filters\Cors;
use yii\rest\Controller;

/**
 * UserController implements the CRUD actions for Offer model.
 */
class ReservationController extends Controller
{

    public function beforeAction($action)
    {

        parent::beforeAction($action);


        if (Yii::$app->getRequest()->getMethod() === 'OPTIONS') {

            Yii::$app->getResponse()->getHeaders()->set('Allow', 'POST GET PUT DELETE');

            Yii::$app->end();

        }


        return true;

    }

    public function behaviors()
    {
        $behaviors = parent::behaviors();

        $auth = $behaviors['authenticator'];

        unset($behaviors['authenticator']);

        $behaviors['corsFilter'] = [

            'class' => Cors::className(),

        ];

        $behaviors['authenticator'] = $auth;

        // avoid authentication on CORS-pre-flight requests (HTTP OPTIONS method)

        $behaviors['authenticator'] = [

            'class' => HttpBearerAuth::className(),
            'only' => [
                'index', 'view', 'create', 'update', 'delete', 'create-room'
            ],
            'except' => [
                'options'
            ]

        ];

        $behaviors['client_api'] = [
            'class' => 'app\behaviors\ClientApi',
            'checkWorkerId' => true
        ];

        return $behaviors;
    }

    /**
     *
     * Lists all Offer models.
     * @param int $page
     * @param int $count
     * @param string $orderBy
     * @param string $sort
     * @return mixed
     */
    public function actionIndex(
        $page = 1,
        $count = 250,
        $orderBy = 'date, start_at',
        $sort = 'asc'
    )
    {

        $workerId = Yii::$app->request->getHeaders()->get('Worker-Id');

        $query = BusinessReservation::find()
            ->innerJoin(
                'business__point_worker_service',
                "business__point_worker_service.id = business__reservation.business__point_worker_service_id AND business__point_worker_service.business__worker_id = '$workerId'");

        $query->orderBy($orderBy . " " . $sort);

        return [
            'total' => (integer)$query->count(),
            'models' => $query->limit($count)->offset(($page - 1) * $count)->all(),
        ];

    }

    /**
     *
     * Lists all Offer models.
     * @param $id
     * @return mixed
     */
    public function actionView($id)
    {

        return BusinessReservation::findOne($id);

    }

    /**
     *
     * Lists all Offer models.
     * @param $id
     * @return mixed
     */
    public function actionCreate()
    {

        $createReservationByEmail = new CreateReservationByEmail();
        $createReservationByEmail->load(Yii::$app->request->post(), '');

        return $createReservationByEmail->create();

    }

    /**
     *
     * Lists all Offer models.
     * @param $id
     * @return mixed
     */
    public function actionUpdate($id)
    {

        if (count(Yii::$app->request->post()) === 0) {
            $response = Yii::$app->response;
            $response->setStatusCode(400);
            $response->data = 'Data is empty';
            return $response;
        }

        $model = BusinessReservation::findOne(['id' => $id]);

        if ($model) {

            if ($model->load(Yii::$app->request->post(), '')) {

                if ($model->oldAttributes['status'] !== $model->status) {

                    if (!in_array($model->status, [BusinessReservation::STATUS_ENABLED, BusinessReservation::STATUS_RECEPTION_CANCELED, BusinessReservation::STATUS_CONFIRMED, BusinessReservation::STATUS_FINISHED])) {

                        $response = Yii::$app->response;
                        $response->setStatusCode(400);
                        $response->data = 'Status is not correct';
                        return $response;

                    }

                }

                if ($model->validate()) {

                    return $model->save();

                }

            }

        }

        $response = Yii::$app->response;
        $response->setStatusCode(400);
        $response->data = $model->errors;
        return $response;

    }

    /**
     *
     * Lists all Offer models.
     * @param $id
     * @return mixed
     */
    public function actionCreateRoom($id)
    {

        $createRoom = new CreateRoomForm();
        $createRoom->id = $id;
        return $createRoom->create();
    }

    /**
     *
     * Lists all Offer models.
     * @param $id
     * @return mixed
     */
    public function actionDelete($id)
    {

        // TODO write fn for set enabled to 0
        return false;

    }

}
