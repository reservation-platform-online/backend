<?php

namespace app\modules\api\modules\v1\modules\worker\controllers;

use app\modules\api\modules\v1\modules\admin\models\Statistic;
use Yii;
use yii\filters\auth\HttpBearerAuth;
use yii\rest\Controller;

/**
 * UserController implements the CRUD actions for Offer model.
 */
class WorkTimeController extends Controller
{

    public function beforeAction($action)
    {

        parent::beforeAction($action);


        if (Yii::$app->getRequest()->getMethod() === 'OPTIONS') {

            Yii::$app->getResponse()->getHeaders()->set('Allow', 'POST GET PUT DELETE');

            Yii::$app->end();

        }


        return true;

    }

    public function behaviors()
    {
        $behaviors = parent::behaviors();

        unset($behaviors['authenticator']);

        $behaviors['corsFilter'] = [

            'class' => \yii\filters\Cors::className(),

        ];

        $behaviors['client_api'] = [
            'class' => 'app\behaviors\ClientApi',
            'checkWorkerId' => true
        ];

        return $behaviors;
    }

    // TODO CRUD

    /**
     *
     * Lists all Offer models.
     * @param int $page
     * @param int $count
     * @param string $orderBy
     * @param string $sort
     * @return mixed
     */
    public function actionIndex(
        $page = 1,
        $count = 5,
        $orderBy = 'id',
        $sort = 'desc'
    )
    {

        $query = Statistic::find();

        $query->orderBy($orderBy . " " . $sort);

        return [
            'total' => (integer) $query->count(),
            'models' => $query->limit($count)->offset(($page - 1) * $count)->all(),
        ];

    }

}
