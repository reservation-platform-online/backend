<?php

namespace app\modules\api\modules\v1\modules\worker\controllers;

use app\modules\api\modules\v1\modules\company\models\forms\UploadFileForm;
use app\modules\api\modules\v1\modules\worker\models\business\BusinessWorker;
use Yii;
use yii\console\Response;
use yii\filters\auth\HttpBearerAuth;
use yii\filters\Cors;
use yii\rest\Controller;
use yii\web\UploadedFile;

/**
 * UserController implements the CRUD actions for Offer model.
 */
class WorkerController extends Controller
{

    public function beforeAction($action)
    {

        parent::beforeAction($action);


        if (Yii::$app->getRequest()->getMethod() === 'OPTIONS') {

            Yii::$app->getResponse()->getHeaders()->set('Allow', 'POST GET PUT DELETE');

            Yii::$app->end();

        }


        return true;

    }

    public function behaviors()
    {
        $behaviors = parent::behaviors();

        $auth = $behaviors['authenticator'];

        unset($behaviors['authenticator']);

        $behaviors['corsFilter'] = [

            'class' => Cors::className(),

        ];

        $behaviors['authenticator'] = $auth;

        // avoid authentication on CORS-pre-flight requests (HTTP OPTIONS method)

        $behaviors['authenticator'] = [

            'class' => HttpBearerAuth::className(),
            'only' => [
                'index', 'update', 'delete', 'upload-image', 'toggle-enable'
            ],
            'except' => [
                'options'
            ]

        ];

        $behaviors['client_api'] = [
            'class' => 'app\behaviors\ClientApi',
            'checkWorkerId' => true
        ];

        return $behaviors;
    }

    /**
     *
     * Lists all Offer models.
     * @return mixed
     */
    public function actionIndex()
    {

        return BusinessWorker::findOne(['id' => Yii::$app->request->getHeaders()->get('Worker-Id')]);

    }

    /**
     *
     * Lists all Offer models.
     * @return mixed
     */
    public function actionUpdate()
    {

        if (count(Yii::$app->request->post()) === 0) {
            $response = Yii::$app->response;
            $response->setStatusCode(400);
            $response->data = 'Data is empty';
            return $response;
        }

        $company = BusinessWorker::findOne(['id' => Yii::$app->request->getHeaders()->get('Worker-Id')]);

        if ($company) {

            $company->setScenario(BusinessWorker::SCENARIO_UPDATE);
            $company->load(Yii::$app->request->post(), '');

            if ($company->validate()) {

                return $company->save();

            }

        }

        return $company->errors;

    }

    /**
     * @param $id
     * @return array|bool|string|Response|\yii\web\Response
     */
    public function actionUploadImage()
    {

        $worker = BusinessWorker::findOne(['id' => Yii::$app->request->getHeaders()->get('Worker-Id')]);

        if ($worker) {
            $worker->setScenario(BusinessWorker::SCENARIO_UPDATE_PHOTO);

            $model = new UploadFileForm();
            $model->rootPath = Yii::$app->params['baseUploadsDir'] . '/workers/' . $worker->id . '/';

            if ($worker->photo) {

                $filename = explode('/', $worker->photo);
                $filename = $filename[count($filename) - 1];

                if ($filename && file_exists($model->rootPath . $filename)) {

                    unlink($model->rootPath . $filename);

                }

            }

            $model->load(Yii::$app->request->post(), '');
            $model->imageFile = UploadedFile::getInstanceByName('imageFile');

            $fileUrl = $model->upload();

            $worker->photo = $fileUrl['url'];

            if ($worker->validate()) {

                return $worker->save();

            }

        }

        $response = Yii::$app->response;
        $response->setStatusCode(400);
        $response->data = $worker->errors;
        return $response;

    }

    /**
     *
     * Lists all Offer models.
     * @return mixed
     */
    public function actionToggleEnable()
    {

        $model = BusinessWorker::findOne(['id' => Yii::$app->request->getHeaders()->get('Worker-Id')]);

        if ($model) {

            $model->setScenario(BusinessWorker::SCENARIO_UPDATE_ENABLED);

            $model->enabled = $model->enabled === BusinessWorker::ENABLED ? BusinessWorker::DISABLED : BusinessWorker::ENABLED;

            if ($model->validate()) {

                return $model->save();

            }

        }

        $response = Yii::$app->response;
        $response->setStatusCode(400);
        $response->data = $model->errors;
        return $response;

    }

    /**
     *
     * Lists all Offer models.
     * @return mixed
     */
    public function actionDelete()
    {

        // TODO enabled set 0
        return false;

    }

}
