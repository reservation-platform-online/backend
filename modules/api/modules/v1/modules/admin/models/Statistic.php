<?php

namespace app\modules\api\modules\v1\modules\admin\models;


/**
 * This is the model class for table "{{%statistic}}".
 *
 * @property int $id
 * @property int $users
 * @property int $users_confirmed
 * @property int $reservations
 * @property int $reservations_confirmed
 * @property int $workers
 * @property int $owners
 * @property int $receptions
 * @property int $points
 * @property int $companies
 * @property string $created_at
 */
class Statistic extends \app\models\statistics\Statistic
{


    /**
     * {@inheritdoc}
     */
    public function fields()
    {
        return [
            'id',
            'users',
            'usersConfirmed' => 'users_confirmed',
            'reservations',
            'reservationsConfirmed' => 'reservations_confirmed',
            'workers',
            'owners',
            'receptions',
            'points',
            'companies',
            'createdAt' => 'created_at',
        ];
    }

}
