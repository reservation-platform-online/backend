<?php

namespace app\modules\api\modules\v1\modules\admin\models\business;

use app\models\business\BusinessCompany;
use app\models\business\BusinessPointWorkerService;
use app\models\business\BusinessSubcategory;
use app\models\promoCodes\PromoCodeBusinessService;

/**
 * This is the model class for table "{{%business__service}}".
 *
 * @property string $id
 * @property string $business__company_id
 * @property string $business__subcategory_id
 * @property string $name
 * @property string $description
 * @property int $availability 1 - Offline 2 - Online 3 - Offline And Online
 * @property int $hide
 * @property int $enabled
 * @property int $status
 * @property string $created_at
 * @property string $updated_at
 *
 * @property BusinessPointWorkerService[] $businessPointWorkerServices
 * @property BusinessSubcategory $businessSubcategory
 * @property BusinessCompany $businessCompany
 * @property PromoCodeBusinessService[] $promoCodeBusinessServices
 */
class BusinessService extends \app\models\business\BusinessService
{

    public function fields() {

        return [
            'id',
            'companyId' => 'business__company_id',
            'subcategoryId' => 'business__subcategory_id',
            'name',
            'description',
            'status',
            'createdAt' => 'created_at',
            'updatedAt' => 'updated_at',
        ];

    }

}
