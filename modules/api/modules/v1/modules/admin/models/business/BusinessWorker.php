<?php

namespace app\modules\api\modules\v1\modules\admin\models\business;

use app\models\additional\AdditionalWeekend;
use app\models\business\BusinessCompany;
use app\models\business\BusinessPointWorkerService;
use app\models\business\BusinessWorkTime;
use app\models\User;

/**
 * This is the model class for table "{{%business__worker}}".
 *
 * @property string $id
 * @property string $user_id
 * @property string $business__company_id
 * @property int $enabled
 * @property int $hide
 * @property int $status
 * @property string $created_at
 * @property string $updated_at
 *
 * @property AdditionalWeekend[] $additionalWeekends
 * @property BusinessPointWorkerService[] $businessPointWorkerServices
 * @property BusinessWorkTime[] $businessWorkTimes
 * @property BusinessCompany $businessCompany
 * @property User $user
 */

class BusinessWorker extends \app\models\business\BusinessWorker
{

    public function fields() {

        return [
            'id',
            'companyId' => 'business__company_id',
            'firstName' => 'first_name',
            'lastName' => 'last_name',
            'score',
            'photo',
            'status',
            'createdAt' => 'created_at',
            'updatedAt' => 'updated_at',
        ];

    }

}
