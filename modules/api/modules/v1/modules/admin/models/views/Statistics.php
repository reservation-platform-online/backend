<?php

namespace app\modules\api\modules\v1\modules\admin\models\views;


/**
 * This is the model class for table "{{%statistic}}".
 *
 * @property int $users
 * @property int $users_confirmed
 * @property int $reservations
 * @property int $reservations_confirmed
 * @property int $workers
 * @property int $owners
 * @property int $receptions
 * @property int $points
 * @property int $companies
 */
class Statistics extends \app\models\views\Statistics
{


    /**
     * {@inheritdoc}
     */
    public function fields()
    {
        return [
            'users',
            'usersConfirmed' => 'users_confirmed',
            'reservations',
            'reservationsConfirmed' => 'reservations_confirmed',
            'workers',
            'owners',
            'receptions',
            'points',
            'companies',
        ];
    }

}
