<?php

namespace app\modules\api\modules\v1\modules\admin\controllers;

use app\modules\api\modules\v1\modules\company\models\business\BusinessService;
use yii\rest\Controller;

/**
 * UserController implements the CRUD actions for Offer model.
 */
class ServiceController extends Controller
{

    // TODO CRUD

    /**
     *
     * Lists all Offer models.
     * @param int $page
     * @param int $count
     * @param null $subcategoryId
     * @param null $companyId
     * @param string $orderBy
     * @param string $sort
     * @return mixed
     */
    public function actionIndex(
        $page = 1,
        $count = 5,
        $subcategoryId = null,
        $companyId = null,
        $orderBy = 'id',
        $sort = 'desc'
    ) {

        $query = BusinessService::find()
            ->where(['enabled' => BusinessService::ENABLED]);

        if (!is_null($subcategoryId)) {
            $query->andWhere(['business__subcategory_id' => $subcategoryId]);
        }

        if (!is_null($companyId)) {
            $query->andWhere(['business__company_id' => $companyId]);
        }

        $query->orderBy($orderBy . " " . $sort);

        return [
            'total' => (integer) $query->count(),
            'models' => $query->limit($count)->offset(($page - 1) * $count)->all(),
        ];

    }

    /**
     *
     * Lists all Offer models.
     * @param $id
     * @return mixed
     */
    public function actionView($id)
    {

        return BusinessService::findOne($id);

    }

}
