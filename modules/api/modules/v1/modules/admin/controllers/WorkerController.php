<?php

namespace app\modules\api\modules\v1\modules\admin\controllers;

use app\modules\api\modules\v1\modules\company\models\business\BusinessWorker;
use yii\rest\Controller;

/**
 * UserController implements the CRUD actions for Offer model.
 */
class WorkerController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {

        $behaviors = parent::behaviors();

        $behaviors['client_api'] = [
            'class' => 'app\behaviors\ClientApi',
        ];

        return $behaviors;

    }

    /**
     *
     * Lists all Offer models.
     * @param int $page
     * @param int $count
     * @param null $companyId
     * @param string $orderBy
     * @param string $sort
     * @return mixed
     */
    public function actionIndex(
        $page = 1,
        $count = 5,
        $companyId = null,
        $orderBy = 'id',
        $sort = 'desc'
    )
    {

        $query = BusinessWorker::find()->where(['enabled' => BusinessWorker::ENABLED]);

        if (!is_null($companyId)) {
            $query->andWhere(['business__company_id' => $companyId]);
        }

        $query->orderBy($orderBy . " " . $sort);

        return [
            'total' => (integer) $query->count(),
            'models' => $query->limit($count)->offset(($page - 1) * $count)->all(),
        ];

    }

    /**
     *
     * Lists all Offer models.
     * @param $id
     * @return mixed
     */
    public function actionView($id)
    {

        return BusinessWorker::findOne($id);

    }
}
