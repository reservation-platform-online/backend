<?php

namespace app\modules\api\modules\v1\modules\admin\controllers\views;

use app\modules\api\modules\v1\modules\admin\models\views\Statistics;
use yii\rest\Controller;

/**
 * UserController implements the CRUD actions for Offer model.
 */
class StatisticsController extends Controller
{

    /**
     *
     * Lists all Offer models.
     * @return mixed
     */
    public function actionIndex()
    {

        return Statistics::find()->all();

    }

}
