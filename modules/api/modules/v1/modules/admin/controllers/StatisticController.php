<?php

namespace app\modules\api\modules\v1\modules\admin\controllers;

use app\modules\api\modules\v1\modules\admin\models\Statistic;
use yii\rest\Controller;

/**
 * UserController implements the CRUD actions for Offer model.
 */
class StatisticController extends Controller
{

    /**
     *
     * Lists all Offer models.
     * @param int $page
     * @param int $count
     * @param string $orderBy
     * @param string $sort
     * @return mixed
     */
    public function actionIndex(
        $page = 1,
        $count = 5,
        $orderBy = 'id',
        $sort = 'desc'
    )
    {

        $query = Statistic::find();

        $query->orderBy($orderBy . " " . $sort);

        return [
            'total' => (integer) $query->count(),
            'models' => $query->limit($count)->offset(($page - 1) * $count)->all(),
        ];

    }

}
