<?php


namespace app\modules\api;

use yii\web\Request;
use yii\web\User;

/**
 * admin module definition class
 */
class Module extends \yii\base\Module
{

    /**
     * {@inheritdoc}
     */
    public function init()
    {
        parent::init();

        // custom initialization code goes here
        $this->components = [
            'request' => [
                'class'=> Request::class,
                'enableCsrfValidation' => false,
                'enableCookieValidation' => false,
                'parsers' => [
                    'application/json' => 'yii\web\JsonParser',
                ],
            ],
            'user' => [
                'class'=> User::class,
                'enableAutoLogin' => false,
                'enableSession' => false,
            ],
        ];

        $this->modules = [
            'v1' => [
                // you should consider using a shorter namespace here!
                'class' => 'app\modules\api\modules\v1\Module',
            ],
        ];

    }
}
