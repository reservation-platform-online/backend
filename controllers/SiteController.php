<?php

namespace app\controllers;

use Yii;
use yii\filters\AccessControl;
use yii\filters\VerbFilter;
use yii\web\Controller;

class SiteController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['logout'],
                'rules' => [
                    [
                        'actions' => ['logout'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'logout' => ['post'],
                ],
            ],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
            'captcha' => [
                'class' => 'yii\captcha\CaptchaAction',
                'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
            ],
        ];
    }

    public function actionRedirect()
    {

        $params = Yii::$app->getRequest()->getQueryParams();
        $redirect = $params['to'];
        switch ($redirect) {
            case 'client/auth/login':
//                $redirect = 'http://localhost:8100/auth/login';

                $redirect = Yii::$app->params['urls'][Yii::$app->params['path']]['client'] . '/auth/login';
                if ($params['code']) {
                    $code = base64_encode($params['code']);
                    $redirect .= "/" . $code;
                    unset($params['code']);
                }
                break;
            case 'client/profile':
//                $redirect = 'http://localhost:8100/profile';

                $redirect = Yii::$app->params['urls'][Yii::$app->params['path']]['client'] . '/profile';
                if ($params['code']) {
                    $code = base64_encode($params['code']);
                    $redirect .= "/" . $code;
                    unset($params['code']);
                }
                break;
        }
        unset($params['to']);
        return Yii::$app->response->redirect($redirect);

    }

    /**
     * Displays homepage.
     *
     * @return string
     */
    public function actionIndex()
    {

        return $this->render('index');
    }
}
